import http from './http'

export const getCartByIdUser = data => {
   return http.post('/Cart/getCartByIdUser', data);
}

export const updateCart = data => {
   return http.post('Cart//addQty', data)
}

export const deleteCart = data => {
   return http.post('/Cart/deleteCart', data)
}

export const createCart = data => {
   return http.post('/Cart/addToCart', data);
}