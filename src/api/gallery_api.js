import http from './http'

export const getGallery = data => {
   return http.post('/Gallery/getGallery/', data);
}

export const deleteGallery = data => {
   return http.post('/Gallery/deleteGallery/', data)
}

export const createGallery = data => {
   console.log("ini data backend", data);
   const dataGroup = new FormData();
   dataGroup.append('gambar', {
      uri: Platform.OS === 'android' ? data.gambar.uri : data.gambar.uri.replace('file://', ''),
      name: 'image.jpg',
      type: data.gambar.type
   });
   dataGroup.append('id_user', data.id_user)
   dataGroup.append('id_workshop', data.id_workshop)

   console.log('form data create trainer', dataGroup);
   return http.post('/Gallery/createGallery', dataGroup, {
      headers: {
         'Content-Type': 'multipart/form-data',
      }
   })
}