import http from './http'

export const getWorkshop = data => {
   return http.post('/Workshop/getWorkshop/', data);
}

export const getWorkshopById = data => {
   return http.post('/Workshop/getWorkshopById', data);
}

export const deleteWorkshop = data => {
   return http.post('/Workshop/deleteWorkshop/', data)
}

export const updateWorkshop = data => {
   return http.post('/Workshop/updateWorkshop/', data);
}

export const updateWorkshopWithPhoto = data => {
   console.log("ini data backend", data);
   const dataGroup = new FormData();
   dataGroup.append('image', {
      uri: Platform.OS === 'android' ? data.image.uri : data.image.uri.replace('file://', ''),
      name: 'image.jpg',
      type: data.image.type
   });
   dataGroup.append('id_user', data.id_user)
   dataGroup.append('id', data.id)
   dataGroup.append('title', data.title)
   dataGroup.append('desc', data.desc)
   dataGroup.append('hosted_by', data.hosted_by)
   dataGroup.append('start_date', data.start_date)
   dataGroup.append('end_date', data.end_date)
   dataGroup.append('class_size', data.class_size)
   dataGroup.append('min_age', data.min_age)
   dataGroup.append('language', data.language)
   dataGroup.append('price', data.price)
   dataGroup.append('level', data.level)
   dataGroup.append('status', data.status)
   dataGroup.append('photo', data.photo)
   dataGroup.append('location', data.location)
   dataGroup.append('keyword', data.keyword)
   dataGroup.append('gender', data.gender)
   dataGroup.append('what_you_learn', data.what_you_learn)
   dataGroup.append('terms', data.terms)
   dataGroup.append('slug', data.slug)
   dataGroup.append('category', data.category)
   dataGroup.append('last_update', data.last_update)
   dataGroup.append('id_user', data.id_user)
   dataGroup.append('id_speaker', data.id_speaker)
   dataGroup.append('counter', data.counter)
   dataGroup.append('inclusive', data.inclusive)
   dataGroup.append('what_you_bring', data.what_you_bring)
   dataGroup.append('time', data.time)
   dataGroup.append('promotion', data.promotion)
   dataGroup.append('alamat', data.alamat)
   dataGroup.append('kelurahan', data.keluarahan)
   dataGroup.append('kecamatan', data.kecamatan)
   dataGroup.append('kotamadya', data.kotamadya)
   dataGroup.append('kodepos', data.kodepos)
   dataGroup.append('promo', data.promo)
   dataGroup.append('promo_price', data.promo_price)
   dataGroup.append('multi_tanggal', data.multi_tanggal)
   dataGroup.append('kota', data.kota)
   dataGroup.append('subcategory', data.subcategory)
   dataGroup.append('time_end', data.time_end)
   dataGroup.append('tempat', data.tempat)
   dataGroup.append('posted', data.posted)
   dataGroup.append('status_publish', data.status_pubish)

   console.log('form data create trainer', dataGroup);
   return http.post('/Workshop/updateWorkshopWithPhoto/', dataGroup, {
      headers: {
         'Content-Type': 'multipart/form-data',
      }
   })
}

export const createWorkshop = data => {
   return http.post('/Workshop/createWorkshop/', data);
}

export const createWorkshopWithPhoto = data => {
   console.log("ini data backend", data);
   const dataGroup = new FormData();
   dataGroup.append('image', {
      uri: Platform.OS === 'android' ? data.image.uri : data.image.uri.replace('file://', ''),
      name: 'image.jpg',
      type: data.image.type
   });
   dataGroup.append('id_user', data.id_user)
   dataGroup.append('id', data.id)
   dataGroup.append('title', data.title)
   dataGroup.append('desc', data.desc)
   dataGroup.append('hosted_by', data.hosted_by)
   dataGroup.append('start_date', data.start_date)
   dataGroup.append('end_date', data.end_date)
   dataGroup.append('class_size', data.class_size)
   dataGroup.append('min_age', data.min_age)
   dataGroup.append('language', data.language)
   dataGroup.append('price', data.price)
   dataGroup.append('level', data.level)
   dataGroup.append('status', data.status)
   dataGroup.append('photo', data.photo)
   dataGroup.append('location', data.location)
   dataGroup.append('keyword', data.keyword)
   dataGroup.append('gender', data.gender)
   dataGroup.append('what_you_learn', data.what_you_learn)
   dataGroup.append('terms', data.terms)
   dataGroup.append('slug', data.slug)
   dataGroup.append('category', data.category)
   dataGroup.append('last_update', data.last_update)
   dataGroup.append('id_user', data.id_user)
   dataGroup.append('id_speaker', data.id_speaker)
   dataGroup.append('counter', data.counter)
   dataGroup.append('inclusive', data.inclusive)
   dataGroup.append('what_you_bring', data.what_you_bring)
   dataGroup.append('time', data.time)
   dataGroup.append('promotion', data.promotion)
   dataGroup.append('alamat', data.alamat)
   dataGroup.append('kelurahan', data.keluarahan)
   dataGroup.append('kecamatan', data.kecamatan)
   dataGroup.append('kotamadya', data.kotamadya)
   dataGroup.append('kodepos', data.kodepos)
   dataGroup.append('promo', data.promo)
   dataGroup.append('promo_price', data.promo_price)
   dataGroup.append('multi_tanggal', data.multi_tanggal)
   dataGroup.append('kota', data.kota)
   dataGroup.append('subcategory', data.subcategory)
   dataGroup.append('time_end', data.time_end)
   dataGroup.append('tempat', data.tempat)
   dataGroup.append('posted', data.posted)
   dataGroup.append('status_publish', data.status_pubish)

   console.log('form data create workshop', dataGroup);
   return http.post('/Workshop/createWorkshopWithPhoto/', dataGroup, {
      headers: {
         'Content-Type': 'multipart/form-data',
      }
   })
}