import http from './http'

export const kodePromo = data => {
   return http.post('/Promo/kodePromo/', data);
}

export const kodeVoucher = data => {
   return http.post('/Promo/kodeVoucher/', data);
}

export const kodeReferral = data => {
   return http.post('/Promo/kodeReferral/', data);
}

export const kodePrakerja = data => {
   return http.post('/Promo/kodePrakerja/', data);
}