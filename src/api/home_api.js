import http from './http'

export const bannerHome = data => {
   return http.get('/Home/bannerList');
}

export const categoryHome = () => {
   return http.get('./Home/categoryList')
}

export const facilitatorList = data => {
   return http.get('/Home/facilitatorList')
}

export const facilitatorListAll = data => {
   return http.get('/Home/facilitatorListAll')
}

export const facilitatorById = data => {
   return http.get('/Home/facilitatorById?id=' + data)
}

export const facilitatorByIdSpeaker = data => {
   return http.get('/Home/facilitatorById?id=' + data)
}

export const workshopList = data => {
   return http.get('./Home/workshopList');
}

export const workshopListAll = data => {
   return http.get('./Home/workshopListAll');
}

export const articleList = data => {
   return http.get('./Home/articleList');
}

export const workshopCity = data => {
   return http.get('./Home/workshopCity?category=' + data)
}

export const workshopByName = data => {
   return http.get('./Home/workshopByName?category=' + data)
}

export const articleByName = data => {
   return http.get('/Home/articleByName?category=' + data)
}

export const highlight = data => {
   return http.post('/Home/highlight');
}

export const prakerja = data => {
   return http.get('/Home/PrakerjaWorkshop/');
}

export const upComming = data => {
   return http.get('/Home/Upcomming/');
}

export const hotWorkshop = data => {
   return http.get('/Home/HotWorkshop/');
}

