import http from './http'

export const getTrainer = data => {
   return http.post('/Trainer/getTrainer/', data);
}

export const updateTrainer = data => {
   return http.post('/Trainer/updateTrainer/', data);
}

export const updateTrainerWithPhoto = data => {
   console.log("ini data backend", data);
   const dataGroup = new FormData();
   dataGroup.append('photo', {
      uri: Platform.OS === 'android' ? data.photo.uri : data.photo.uri.replace('file://', ''),
      name: 'image.jpg',
      type: data.photo.type
   });
   dataGroup.append('name', data.name)
   dataGroup.append('email', data.email)
   dataGroup.append('competency', data.competency)
   dataGroup.append('desc', data.desc)
   dataGroup.append('certification', data.certification)
   dataGroup.append('status', data.status)
   dataGroup.append('id_user', data.id_user)
   dataGroup.append('id', data.id)

   console.log('form data create trainer', dataGroup);
   return http.post('/Trainer/updateTrainerWithPhoto', dataGroup, {
      headers: {
         'Content-Type': 'multipart/form-data',
      }
   })
}

export const createTrainer = data => {
   return http.post('/Trainer/createTrainer/', data)
}

export const createTrainerWithPhoto = data => {
   console.log("ini data backend", data);
   const dataGroup = new FormData();
   dataGroup.append('photo', {
      uri: Platform.OS === 'android' ? data.photo.uri : data.photo.uri.replace('file://', ''),
      name: 'image.jpg',
      type: data.photo.type
   });
   dataGroup.append('id_user', data.id_user)
   dataGroup.append('name', data.name)
   dataGroup.append('email', data.email)
   dataGroup.append('competency', data.competency)
   dataGroup.append('desc', data.desc)
   dataGroup.append('certification', data.certification)
   dataGroup.append('status', data.status)

   console.log('form data create trainer', dataGroup);
   return http.post('/Trainer/createTrainerWithPhoto', dataGroup, {
      headers: {
         'Content-Type': 'multipart/form-data',
      }
   })
}

export const deleteTrainer = data => {
   return http.post('/Trainer/deleteTrainer/', data)
}
