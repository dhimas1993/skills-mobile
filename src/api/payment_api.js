import http from './http'

export const getPayment_waiting = data => {
   return http.post('/Payment/paymentWaiting', data);
}

export const getPayment_verified = data => {
   return http.post('/Payment/paymentVerified', data);
}

export const cancelPayment = data => {
   return http.post('/Payment/cancelPayment', data);
}

export const checkout = data => {
   return http.post('/Payment/Checkout', data);
}

export const uploadPayment = data => {
   console.log("ini data backend", data);
   const dataGroup = new FormData();
   dataGroup.append('gambar', {
      uri: Platform.OS === 'android' ? data.gambar.uri : data.gambar.uri.replace('file://', ''),
      name: 'image.jpg',
      type: data.gambar.type
   });
   dataGroup.append('id_user', data.id_user)
   dataGroup.append('code', data.code)

   console.log('form data upload payment', dataGroup);
   return http.post('/Payment/uploadPayment', dataGroup, {
      headers: {
         'Content-Type': 'multipart/form-data',
      }
   })
}