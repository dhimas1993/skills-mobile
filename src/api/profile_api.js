import http from './http'

export const getProfile = data => {
   return http.post('/Profile/getProfile', data);
}

export const editProfile = data => {
   return http.post('/Profile/editProfile', data);
}

export const editProfileWithPhoto = data => {
   console.log("ini data backend", data);
   const dataGroup = new FormData();
   dataGroup.append('photo', {
      uri: Platform.OS === 'android' ? data.photo.uri : data.photo.uri.replace('file://', ''),
      name: 'image.jpg',
      type: data.photo.type
   });
   dataGroup.append('name', data.name)
   dataGroup.append('no_handphone', data.no_handphone)
   dataGroup.append('address', data.address)
   dataGroup.append('id_user', data.id_user)
   dataGroup.append('status', data.status)

   console.log('form data', dataGroup);
   return http.post('/Profile/editProfileWithPhoto', dataGroup, {
      headers: {
         'Content-Type': 'multipart/form-data',
      }
   })
}

export const regist_participant = data => {
   return http.post('/Registrasi/registrasiParticipant', data)
}

export const regist_facilitator = data => {
   return http.post('/Registrasi/registrasiFacilitator', data)
}

export const changePass = data => {
   return http.post('/Profile/changePass', data)
}

export const changePhoto = data => {
   console.log("ini data backend", data);
   const dataGroup = new FormData();
   dataGroup.append('avatar', {
      uri: Platform.OS === 'android' ? data.avatar.uri : data.avatar.uri.replace('file://', ''),
      name: 'image.jpg',
      type: data.avatar.type
   });
   dataGroup.append('id_user', data.id_user)
   dataGroup.append('name', data.name)
   dataGroup.append('status', data.status)

   // console.log('form data', dataGroup);
   return http.post('/Profile/changePhoto', dataGroup, {
      headers: {
         'Content-Type': 'multipart/form-data',
      }
   })
}

export const changeCover = data => {
   console.log("ini data backend", data);
   const dataGroup = new FormData();
   dataGroup.append('cover', {
      uri: Platform.OS === 'android' ? data.cover.uri : data.cover.uri.replace('file://', ''),
      name: 'image.jpg',
      type: data.cover.type
   });
   dataGroup.append('id_user', data.id_user)
   dataGroup.append('name', data.name)

   console.log('form data cover', dataGroup);
   return http.post('/Profile/changeCover', dataGroup, {
      headers: {
         'Content-Type': 'multipart/form-data',
      }
   })
}

