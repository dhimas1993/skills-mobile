import http from './http'

export const getWishlist = data => {
   return http.post('/Wishlist/getWishlist', data);
}

export const deleteWishlist = data => {
   return http.post('/Wishlist/deleteWIshlist', data)
}

export const getWishlistById = (id_workshop, id_user) => {
   return http.post('/Wishlist/getWishlistById', id_workshop, id_user);
}

export const createWishlist = data => {
   return http.post('/Wishlist/createWishlist', data);
}