const Stack = createStackNavigator();
const cari = createStackNavigator();
const Seek = createStackNavigator();

console.ignoredYellowBox

function Cari() {
   return (
      <cari.Navigator headerMode="none">
         <cari.Screen name="Acctypes" component={Acctype} />
         <cari.Screen name="Regis" component={Regis} />
         <cari.Screen name="Reffcode" component={Reffcode} />
         <cari.Screen name="Otpcode" component={Otpcode} />
         <cari.Screen name="Compdetail" component={Compdetail} />
         <cari.Screen name="Category" component={Category} />
         <cari.Screen name="Terms" component={Terms} />
         <cari.Screen name="SetScreen" component={SetScreen} />
      </cari.Navigator>
   );
}

function Seeker() {
   return (
      <Seek.Navigator headerMode="none">
         <Seek.Screen name="Tabs" component={Tabs} />
         <Seek.Screen name="Web" component={Web} />
         <Seek.Screen name="Devid" component={Devid} />
         <Seek.Screen name="SaveJobs" component={SaveJobs} />
         <Seek.Screen name="JobApplied" component={JobApplied} />
         <Seek.Screen name="History" component={History} />
         <Seek.Screen name="ResetPass" component={ResetPass} />
         <Seek.Screen name="DetailJob" component={DetailJob} />
         <Seek.Screen name="CategoryJobs" component={CategoryJobs} />
         <Seek.Screen name="Resume" component={Resume} />
         <Seek.Screen name="PersonInfo" component={PersonInfo} />
         <Seek.Screen name="TechnicalSkills" component={TechnicalSkills} />
         <Seek.Screen name="Education" component={Education} />
         <Seek.Screen name="Experience" component={Experience} />
      </Seek.Navigator>
   );
}

function App() {
   return (
      <Provider store={store}>
         <PersistGate persistor={persistor}>
            <NavigationContainer>
               <Stack.Navigator headerMode="none">
                  <Stack.Screen name="Intro" component={Intro} />
                  <Stack.Screen name="Login" component={Login} />
                  <Stack.Screen name="ForgetPass" component={ForgetPass} />
                  <Stack.Screen name="SetScreen" component={SetScreen} />
                  <Stack.Screen name="Acctype" component={Cari} />
                  <Stack.Screen name="Otp" component={Cari} />
                  <Stack.Screen name="Tab" component={Seeker} />
               </Stack.Navigator>
            </NavigationContainer>
         </PersistGate>
      </Provider>
   );
}

export default App;