import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

import MassageScreen from '../seeker/Message'
import HomeScreen from '../seeker/Home'
import ProfileScreen from '../seeker/Profile'


const HomeStack = createStackNavigator();
function HomeStackScreen() {
  return (
    <HomeStack.Navigator headerMode="none">
      <HomeStack.Screen name="Home" component={HomeScreen} />
    </HomeStack.Navigator>
  );
}

const ProfileStack = createStackNavigator();
function ProfileStackScreen() {
  return (
    <ProfileStack.Navigator headerMode="none">
      <ProfileStack.Screen name="Profile" component={ProfileScreen} />
    </ProfileStack.Navigator>
  );
}

const MessageStack = createStackNavigator();
function MessageStackScreen() {
  return (
    <MessageStack.Navigator headerMode="none">
      <MessageStack.Screen name="MassageScreen" component={MassageScreen} />
    </MessageStack.Navigator>
  );
}

const Tab = createBottomTabNavigator();
export default function App() {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          if (route.name === 'Home') {
            iconName = focused
              ? 'md-home'
              : 'md-home';
          } else if (route.name === 'Profile') {
            iconName = focused
              ? 'md-person'
              : 'md-person';
          } else if (route.name === 'Message') {
            iconName = focused
              ? 'md-mail-open'
              : 'md-mail';
          }

          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }}
    >
      <Tab.Screen name="Home" component={HomeStackScreen} />
      <Tab.Screen name="Profile" component={ProfileStackScreen} />
      <Tab.Screen name="Message" component={MessageStackScreen} />
    </Tab.Navigator>
  );
}
