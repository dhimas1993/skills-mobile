import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, ScrollView, ImageBackground } from 'react-native'

import { prakerja } from '../../api/home_api'

import Icons from 'react-native-vector-icons/FontAwesome5';


export default class Workshop extends Component {
   constructor(props) {
      super(props);
      this.state = {
         workshop: []
      };
   }

   _toWorkshopPage = (param) => {
      this.props.navigation.navigate('WorkshopPage', param)
   }

   _toWorkshopAll = () => {
      this.props.navigation.navigate('Explore')
   }

   async componentDidMount() {
      const api = await prakerja()
      this.setState({
         workshop: api.data,
      })
      console.log('api', api);
   }

   renderList = () => {
      if (this.state.workshop) {
         return (
            this.state.workshop.map((item) => {
               // console.log("item.image", item.image);
               return (
                  <TouchableOpacity onPress={() => this._toWorkshopPage(item)}>
                     <View style={{ width: 180, height: 200, borderRadius: 10, marginHorizontal: 5, backgroundColor: 'white', elevation: 5, shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, }}>
                        <View style={{ height: '65%' }}>
                           <ImageBackground
                              source={{ uri: 'https://skills.id/admin/source/images/thumbnail/' + item.image }} style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }} imageStyle={{ borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                              <View style={{ width: '100%', height: 25, backgroundColor: '#078b6d', opacity: 0.6, marginTop: 90 }}></View>
                           </ImageBackground>
                           <Text style={{ color: "white", position: "absolute", marginLeft: 13, fontSize: 11, marginTop: 103, fontWeight: 'bold' }}>{item.category}</Text>
                        </View>
                        <Text style={{ paddingLeft: 7, paddingTop: 5, fontSize: 11 }} numberOfLines={2}>{item.title}</Text>
                        <Text style={{ paddingLeft: 7, paddingTop: 5, fontSize: 11, fontStyle: 'italic' }}>{item.name}</Text>
                     </View>
                  </TouchableOpacity>
               )
            })
         )
      } else {
         <View>
            <Text>Kosong</Text>
         </View>
      }
   }

   render() {
      return (
         <View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 10 }}>
               <Text style={{ fontSize: 18, marginTop: 3, marginBottom: 20, fontWeight: 'bold' }}>Prakerja</Text>
               <TouchableOpacity
                  onPress={() => this._toWorkshopAll()}
               >
                  {/* <Text style={{ fontSize: 13, marginTop: 10, marginRight: 10, fontWeight: 'bold' }}>View All</Text>
                   */}
                  <Icons style={{ fontSize: 20, marginRight: 15, color: '#52057b' }} name='chevron-circle-right' />
               </TouchableOpacity>
            </View>

            <View style={{ height: 220, justifyContent: 'space-around' }}>
               <ScrollView style={{ marginHorizontal: 5 }} horizontal={true} showsHorizontalScrollIndicator={false}>
                  {this.renderList()}
               </ScrollView>
            </View>
         </View>
      )
   }
}

const styles = StyleSheet.create({})
