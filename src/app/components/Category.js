import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView, TouchableOpacity } from 'react-native'

import { categoryHome } from '../../api/home_api'

import Icons from 'react-native-vector-icons/FontAwesome5';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Category extends Component {
   constructor(props) {
      super(props);
      this.state = {
         category: []
      };
   }

   componentDidMount() {
      this.getCategory()
      console.log(this.state.category);
   }

   getCategory = async () => {
      const api = await categoryHome()
      this.setState({
         category: api.data,
      })
      console.log("get", api.data);
   }

   _toCategoryDetailPage = (param) => {
      this.props.navigation.navigate('DetailCategoryPage', { detail_cat: param })
   }

   renderCategory = () => {
      this.state.category.map((item) => {
         return (
            <View style={{ backgroundColor: 'pink' }}>
               <Text>{item.category}</Text>
            </View>
         )
      })
   }

   render() {
      return (
         <View style={{ flex: 1 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 10 }}>
               <Text style={{ fontSize: 18, marginTop: 3, marginBottom: 10, fontWeight: 'bold' }}>Category</Text>
               <TouchableOpacity onPress={() => this.props.navigation.navigate('CategoryPage')}>
                  <Icons style={{ fontSize: 20, marginRight: 15, color: '#52057b' }} name='chevron-circle-right' />
                  {/* <Text style={{ fontSize: 13, marginTop: 10, marginRight: 10, fontWeight: 'bold' }}>View All </Text> */}
               </TouchableOpacity>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', paddingTop: 25, paddingBottom: 25 }}>
               <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}
                  style={{}}
               >
                  {
                     this.state.category.map((item) => {
                        return (
                           <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                              <TouchableOpacity onPress={() => this._toCategoryDetailPage(item)}>
                                 <View style={{ marginLeft: 10, borderColor: '#52057b', borderWidth: 1, justifyContent: 'center', alignItems: 'center', padding: 5, backgroundColor: 'white', borderRadius: 100, width: 85, height: 85 }}>
                                    {
                                       item.category == 'Music' ?
                                          <Icons style={{ fontSize: 25, color: '#52057b' }} name='music' />
                                          :
                                          item.category == 'Art and Craft' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='paint-brush' /> :
                                             item.category == 'Food and Beverages' ? <Icon style={{ fontSize: 25, color: '#52057b' }} name='cutlery' /> :
                                                item.category == 'Design' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='pencil-ruler' /> :
                                                   item.category == 'Photography' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='camera' /> :
                                                      item.category == 'Sport' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='basketball-ball' /> :
                                                         item.category == 'Business' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='briefcase' /> :
                                                            item.category == 'Fashion' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='tshirt' /> :
                                                               item.category == 'Lifestyle' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='smile' /> :
                                                                  item.category == 'Technology' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='laptop' /> :
                                                                     item.category == 'Specific' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='ellipsis-h' /> :
                                                                        <Text>Gagal Loading</Text>
                                    }
                                    <Text style={{ fontSize: 10, marginTop: 10, textAlign: 'center' }}>{item.category}</Text>
                                    {
                                       this.renderCategory()
                                    }
                                 </View>
                              </TouchableOpacity>
                           </View>
                        )
                     })
                  }
               </ScrollView>
            </View>
         </View>
      )
   }
}

const styles = StyleSheet.create({})
