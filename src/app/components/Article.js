import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, ImageBackground, ScrollView } from 'react-native'

import { articleList } from '../../api/home_api'

import Icons from 'react-native-vector-icons/FontAwesome5';


export default class Article extends Component {
   constructor(props) {
      super(props);
      this.state = {
         article: []
      }
   }

   async componentDidMount() {
      const api = await articleList()
      this.setState({
         article: api.data
      })
   }

   _toArticlePage = (param) => {
      this.props.navigation.navigate('ArticlePage', { detail_article: param })
   }


   renderList = () => {
      if (this.state.article) {
         return (
            this.state.article.map((item) => {
               return (
                  <TouchableOpacity onPress={() => this._toArticlePage(item)} >
                     <View style={{ width: 180, height: 170, marginHorizontal: 5, borderRadius: 10, backgroundColor: 'white', elevation: 5, shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, }}>
                        <View style={{ height: '65%' }}>
                           <ImageBackground
                              source={{ uri: 'https://skills.id/admin/source/images/blog/' + item.banner }} style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }} imageStyle={{ borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                              <View style={{ width: '100%', height: 25, backgroundColor: '#52057b', opacity: 0.6, marginTop: 80, marginLeft: 30, marginRight: 30 }}></View>
                           </ImageBackground>
                           <Text style={{ color: "white", position: "absolute", marginLeft: 10, fontSize: 11, marginTop: 89, fontWeight: 'bold' }}>{item.category}</Text>
                        </View>
                        <Text style={{ paddingLeft: 7, paddingTop: 5, fontSize: 11 }} numberOfLines={2}>{item.title}</Text>
                        <Text style={{ paddingLeft: 7, paddingTop: 5, fontSize: 11, fontStyle: 'italic' }}>{item.posted_by}</Text>
                     </View>
                  </TouchableOpacity>
               )
            })
         )
      } else {
         <View>
            <Text>gada</Text>
         </View>
      }
   }

   render() {
      return (
         <View style={{ marginBottom: 10 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 10 }}>
               <Text style={{ fontSize: 18, marginTop: 3, marginBottom: 20, fontWeight: 'bold' }}>Article</Text>
               <TouchableOpacity >
                  <Icons style={{ fontSize: 20, marginRight: 15, color: '#52057b' }} name='chevron-circle-right' />
                  {/* <Text style={{ fontSize: 13, marginTop: 10, marginRight: 10, fontWeight: 'bold' }}>View All</Text> */}
               </TouchableOpacity>
            </View>
            <View style={{ height: 190, justifyContent: 'space-around' }}>
               <ScrollView style={{ marginHorizontal: 5 }} horizontal={true} showsHorizontalScrollIndicator={false}>
                  {this.renderList()}
               </ScrollView>
            </View>
         </View>
      )
   }
}

const styles = StyleSheet.create({})
