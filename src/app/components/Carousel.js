import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux'

import { bannerHome } from '../../api/home_api'
import { forceLogout } from '../../../src/store/auth/action'

const DEVICE_WIDTH = Dimensions.get("window").width;

class Carousel extends Component {
   scrollRef = React.createRef();
   constructor(props) {
      super(props);

      this.state = {
         selectedIndex: 0,
         banner: []
      }
   }

   // component did mount untuk render layer focus terbuka load pertama kali
   async componentDidMount() {
      const api = await bannerHome()
      this.setState({
         banner: api.data
      })
      console.log(api.data);
   }

   onLogout = async () => {
      await this.props.forceLogout()
      // this.props.navigation.navigate('Home')
   }

   banner = () => {
      const myVar = setInterval(setBanner, 300)
   }

   render() {
      return (
         <View style={styles.carousel}>
            <ScrollView horizontal={true} pagingEnabled showsHorizontalScrollIndicator={false}>
               {
                  this.state.banner !== '' ?
                     this.state.banner.map((image) => {
                        return (
                           <View
                              // onPress={() => { this.onLogout() }}
                              key={image.id}
                              style={{ width: DEVICE_WIDTH, height: '80%', backgroundColor: 'pink', justifyContent: 'center' }}>
                              <Image
                                 style={{ width: DEVICE_WIDTH, height: '100%' }}
                                 source={{ uri: 'https://skills.id/admin/source/images/banner/' + image.banner }} />
                           </View>
                        )
                     })
                     : null
               }
            </ScrollView>
         </View>
      );
   }
}

export default connect(null, { forceLogout })(Carousel)

const styles = StyleSheet.create({
   carousel: {
      height: 175
   },
   dot: {
      width: 10,
      height: 10,
      backgroundColor: 'grey',
      borderRadius: 100,
      marginRight: 10,
      marginTop: 10
   }
});
