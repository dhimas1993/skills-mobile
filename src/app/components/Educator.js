import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, ScrollView, TouchableOpacity } from 'react-native'

import Iconss from "react-native-vector-icons/MaterialCommunityIcons"

import { facilitatorList } from '../../api/home_api';

export default class Facilitator extends Component {
   constructor(props) {
      super(props);
      this.state = {
         educator: []
      };
   }

   async componentDidMount() {
      const api = await facilitatorList()
      this.setState({
         educator: api.data,
      })
      // console.log('edu', this.state.educator);
   }

   _toFacilitatorPage = (param) => {
      this.props.navigation.navigate('FacilitatorPage', { detail_edu: param })
   }

   _toFacilitatorAll = () => {
      this.props.navigation.navigate('Explore')
   }

   render() {
      return (
         <View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 10 }}>
               <Text style={{ fontSize: 18, marginTop: 3, marginBottom: 5 }}>Facilitator</Text>
               <TouchableOpacity onPress={() => this._toFacilitatorAll()}>
                  <Text style={{ fontSize: 13, marginTop: 10, marginRight: 10, fontWeight: 'bold' }}>View All</Text>
               </TouchableOpacity>
            </View>
            <View style={{ paddingTop: 10, marginBottom: 15, marginHorizontal: 10 }}>
               <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                  {
                     this.state.educator.map((item) => {
                        {/* console.log('edu', item); */ }
                        return (
                           <TouchableOpacity onPress={() => this._toFacilitatorPage(item)}>
                              <View style={{
                                 width: 180, borderRadius: 10, marginRight: 10, backgroundColor: 'white', elevation: 5, marginVertical: 7, marginHorizontal: 2, shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84,
                              }}>
                                 <Image style={{ height: 130, flex: 1, width: 180, borderTopRightRadius: 10, borderTopLeftRadius: 10 }} source={{ uri: 'https://skills.id/admin/source/images/speaker/' + item.photo }}></Image>
                                 <View style={{ flex: 1, padding: 10 }}>
                                    <View style={{ marginBottom: 10 }}>
                                       <Text style={{ paddingTop: 2, fontSize: 15, fontWeight: 'bold' }} numberOfLines={1}>{item.name}</Text>
                                       <Text style={{ paddingTop: 2, fontSize: 13, flexWrap: "wrap", fontStyle: 'italic' }} numberOfLines={1}>{item.email}</Text>
                                    </View>
                                    <View style={{ marginBottom: 10 }}>
                                       <View style={{ flexDirection: 'row', marginVertical: 1, alignItems: 'center', marginBottom: 5 }}>
                                          <Iconss name="camcorder" style={{ fontSize: 20, marginRight: 10 }} />
                                          <Text style={{ fontSize: 13 }}>5 Chapter</Text>
                                       </View>
                                       <View style={{ flexDirection: 'row', marginVertical: 1, alignItems: 'center' }}>
                                          <Iconss name='google-classroom' style={{ fontSize: 20, marginRight: 10 }} />
                                          <Text style={{ fontSize: 13 }}>{item.sum_wk} Class</Text>
                                       </View>
                                    </View>
                                    <View style={{ flexDirection: "row", justifyContent: 'space-between', marginBottom: 10 }}>
                                       <View style={{ flexDirection: "row" }}>
                                          <Iconss name="star" style={{ color: 'orange', fontSize: 20 }} />
                                          <Iconss name="star" style={{ color: 'orange', fontSize: 20 }} />
                                          <Iconss name="star" style={{ color: 'orange', fontSize: 20 }} />
                                          <Iconss name="star" style={{ color: 'orange', fontSize: 20 }} />
                                          <Iconss name="star" style={{ color: 'orange', fontSize: 20 }} />
                                       </View>
                                       <View style={{ marginRight: 10, justifyContent: 'center' }}>
                                          <Text>5.0</Text>
                                       </View>
                                    </View>
                                 </View>
                              </View>
                           </TouchableOpacity>
                        )
                     })
                  }
               </ScrollView>
            </View>
         </View>
      )
   }
}

const styles = StyleSheet.create({})
