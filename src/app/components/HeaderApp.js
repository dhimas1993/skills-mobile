import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Text, View } from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function HeaderApp({ props, tittle }) {
   // console.log('ini cfott', props);
   return (
      <View style={{ justifyContent: 'space-between', flexDirection: 'row', marginVertical: 10, alignItems: 'center', marginHorizontal: 20 }}>
         <View>
            <Icon onPress={() => props.goBack()} name='arrow-back' />
         </View>
         <View>
            <Text>{tittle}</Text>
         </View>
         <View>
            <TouchableOpacity onPress={() => props.navigate('Home')}>
               <Text>Home</Text>
            </TouchableOpacity>
         </View>
      </View>
   );
}