import { Card } from 'native-base';
import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, ScrollView, ImageBackground, Dimensions } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';

import { highlight } from '../../api/home_api'

const DEVICE_WIDTH = Dimensions.get("window").width - 30;

export default class Highlight extends Component {
   constructor(props) {
      super(props)
      this.state = {
         data: []
      }
   }

   async componentDidMount() {
      const api = await highlight()
      this.setState({
         data: api.data,
      })
      // console.log(api.data);
   }

   _toFacilitatorPage = (param) => {
      this.props.navigation.navigate('FacilitatorPage', { detail_edu: param })
   }

   renderHeader = () => {
      if (this.state.data !== []) {
         return (
            this.state.data.map(item => {
               console.log(item)
               return (
                  <Card style={{
                     width: DEVICE_WIDTH, borderRadius: 10, marginRight: 10,
                  }}>
                     <View style={{ marginHorizontal: 10, marginVertical: 10 }}>
                        <View style={{ flexDirection: 'row' }}>
                           <Image
                              style={{ height: 70, width: 70, borderRadius: 100 }}
                              source={{ uri: 'https://skills.id/admin/source/images/speaker/' + item.photo }}
                           />
                           <View style={{ marginHorizontal: 5, justifyContent: 'center', width: '80%' }}>
                              <Text style={{ fontSize: 15, fontWeight: 'bold' }}>{item.name}</Text>
                              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                 <Text style={{ fontSize: 12, marginTop: 5, fontStyle: 'italic', fontWeight: '300' }}>Profesional Speakers</Text>
                                 <TouchableOpacity
                                    onPress={() => this._toFacilitatorPage(item)}
                                    style={{ marginRight: 20, backgroundColor: '#078b6d', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 25, borderRadius: 10 }}>
                                    <Text style={{ fontSize: 13, marginTop: 5, fontWeight: '500', textAlign: 'center', color: 'white', marginBottom: 5 }}>Details</Text>
                                 </TouchableOpacity>
                              </View>
                           </View>

                        </View>
                     </View>
                     <View style={{ marginHorizontal: 10, }}>
                        <View style={{ flexDirection: 'row' }}>
                           {
                              item.workshop.map(items => {
                                 return (
                                    <View style={{ width: '33.3%', marginBottom: 10, alignItems: 'center', }}>
                                       <Image
                                          style={{
                                             width: '95%', height: 100, margin: 0, borderRadius: 10,
                                             borderColor: 'grey', borderWidth: 0.5
                                          }}
                                          source={{ uri: 'https://skills.id/admin/source/images/thumbnail/' + items.image }}
                                       />
                                       <Text numberOfLines={3} style={{ fontSize: 12, paddingTop: 10, paddingHorizontal: 1, fontWeight: '300' }}>{items.title}</Text>
                                    </View>
                                 )
                              })
                           }
                        </View>
                     </View>
                  </Card>
               )
            })
         )
      } else {
         return (
            <View>
               <Text>tidak ada  </Text>
            </View>
         )
      }
   }

   render() {
      // console.log(this.state.data[2])
      return (
         <View style={{ marginLeft: 10, marginBottom: 20 }}>
            <Text style={{ fontSize: 18, marginTop: 3, marginBottom: 15, fontWeight: 'bold' }}>Highlight</Text>
            <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} >
               {
                  this.renderHeader()
               }
            </ScrollView>
         </View>
      )
   }
}

const styles = StyleSheet.create({})





// import React, { Component } from 'react';
// import { Image, View, Text, StyleSheet, TouchableOpacity } from 'react-native';
// import { DeckSwiper, Card, Icon } from 'native-base'
// import { Alert } from 'react-native';

// import { highlight } from '../../api/home_api'

// const asu = 'https://m.ayosemarang.com/images-semarang/post/articles/2020/05/01/56273/deddy-corbuzier.jpg';
// const content1 = 'https://images.unsplash.com/photo-1587994122928-eb0b136b4d3b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=3302&q=80'
// const content2 = 'https://images.unsplash.com/photo-1587614295506-f03c0e6f5b44?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60'
// const content3 = 'https://images.unsplash.com/photo-1588981686673-5120ed7a819a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60'

// const cards = [
//    {
//       text: 'Deddy Corbuzier',
//       name: 'Profesional Magician',
//       image: 'https://m.ayosemarang.com/images-semarang/post/articles/2020/05/01/56273/deddy-corbuzier.jpg',
//    },
//    {
//       text: 'Renata Moeloek',
//       name: 'Profesional Chef',
//       image: 'https://akcdn.detik.net.id/community/media/visual/2020/01/21/a6963226-4a53-4d18-ad76-ccd50e6e991b.png?w=900&q=70',
//    },
//    {
//       text: 'Capt Vincent',
//       name: 'Pilot',
//       image: 'https://blue.kumparan.com/image/upload/fl_progressive,fl_lossy,c_fill,q_auto:best,w_640/v1559032458/zwdwnjgvqo8oowd2auxj.jpg',
//    },
// ];


// export default class DeckSwiperAdvancedExample extends Component {
//    constructor(props) {
//       super(props)
//       this.state = {
//          data: ''
//       }
//    }

//    async componentDidMount() {
//       const api = await highlight()
//       this.setState({
//          data: api.data,
//       })
//       console.log(api.data);
//    }

//    render() {
//       return (
//          <View>
//             <View style={{ padding: 10, height: 260 }}>
//                <Text style={{ paddingLeft: 5, paddingBottom: 10, fontSize: 18 }}>Promo This Week</Text>
//                <DeckSwiper
//                   ref={(c) => this._deckSwiper = c}
//                   dataSource={cards}
//                   renderEmpty={() =>
//                      <View style={{ alignSelf: "center" }}>
//                         <Text>Over</Text>
//                      </View>
//                   }
//                   renderItem={item =>
//                      <Card style={styles.card}>
//                         <View style={styles.cardContainer1}>
//                            <View style={styles.cardContainerTop}>
//                               <View style={styles.containerProfile1}>
//                                  <Image style={styles.avatar} source={{ uri: item.image }} />
//                                  <View style={{ flex: 1 }}>
//                                     <Text style={styles.facilitatorName}>{item.text}</Text>
//                                     <Text style={styles.position}>{item.name}</Text>
//                                  </View>
//                               </View>
//                               <View style={{ flex: 1 }}>
//                                  <Text style={styles.titleVideo1}>Learn The Basic of Video Editing With Adobe Premiere</Text>
//                               </View>
//                            </View>
//                            <View style={styles.cardContainer2}>
//                               <View style={styles.cardBottom}>
//                                  <View><Image style={styles.gambar1} source={{ uri: content1 }} /></View>
//                               </View>
//                               <View style={styles.cardBottomRight}>
//                                  <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
//                                     <View style={{ flex: 1, height: "60%", alignItems: 'center' }}>
//                                        <Image style={styles.gambar2} source={{ uri: content2 }} />
//                                        <Text style={{ fontSize: 9, flexWrap: "wrap", padding: 3 }}>Learn The Basic of Video Editing With Adobe Premiere</Text>
//                                     </View>
//                                     <View style={{ flex: 1, height: "60%", alignItems: 'center' }}>
//                                        <Image style={[styles.gambar2]} source={{ uri: content3 }} />
//                                        <Text style={{ fontSize: 9, flexWrap: "wrap", padding: 3 }}>Learn The Basic of Video Editing With Adobe Premiere</Text>
//                                     </View>
//                                  </View>
//                                  <View>
//                                     <TouchableOpacity
//                                        onPress={() => Alert.alert("Notifikasi", "Comming Soon")}
//                                        style={{
//                                           backgroundColor: "red", borderRadius: 50, justifyContent: 'center', alignItems: 'center', height: 25
//                                        }}>
//                                        <Text style={{ color: 'white', justifyContent: 'center', alignItems: 'center', fontSize: 13 }}>Details</Text>
//                                     </TouchableOpacity>
//                                  </View>
//                               </View>
//                            </View>
//                         </View>
//                      </Card>
//                   }
//                />
//             </View>
//             <View style={{ flexDirection: "row", flex: 1, justifyContent: 'space-between', marginHorizontal: 15, marginBottom: 20 }}>
//                <TouchableOpacity iconLeft onPress={() => this._deckSwiper._root.swipeLeft()}>
//                   <Icon name="arrow-back" />
//                </TouchableOpacity>
//                <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
//                   {
//                      cards.map((item) => {
//                         return (
//                            <TouchableOpacity style={styles.dot} />
//                         )
//                      })
//                   }
//                </View>
//                <TouchableOpacity iconRight onPress={() => this._deckSwiper._root.swipeRight()}>
//                   <Icon name="arrow-forward" />
//                </TouchableOpacity>
//             </View>
//          </View>
//       );
//    }
// }


// const styles = StyleSheet.create({
//    container: {
//       padding: 10
//    },
//    title: {
//       marginBottom: 5, fontSize: 18
//    },
//    card: {
//       borderRadius: 7, padding: 5
//    },
//    cardContainer1: {
//       height: 190, width: '100%', padding: 5
//    },
//    cardContainer2: {
//       flex: 1, flexDirection: 'row'
//    },
//    cardContainerTop: {
//       flexDirection: 'row', height: 55, justifyContent: "center"
//    },
//    containerProfile1: {
//       flex: 1, flexDirection: 'row'
//    },
//    avatar: {
//       height: 50, width: 50, borderRadius: 50
//    },
//    facilitatorName: {
//       paddingLeft: 10, paddingTop: 2, fontSize: 14
//    },
//    position: {
//       paddingLeft: 10, paddingTop: 2, fontSize: 12, flexWrap: "wrap", fontStyle: 'italic'
//    },
//    titleVideo1: {
//       paddingLeft: 10, paddingTop: 2, fontSize: 13, flex: 1
//    },
//    cardBottom: {
//       width: '50%'
//    },
//    cardBottomRight: {
//       flex: 1, justifyContent: 'space-between'
//    },
//    gambar1: {
//       height: '100%',
//       width: '95%',
//       borderRadius: 6,
//    },
//    gambar2: {
//       height: 55,
//       width: '95%',
//       borderRadius: 5
//    },
//    dot: {
//       width: 10,
//       height: 10,
//       backgroundColor: 'grey',
//       borderRadius: 100,
//       marginRight: 10,
//       marginTop: 10
//    }
// })