// import * as React from 'react';
// import { createStackNavigator } from '@react-navigation/stack';
// import { createBottomTabNavigator } from 'react-navigation-tabs';
// import Ionicons from 'react-native-vector-icons/Ionicons';

// import Home from '../page/Home/Home'
// import Cart from '../page/Cart/Cart'
// import Explore from '../page/Explore/Explore'
// import More from '../page/More/More'


// // const Home = createStackNavigator();
// // function HomeStackScreen() {
// //    return (
// //       <Home.Navigator headerMode="none">
// //          <HomeStack.Screen name="Home" component={HomeScreen} />
// //       </Home.Navigator>
// //    );
// // }

// const HomeStack = createStackNavigator(
//    { Home: Home, },
//    {
//       headerMode: 'none'
//    }
// )

// const CartStack = createStackNavigator(
//    { Cart: Cart, },
//    {
//       headerMode: 'none'
//    }
// )

// const ExploreStack = createStackNavigator(
//    { Explore: Explore, },
//    {
//       headerMode: 'none'
//    }
// )

// const MoreStack = createStackNavigator(
//    { More: More, },
//    {
//       headerMode: 'none'
//    }
// )


// const Tab = createBottomTabNavigator();
// export default function App() {
//    return (
//       <Tab.Navigator
//          screenOptions={({ route }) => ({
//             tabBarIcon: ({ focused, color, size }) => {
//                let iconName;
//                if (route.name === 'Home') {
//                   iconName = focused
//                      ? 'md-home'
//                      : 'md-home';
//                } else if (route.name === 'Cart') {
//                   iconName = focused
//                      ? 'md-person'
//                      : 'md-person';
//                } else if (route.name === 'Explore') {
//                   iconName = focused
//                      ? 'md-mail-open'
//                      : 'md-mail';
//                } else if (route.name === 'More') {
//                   iconName = focused
//                      ? 'md-mail-open'
//                      : 'md-mail';
//                }
//                // You can return any component that you like here!
//                return <Ionicons name={iconName} size={size} color={color} />;
//             },
//          })}
//          tabBarOptions={{
//             activeTintColor: 'tomato',
//             inactiveTintColor: 'gray',
//          }}
//       >
//          <Tab.Screen name="Home" component={HomeStack} />
//          <Tab.Screen name="Profile" component={CartStack} />
//          <Tab.Screen name="Message" component={ExploreStack} />
//          <Tab.Screen name="More" component={MoreStack} />
//       </Tab.Navigator>
//    );
// }