import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView, ImageBackground, TouchableOpacity, SafeAreaView, Alert } from 'react-native'

import { highlight } from '../../api/home_api'

const coming_soon = 'https://images.squarespace-cdn.com/content/v1/55ca787ae4b07d9498906d9e/1551490529737-C7C2LZQ8MSFLT8L632H4/ke17ZwdGBToddI8pDm48kPHgPSpJs3pqpkUZU93_mvpZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PInndiFr6ALsCf3uCDBRk4eJzsF3rQ925VZ6DZqxvBZkQKMshLAGzx4R3EDFOm1kBS/coming-soon-neon-sign-coming-soon-badge-in-vector-21133321.jpg?format=750w'

export default class Product extends Component {
   constructor(props) {
      super(props)
      this.state = {
         data: []
      }
   }

   async componentDidMount() {
      const api = await highlight()
      this.setState({
         data: api.data,
      })
   }

   renderList = () => {
      const data = [
         {
            id_speaker: "12",
            name: "Theresia Lestari CFP, CPC, CBA, CPFM, (Coach & Trainer)",
            photo: "cropped-IMG_20171102_140147-950x850-850x850.jpg",
            data: [
               {
                  title: "(Webinar Class) Belajar Tehnik Negosiasi",
                  image: "Belajar-Tehnik-Negosiasi.jpg"
               },
               {
                  title: "(Webinar Class) Belajar Mengenal Branding",
                  image: "Belajar-Mengenal-Branding.jpg"
               },
               {
                  title: "(Webinar Class) Belajar Menetapkan Skala Prioritas Tanggung Jawab",
                  image: "Belajar-Menetapkan-Skala-Prioritas-Tanggung-Jawab.jpg"
               }
            ]
         },
         {
            id_speaker: "64",
            name: "Miss Via (Millenials Trainer & Public Speaking Expert)",
            photo: "via.jpg",
            data: [
               {
                  title: "Webinar: Cara Berbicara Di Depan Umum Saat Wawancara Kerja (Public Speaking for Job Interview)",
                  image: "Cara-Berbicara-Di-Depan-Umum-Saat-Wawancara-Kerja-Public-Speaking-for-Job-Interview.png"
               },
               {
                  title: "(Webinar) Public Speaking for Job Interview",
                  image: "Public-Speaking-for-Job-Interview.png"
               },
               {
                  title: "(Webinar) Public Speaking For Professional Career",
                  image: "Public-Speaking-For-Professional-Career.jpg"
               }
            ]
         },
         {
            id_speaker: "80",
            name: "Grace Anggraeni",
            photo: "Profile_Photo.jpg",
            data: [
               {
                  title: "Learn The Art of Brush Pen Lettering and Watercolour Painting with Valentine Theme",
                  image: "Learn-The-Art-of-Brush-Pen-Lettering-and-Watercolour-Painting-with-Valentine-Theme.jpg"
               },
               {
                  title: "Learn How To Paint Acrylic Waves of Love (Art Date)",
                  image: "Learn-How-To-Paint-Acrylic-Waves-of-Love-Art-Date.jpg"
               },
               {
                  title: "(Acrylic Painting) Learn How to Make Galaxy Heart",
                  image: "Acrylic-Painting-Learn-How-to-Make-Galaxy-Heart.jpg"
               }
            ]
         }
      ]

      if (this.state.data !== []) {
         console.log('renderlist', this.state.data)
         this.state.data.map((item) => {
            return (
               <View style={{ width: 140, height: 190, marginHorizontal: 5 }} >
                  <TouchableOpacity onPress={() => Alert.alert("Notifikasi", "Comming Soon")} pagingEnabled>
                     <ImageBackground source={{ uri: item.image }} style={{ width: '100%', height: '100%' }} imageStyle={{ borderRadius: 10 }}>
                        <View style={{ width: '100%', flex: 1, backgroundColor: 'black', borderRadius: 10, opacity: 0.5 }}></View>
                        <View style={{ color: "white", position: 'absolute', justifyContent: 'center', marginTop: 125, padding: 10 }} >
                           <Text numberOfLines={2} style={{ fontWeight: 'bold', fontSize: 12, color: 'white' }}>{item.name}</Text>
                           <Text style={{ fontSize: 12, color: 'white' }}>Rp. {item.harga}</Text>
                           <Text style={{ fontWeight: 'bold', fontSize: 10, color: 'white' }}>{item.author}</Text>
                        </View>
                     </ImageBackground>
                  </TouchableOpacity>
               </View>
            )
         })
      } else {
         console.log('render data', this.state.data)
         data.map((item) => {
            return (
               <View style={{ width: 140, height: 190, marginHorizontal: 5 }} >
                  <TouchableOpacity onPress={() => Alert.alert("Notifikasi", "Comming Soon")} pagingEnabled>
                     <ImageBackground source={{ uri: item.image }} style={{ width: '100%', height: '100%' }} imageStyle={{ borderRadius: 10 }}>
                        <View style={{ width: '100%', flex: 1, backgroundColor: 'black', borderRadius: 10, opacity: 0.5 }}></View>
                        <View style={{ color: "white", position: 'absolute', justifyContent: 'center', marginTop: 125, padding: 10 }} >
                           <Text numberOfLines={2} style={{ fontWeight: 'bold', fontSize: 12, color: 'white' }}>{item.name}</Text>
                           <Text style={{ fontSize: 12, color: 'white' }}>Rp. {item.text}</Text>
                           <Text style={{ fontWeight: 'bold', fontSize: 10, color: 'white' }}>{item.author}</Text>
                        </View>
                     </ImageBackground>
                  </TouchableOpacity>
               </View>
            )
         })
      }
   }

   render() {

      const data = [
         {
            id_speaker: "250.000",
            name: "Cd Sales Vol.1",
            photo: "Dhimas Hertianto",
            data: [
               {
                  title: "(Webinar Class) Belajar Tehnik Negosiasi",
                  image: "Belajar-Tehnik-Negosiasi.jpg"
               },
               {
                  title: "(Webinar Class) Belajar Mengenal Branding",
                  image: "Belajar-Mengenal-Branding.jpg"
               },
               {
                  title: "(Webinar Class) Belajar Menetapkan Skala Prioritas Tanggung Jawab",
                  image: "Belajar-Menetapkan-Skala-Prioritas-Tanggung-Jawab.jpg"
               }
            ]
         },
         {
            id_speaker: "250.000",
            name: "Cd Sales Vol. 2",
            photo: "Dhimas Hertianto",
            data: [
               {
                  title: "Webinar: Cara Berbicara Di Depan Umum Saat Wawancara Kerja (Public Speaking for Job Interview)",
                  image: "Cara-Berbicara-Di-Depan-Umum-Saat-Wawancara-Kerja-Public-Speaking-for-Job-Interview.png"
               },
               {
                  title: "(Webinar) Public Speaking for Job Interview",
                  image: "Public-Speaking-for-Job-Interview.png"
               },
               {
                  title: "(Webinar) Public Speaking For Professional Career",
                  image: "Public-Speaking-For-Professional-Career.jpg"
               }
            ]
         },
         {
            id_speaker: "80",
            name: "Cd Sales Vol. 3",
            photo: "Dhimas Hertianto",
            data: [
               {
                  title: "Learn The Art of Brush Pen Lettering and Watercolour Painting with Valentine Theme",
                  image: "Learn-The-Art-of-Brush-Pen-Lettering-and-Watercolour-Painting-with-Valentine-Theme.jpg"
               },
               {
                  title: "Learn How To Paint Acrylic Waves of Love (Art Date)",
                  image: "Learn-How-To-Paint-Acrylic-Waves-of-Love-Art-Date.jpg"
               },
               {
                  title: "(Acrylic Painting) Learn How to Make Galaxy Heart",
                  image: "Acrylic-Painting-Learn-How-to-Make-Galaxy-Heart.jpg"
               }
            ]
         }
      ]

      return (
         <SafeAreaView>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 10 }}>
               <Text style={{ fontSize: 18, marginTop: 3, marginBottom: 10 }}>Hot Product</Text>
               <TouchableOpacity >
                  <Text style={{ fontSize: 13, marginTop: 10, marginRight: 10, fontWeight: 'bold' }}>View All</Text>
               </TouchableOpacity>
            </View>
            <View style={{ paddingBottom: 10 }}>
               <View style={{ backgroundColor: 'orange', flex: 0, justifyContent: 'center', paddingVertical: 20, borderBottomLeftRadius: 100, borderTopRightRadius: 100 }}>
                  <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}
                     style={{ marginHorizontal: 5 }}
                  >
                     {
                        this.state.data == '' ?
                           this.state.data.map((item) => {
                              return (
                                 <View style={{ width: 140, height: 190, marginHorizontal: 5 }} >
                                    <TouchableOpacity onPress={() => Alert.alert("Notifikasi", "Fitur ini Comming Soon")} pagingEnabled>
                                       <ImageBackground source={{ uri: 'https://skills.id/admin/source/images/speaker/' + item.photo }} style={{ width: '100%', height: '100%' }} imageStyle={{ borderRadius: 10 }}>
                                          <View style={{ width: '100%', flex: 1, backgroundColor: 'black', borderRadius: 10, opacity: 0.5 }}></View>
                                          <View style={{ color: "white", position: 'absolute', justifyContent: 'center', marginTop: 125, padding: 10 }} >
                                             <Text numberOfLines={2} style={{ fontWeight: 'bold', fontSize: 12, color: 'white' }}>{item.name}</Text>
                                             <Text style={{ fontSize: 12, color: 'white' }}>Rp. {item.id_speaker}</Text>
                                             <Text style={{ fontWeight: 'bold', fontSize: 10, color: 'white' }}>{item.text}</Text>
                                          </View>
                                       </ImageBackground>
                                    </TouchableOpacity>
                                 </View>
                              )
                           }) :
                           data.map((item) => {
                              return (
                                 <View style={{ width: 210, height: 190, marginHorizontal: 5 }} >
                                    <TouchableOpacity onPress={() => Alert.alert("Notifikasi", "Fitur ini Comming Soon")} pagingEnabled>
                                       <ImageBackground source={{ uri: coming_soon }} style={{ width: '100%', height: '100%' }} imageStyle={{ borderRadius: 10 }}>
                                          <View style={{ width: '100%', flex: 1, backgroundColor: 'black', borderRadius: 10, opacity: 0.5 }}></View>
                                          <View style={{ color: "white", position: 'absolute', justifyContent: 'center', marginTop: 125, padding: 10 }} >
                                             <Text numberOfLines={2} style={{ fontWeight: 'bold', fontSize: 12, color: 'white' }}>{item.name}</Text>
                                             <Text style={{ fontSize: 12, color: 'white' }}>Rp. {item.photo}</Text>
                                             <Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white' }}>Rp. {item.id_speaker}</Text>
                                          </View>
                                       </ImageBackground>
                                    </TouchableOpacity>
                                 </View>
                              )
                           })
                     }
                  </ScrollView>
               </View>
            </View>
         </SafeAreaView>
      )
   }
}

const styles = StyleSheet.create({})