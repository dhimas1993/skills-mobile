/* eslint-disable react-native/no-inline-styles */
/* eslint-disable eqeqeq */
/* eslint-disable no-unused-vars */
import React from 'react';
import {TouchableOpacity, Text, Alert} from 'react-native';
import {View, Icon} from 'native-base';
import {connect} from 'react-redux';
// import Ionicons from 'react-native-vector-icons/FontAwesome5';

import {forceLogout} from '../../../store/auth/action';
import {getCartByIdUser} from '../../../api/cart_api';
import {SafeAreaView} from 'react-navigation';
import {ScrollView} from 'react-native-gesture-handler';

class More extends React.Component {
  constructor(props) {
    // tarik data dari page redux
    // console.log('data redux', props.user.auth);
    super(props);
    this.state = {
      id_user: this.props.user.auth.data,
      data_user: [],
      cart: '',
    };
  }

  componentDidMount() {
    const data = this.props.user.auth.data;
    console.log('mante', data);
    this.getCart();
  }

  onLogout = async () => {
    await this.props.forceLogout();
    // this.props.navigation.navigate('Home')
  };

  getCart = async () => {
    const data = {
      id_user: this.state.id_user,
    };

    const api = await getCartByIdUser(data);
    this.setState({
      cart: api.data.data,
      // quant: api.data.data.quant
    });
    console.log('cart', api.data.data);
  };

  ButtonAlert = () =>
    Alert.alert(
      'Notifiikasi',
      'Apakah anda yakin untuk keluar aplikasi Skills.ID ?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => this.onLogout()},
      ],
      {cancelable: false},
    );

  dashboardEducator = () => {
    const {as_educator, as_learner} = this.props.user.auth.data;

    if (as_educator == 1 && as_learner == 0) {
      return (
        <>
          <Text
            style={{
              paddingHorizontal: 5,
              paddingBottom: 10,
              fontWeight: 'bold',
              fontSize: 18,
            }}>
            Dashboard
          </Text>
          <TouchableOpacity
            onPress={() => Alert.alert('Notifikasi', 'Comming Soon')}
            // onPress={() => this.props.navigation.navigate('MyVideo')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>My Video</Text>
              {/* <Ionicons name="arrow-forward-outline" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('SubmitWorkshop', {
                data: this.state.id_user.id_user,
              })
            }
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>Submit Workshop</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Gallery')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>Submit Galery</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('ManageWorkshop')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>Manage workshop</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Trainer')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>Submit Trainer</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
        </>
      );
    } else if (as_learner == 1 && as_educator == 0) {
      return (
        <>
          <Text
            style={{
              paddingHorizontal: 5,
              paddingBottom: 10,
              fontWeight: 'bold',
              fontSize: 18,
            }}>
            Dashboard
          </Text>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Wishlist')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>My Favorite Workshop</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Cart')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>My Cart</Text>
              {this.state.cart ? (
                <View
                  style={{
                    backgroundColor: 'red',
                    height: 10,
                    width: 10,
                    borderRadius: 50,
                  }}
                />
              ) : null}
            </View>
          </TouchableOpacity>
          {/* <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('Checkout')}
                  style={{ marginBottom: 10 }}>
                  <View style={{ flexDirection: 'row', width: '100%', height: 50, backgroundColor: 'white', borderRadius: 10, elevation: 5, justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 10 }}>
                     <Text>My Workshop</Text>
                     <Icon name="add" style={{ fontSize: 20 }} />
                  </View>
               </TouchableOpacity> */}
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('ReferralPage')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>My Referral</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
          {/* <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('Checkout')}
                  style={{ marginBottom: 10 }}>
                  <View style={{ flexDirection: 'row', width: '100%', height: 50, backgroundColor: 'white', borderRadius: 10, elevation: 5, justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 10 }}>
                     <Text>Checkout</Text>
                  </View>
               </TouchableOpacity> */}
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Pay')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>Payment Confirmation</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
        </>
      );
    } else if (as_learner == 0 && as_educator == 1) {
      return (
        <>
          <Text
            style={{
              paddingHorizontal: 5,
              paddingBottom: 10,
              fontWeight: 'bold',
              fontSize: 18,
            }}>
            Dashboard
          </Text>
          <TouchableOpacity
            onPress={() => Alert.alert('Notifikasi', 'Comming Soon')}
            // onPress={() => this.props.navigation.navigate('MyVideo')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>My Video</Text>
              {/* <Ionicons name="arrow-forward-outline" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('SubmitWorkshop', {
                data: this.state.id_user.id_user,
              })
            }
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>Submit Workshop</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Gallery')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>Submit Galery</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('ManageWorkshop')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>Manage workshop</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Trainer')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>Submit Trainer</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
        </>
      );
    } else if (as_learner == 1 && as_educator == 1) {
      return (
        <>
          <Text
            style={{
              paddingHorizontal: 5,
              paddingBottom: 10,
              fontWeight: 'bold',
              fontSize: 18,
            }}>
            Dashboard
          </Text>
          <TouchableOpacity
            onPress={() => Alert.alert('Notifikasi', 'Comming Soon')}
            // onPress={() => this.props.navigation.navigate('MyVideo')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>My Video</Text>
              {/* <Ionicons name="arrow-forward-outline" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('SubmitWorkshop', {
                data: this.state.id_user.id_user,
              })
            }
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>Submit Workshop</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Gallery')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>Submit Galery</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('ManageWorkshop')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>Manage workshop</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Trainer')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>Submit Trainer</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Wishlist')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>My Favorite Workshop</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Cart')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>My Cart</Text>
              {this.state.cart ? (
                <View
                  style={{
                    backgroundColor: 'red',
                    height: 10,
                    width: 10,
                    borderRadius: 50,
                  }}
                />
              ) : null}
            </View>
          </TouchableOpacity>
          {/* <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('Checkout')}
                  style={{ marginBottom: 10 }}>
                  <View style={{ flexDirection: 'row', width: '100%', height: 50, backgroundColor: 'white', borderRadius: 10, elevation: 5, justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 10 }}>
                     <Text>My Workshop</Text>
                     <Icon name="add" style={{ fontSize: 20 }} />
                  </View>
               </TouchableOpacity> */}
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('ReferralPage')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>My Referral</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
          {/* <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('Checkout')}
                  style={{ marginBottom: 10 }}>
                  <View style={{ flexDirection: 'row', width: '100%', height: 50, backgroundColor: 'white', borderRadius: 10, elevation: 5, justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 10 }}>
                     <Text>Checkout</Text>
                  </View>
               </TouchableOpacity> */}
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Pay')}
            style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                height: 50,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text>Payment Confirmation</Text>
              {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
            </View>
          </TouchableOpacity>
        </>
      );
    }
  };

  renderOtherLogin = () => {
    const {id_user} = this.props.user.auth.data;
    if (id_user) {
      return (
        <TouchableOpacity
          onPress={() => this.ButtonAlert()}
          style={{
            flexDirection: 'row',
            width: '100%',
            height: 50,
            backgroundColor: 'white',
            borderRadius: 10,
            elevation: 5,
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingHorizontal: 10,
          }}>
          <Text>Logout</Text>
          {/* <Ionicons name="play" style={{ fontSize: 20 }} /> */}
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('LoginPage')}
          style={{
            flexDirection: 'row',
            width: '100%',
            height: 50,
            backgroundColor: 'white',
            borderRadius: 10,
            elevation: 5,
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingHorizontal: 10,
          }}>
          <Text>Login</Text>
          {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
        </TouchableOpacity>
      );
    }
  };

  render() {
    return (
      <SafeAreaView>
        <ScrollView style={{height: '100%'}}>
          <View style={{paddingHorizontal: 15, paddingTop: 10}}>
            <Text
              style={{
                paddingHorizontal: 5,
                paddingVertical: 10,
                fontWeight: 'bold',
                fontSize: 18,
              }}>
              Account
            </Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('MyProfile')}
              style={{marginBottom: 10}}>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  height: 50,
                  backgroundColor: 'white',
                  borderRadius: 10,
                  elevation: 5,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  paddingHorizontal: 10,
                }}>
                <Text>My Profile</Text>

                {/* <View style={{ backgroundColor: 'red', height: 10, width: 10, borderRadius: 50 }} /> */}
              </View>
            </TouchableOpacity>
          </View>

          <View style={{paddingHorizontal: 15, paddingTop: 10}}>
            {this.dashboardEducator()}
          </View>

          <View
            style={{paddingHorizontal: 15, paddingTop: 10, paddingBottom: 10}}>
            <Text
              style={{
                paddingHorizontal: 5,
                paddingVertical: 5,
                fontWeight: 'bold',
                fontSize: 18,
              }}>
              Other
            </Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Privacy')}
              style={{marginBottom: 10}}>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  height: 50,
                  backgroundColor: 'white',
                  borderRadius: 10,
                  elevation: 5,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  paddingHorizontal: 10,
                }}>
                <Text>Privacy</Text>
                {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Help')}
              style={{marginBottom: 10}}>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  height: 50,
                  backgroundColor: 'white',
                  borderRadius: 10,
                  elevation: 5,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  paddingHorizontal: 10,
                }}>
                <Text>Help</Text>
                {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('About')}
              style={{marginBottom: 10}}>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  height: 50,
                  backgroundColor: 'white',
                  borderRadius: 10,
                  elevation: 5,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  paddingHorizontal: 10,
                }}>
                <Text>About</Text>
                {/* <Icon name="add" style={{ fontSize: 20 }} /> */}
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={{marginBottom: 10}}>
              {this.renderOtherLogin()}
            </TouchableOpacity>

            {/*
                  <TouchableOpacity
                     onPress={() => this.props.navigation.navigate('Verified')}
                     style={{ marginBottom: 10 }}>
                     <View style={{ flexDirection: 'row', width: '100%', height: 50, backgroundColor: 'white', borderRadius: 10, elevation: 5, justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 10 }}>
                        <Text>Verified</Text>
                     </View>
                  </TouchableOpacity> */}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.auth,
  };
};

export default connect(mapStateToProps, {forceLogout})(More);
