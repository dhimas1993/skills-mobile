// import { loadPartialConfig } from '@babel/core';
import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, Image, ImageBackground, Alert, SafeAreaView, RefreshControl, Modal } from 'react-native'
import { Icon } from 'native-base';

export default class Card extends Component {
   constructor(props) {
      // console.log("card", props.fun);
      super(props);
      this.state = {
         card: this.props.items
      };
      // console.log('props card', this.state.qty);
   }

   // qty_plus = () => {
   //    this.props.updateQty()
   //    this.setState({
   //       qty: this.state.qty + 1
   //    })
   // }


   render() {
      // console.log('card', props);
      const source = 'https://skills.id/admin/source/images/thumbnail/'
      const { id_cart, id_user, id_workshop, image, multi_tanggal, name, price, promo_price, quant, title } = this.state.card
      return (
         <View style={{ marginHorizontal: 10, alignSelf: 'center', flexDirection: 'row', minHeight: 120, marginBottom: 10 }}>
            <TouchableOpacity
               style={{ borderRadius: 10, flex: 1 }}
            >
               <View style={{ borderRadius: 10, flex: 1, flexDirection: 'row', backgroundColor: 'white' }}>
                  <View style={{ flex: 2.5 }}>
                     <Image
                        style={{ flex: 1, borderTopLeftRadius: 10, borderBottomLeftRadius: 10 }}
                        source={{ uri: source + image }} />
                  </View>
                  <View style={{ flex: 5, margin: 10, justifyContent: 'space-between' }}>
                     <View>
                        <Text numberOfLines={2} style={{ fontSize: 15, textAlign: 'left', fontWeight: 'bold' }}> {title} </Text>
                        <Text numberOfLines={1} style={{ fontSize: 12, color: '#078b6d', fontStyle: 'italic', marginBottom: 5 }}>Author : {name} </Text>
                        <Text style={{ fontSize: 12 }}> Rp. {parseInt(price).toLocaleString("IN")} / Person </Text>
                        <View style={{ flexDirection: 'row' }}>
                           <Text style={{ fontSize: 15, flex: 1 }}>Total</Text>
                           {/* <Text style={{ fontSize: 12 }}> Rp. {parseInt(price).toLocaleString("IN")} / Person </Text> */}
                        </View>
                     </View>

                  </View>
                  <View style={{ margin: 5, justifyContent: 'space-around', padding: 5, borderRadius: 5, alignItems: 'center', borderStyle: 'solid', borderWidth: 1, borderColor: 'lightgrey', marginRight: 5, width: '10%' }}>
                     <TouchableOpacity onPress={() => this.props.fun(id_cart)} style={{ width: '100%' }}>
                        <Icon style={{ fontSize: 20, textAlign: 'center' }} name="add" />
                     </TouchableOpacity>
                     <Text>{Number(quant)}</Text>
                     <TouchableOpacity onPress={() => { this.setState({ qty: this.state.qty - 1 }) }} style={{ width: '100%' }}>
                        <Icon style={{ fontSize: 20, textAlign: 'center' }} name="ios-remove" />
                     </TouchableOpacity>
                  </View>
               </View>
            </TouchableOpacity>
         </View>
      )
   }
}

const styles = StyleSheet.create({})

// return (
//    <View style={{ marginHorizontal: 10, alignSelf: 'center', flexDirection: 'row' }}>
//       <TouchableOpacity
//          style={{ borderRadius: 10, flex: 1 }}>
//          <Card rounded style={{ borderRadius: 10, flex: 1, flexDirection: 'row' }}>
//             <View style={{ flex: 2.5 }}>
//                <Image
//                   style={{ flex: 1, borderTopLeftRadius: 10, borderBottomLeftRadius: 10 }}
//                   source={{ uri: source + image }} />
//             </View>
//             <View style={{ flex: 5, margin: 10, justifyContent: 'space-between' }}>
//                <View>
//                   <Text numberOfLines={2} style={{ fontSize: 15, textAlign: 'left', fontWeight: 'bold' }}> {title} </Text>
//                   <View>
//                      <Text numberOfLines={1} style={{ fontSize: 12, color: '#078b6d', fontStyle: 'italic', marginBottom: 5 }}>Author : {name} </Text>
//                   </View>
//                   {/* {
//                      item.multi_tanggal !== undefined ?
//                         this.renderTanggal(item.multi_tanggal.split(",")) : null
//                   } */}
//                   <Text style={{ fontSize: 12 }}> Rp. {parseInt(price).toLocaleString("IN")} / Person </Text>
//                   <View style={{ flexDirection: 'row' }}>
//                      <Text style={{ fontSize: 15, flex: 1 }}>Total</Text>
//                      {/* {
//                         item.multi_tanggal !== undefined ?
//                            <Text style={{ fontSize: 15 }}>Rp {Number(item.price * item.multi_tanggal.split(",").length * item.quant).toLocaleString('IN')} </Text>
//                            : null
//                      }
//                      {
//                         item.multi_tanggal !== undefined ?
//                            this.total(Number(item.price * item.multi_tanggal.split(",").length * item.quant))
//                            : null
//                      } */}
//                   </View>
//                </View>

//             </View>
//             <View style={{ margin: 5, justifyContent: 'space-around', padding: 5, borderRadius: 5, alignItems: 'center', borderStyle: 'solid', borderWidth: 1, borderColor: 'lightgrey', marginRight: 5, width: '10%' }}>
//                <TouchableOpacity
//                   // onPress={() => this._onPressAdd(item)} 
//                   style={{ width: '100%' }}>
//                   <Icon style={{ fontSize: 20, textAlign: 'center' }} name="add" />
//                </TouchableOpacity>
//                <Text>{quant}</Text>
//                <TouchableOpacity
//                   // onPress={() => this._onPressMin(item)} 
//                   style={{ width: '100%' }}>
//                   <Icon style={{ fontSize: 20, textAlign: 'center' }} name="ios-remove" />
//                </TouchableOpacity>
//             </View>
//          </Card>
//       </TouchableOpacity>
//    </View>
// )

