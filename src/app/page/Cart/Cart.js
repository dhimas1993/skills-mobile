/* eslint-disable no-unused-vars */
/* eslint-disable no-sequences */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable eqeqeq */
/* eslint-disable radix */
/* eslint-disable no-lone-blocks */
import React, {Component} from 'react';
import {
  BackHandler,
  Text,
  StyleSheet,
  View,
  SafeAreaView,
  ScrollView,
  Alert,
  TouchableOpacity,
  Image,
  ImageBackground,
  Modal,
  TextInput,
  RefreshControl,
} from 'react-native';
import {CheckBox} from 'native-base';
import {connect} from 'react-redux';
import {NavigationEvents} from 'react-navigation';

import Icons from 'react-native-vector-icons/FontAwesome5';
import {getCartByIdUser} from '../../../api/cart_api';
import {
  kodePromo,
  kodeVoucher,
  kodeReferral,
  kodePrakerja,
} from '../../../api/promo_api';
import LoginPage from '../../screen/login/loginScreen/LoginPage';

class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      cart: '',
      grand_total: [],
      modalVisible: true,
      kode_promo: '',
      kode_voucher: '',
      kode_ecommerce: '',
      kode_referral: '',
      _voucher: '',
      _promo: '',
      _ecommerce: '',
      price_ecommerce: 0,
      _referral: '',
      status: false,
    };
  }

  _onRefresh() {
    this.setState({refreshing: true});
    this.getCart().then(() => {
      this.setState({refreshing: false});
    });
    console.log(this.state.cart);
  }

  componentDidMount() {
    console.log('cdm');
    this.refresh();
    this.getCart();
    this.totalQty();
  }

  refresh = () => {
    const status = this.state.status;
    console.log('refresh', !status);
    this.setState({
      status: !status,
    });
  };

  onBackButton = () => {
    // eslint-disable-next-line no-alert
    alert('Tombol back di tekan');
    // Menonaktifkan default function (kembali ke halaman sebelumnya)
    return true;
  };

  alertButton(item) {
    Alert.alert('Alert', 'Delete this cart ?', [
      {text: 'NO', onPress: () => this.getCart(), style: 'cancel'},
      {text: 'YES', onPress: () => this.onButtonDelete(item)},
    ]);
  }

  onButtonDelete = async (item) => {
    // console.log(item);

    {
      this.getCart();
    }
  };

  getCart = async () => {
    const data = {
      id_user: this.props.user.auth.data.id_user,
    };

    const api = await getCartByIdUser(data);
    this.setState({
      cart: api.data.data,
      // quant: api.data.data.quant
    });
    // console.log('cart', api.data.data);
  };

  plus_qty = (id_cart) => {
    let cart = this.state.cart;
    // console.log('card', cart);
    let newCart = cart.map((item) => {
      if (item.id_cart == id_cart) {
        item.quant = parseInt(item.quant) + 1;
      }
      return cart;
    });
    this.setState({
      cart: newCart[0],
    });
  };

  min_qty = (id_cart) => {
    let cart = this.state.cart;
    // console.log('card', cart);
    let newCart = cart.map((item) => {
      if (item.id_cart == id_cart) {
        item.quant = parseInt(item.quant) - 1;
        console.log(item.quant);
        if (item.quant < 1) {
          this.alertButton(id_cart);
        }
      }
      return cart;
    });
    this.setState({
      cart: newCart[0],
    });
  };

  totalQty = () => {
    var total = 0;

    if (this.state.cart) {
      for (let i = 0; i < this.state.cart.length; i++) {
        const jumlah = this.state.cart[i].quant;
        total = total + Number(jumlah);
      }
      return total;
    } else {
      return total;
    }
  };

  Disc_voucher = () => {
    var voucher = 0;
    const voucher_state = this.state._voucher.potongan;

    // console.log(Number(voucher_state));

    if (voucher_state) {
      return (voucher += Number(voucher_state));
    } else {
      return voucher;
    }
  };

  Disc_promo = () => {
    var Nominal = 0;
    var _Persen = 0;
    const total_revenue = this.totalCart();
    const promo = this.state._promo;

    if (promo && promo.tipe_potongan == 'Nominal') {
      Nominal += Number(promo.potongan);
      return Nominal;
    } else if (promo && promo.tipe_potongan == 'Persen') {
      var Persen = [];

      Persen.push(promo.persen);
      Persen.push(promo.maks_potongan);

      var a = Number(total_revenue) * Number(Persen[0]);
      if (a < Number(promo.maks_potongan)) {
        return (_Persen += Number(total_revenue) * Number(promo.persen));
      } else {
        return (_Persen += Number(promo.maks_potongan));
      }
    } else {
      return Nominal;
    }
  };

  Total_disc = () => {
    var total = 0;
    const disc_voucher = this.Disc_voucher();
    const disc_promo = this.Disc_promo();
    const disc_ecommerce = Number(this.state.price_ecommerce);

    // console.log(disc_voucher, disc_promo, disc_ecommerce);
    // if(disc_voucher)
    // if ()
    return (total += disc_voucher + disc_promo + disc_ecommerce);
  };

  totalCart() {
    var sub = [];
    var total = 0;
    var cart = this.state.cart;

    if (cart) {
      cart.map((item) => {
        sub.push(
          item.price * item.quant * item.multi_tanggal.split(',').length,
        );
      });
      for (var i = 0; i < sub.length; i++) {
        total += sub[i];
      }
      return total;
    } else {
      return total;
    }
  }

  renderTanggal = (multi_tanggal) => {
    const multi = multi_tanggal.split(',');
    const ada = this.state.cart.multi_tanggal;

    // console.log('tanggal', ada);
    if (ada == undefined) {
      return multi.map((item) => {
        // console.log("asu", item);
        return (
          <View
            style={{
              backgroundColor: '#078b6d',
              padding: 5,
              borderRadius: 25,
              paddingHorizontal: 5,
              marginBottom: 5,
              marginHorizontal: 5,
              paddingTop: 7,
            }}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
              <View style={{flex: 1}}>
                <CheckBox checked={true} />
              </View>
              <Text style={{color: 'white', flex: 4}}>{item}</Text>
            </View>
          </View>
        );
      });
    }
  };

  renderCard() {
    const source = 'https://skills.id/admin/source/images/thumbnail/';
    return this.state.cart.map((item) => {
      const total_tanggal = item.multi_tanggal.split(',').length;

      // console.log(total_tanggal);
      return (
        <View
          style={{
            alignSelf: 'center',
            flexDirection: 'row',
            Height: 100,
            width: '100%',
            marginBottom: 10,
            paddingHorizontal: 15,
          }}>
          <TouchableOpacity
            onPress={() => Alert.alert('Title', item.title)}
            style={{
              borderRadius: 0,
              flex: 1,
              shadowColor: '#000',
              shadowOffset: {width: 0, height: 2},
              shadowOpacity: 0.5,
              shadowRadius: 5,
              elevation: 2,
            }}>
            <View
              style={{
                borderRadius: 5,
                flex: 1,
                flexDirection: 'row',
                backgroundColor: 'white',
              }}>
              <View style={{flex: 2}}>
                <Image
                  style={{
                    flex: 1,
                    borderTopLeftRadius: 5,
                    borderBottomLeftRadius: 5,
                  }}
                  source={{uri: source + item.image}}
                />
              </View>
              <View
                style={{flex: 5, margin: 10, justifyContent: 'space-between'}}>
                <View>
                  <Text
                    numberOfLines={2}
                    style={{
                      fontSize: 13,
                      textAlign: 'left',
                      fontWeight: '600',
                      marginBottom: 10,
                    }}>
                    {' '}
                    {item.title}{' '}
                  </Text>
                  {/* <Text numberOfLines={1} style={{ fontSize: 12, color: '#078b6d', fontStyle: 'italic', marginBottom: 5 }}>Author : {item.name} </Text> */}
                  {/* {
                                 this.renderTanggal(item.multi_tanggal)
                              }
                              <Text style={{ fontSize: 12 }}> Rp. {parseInt(item.price).toLocaleString("IN")} / Person </Text> */}
                  <View style={{flexDirection: 'row'}}>
                    <Text style={{fontSize: 12, flex: 1}}>Total :</Text>
                    <Text style={{fontSize: 12}}>
                      {' '}
                      Rp.{' '}
                      {parseInt(
                        item.price * item.quant * total_tanggal,
                      ).toLocaleString('IN')}
                    </Text>
                  </View>
                </View>
              </View>

              <View
                style={{
                  margin: 5,
                  justifyContent: 'space-around',
                  padding: 5,
                  marginRight: 5,
                  width: '30%',
                }}>
                <TouchableOpacity
                  onPress={() => this.alertButton(item.id_cart)}>
                  <Text
                    style={{
                      padding: 2,
                      paddingBottom: 10,
                      color: 'red',
                      textAlign: 'right',
                    }}>
                    Delete
                  </Text>
                </TouchableOpacity>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    flex: 1,
                    borderRadius: 5,
                    alignItems: 'center',
                  }}>
                  {item.quant <= 1 ? (
                    <TouchableOpacity style={{width: '100%'}}>
                      <Icons
                        style={{
                          fontSize: 20,
                          textAlign: 'center',
                          color: '#078b6d',
                          marginHorizontal: 30,
                        }}
                        name="minus-circle"
                      />
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      onPress={() => this.min_qty(item.id_cart)}
                      style={{width: '100%'}}>
                      <Icons
                        style={{
                          fontSize: 20,
                          textAlign: 'center',
                          color: '#078b6d',
                        }}
                        name="minus-circle"
                      />
                    </TouchableOpacity>
                  )}
                  <Text>{Number(item.quant)}</Text>
                  <TouchableOpacity
                    onPress={() => this.plus_qty(item.id_cart)}
                    style={{width: '100%'}}>
                    <Icons
                      style={{
                        fontSize: 20,
                        textAlign: 'center',
                        color: '#078b6d',
                      }}
                      name="plus-circle"
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      );
    });
  }

  setModalVisible = (visible) => {
    this.setState({modalVisible: visible});
  };

  integer_to_roman(num) {
    if (typeof num !== 'number') {
      return false;
    }

    var digits = String(+num).split(''),
      key = [
        '',
        'C',
        'CC',
        'CCC',
        'CD',
        'D',
        'DC',
        'DCC',
        'DCCC',
        'CM',
        '',
        'X',
        'XX',
        'XXX',
        'XL',
        'L',
        'LX',
        'LXX',
        'LXXX',
        'XC',
        '',
        'I',
        'II',
        'III',
        'IV',
        'V',
        'VI',
        'VII',
        'VIII',
        'IX',
      ],
      roman_num = '',
      i = 3;
    while (i--) {
      roman_num = (key[+digits.pop() + i * 10] || '') + roman_num;
    }
    return Array(+digits.join('') + 1).join('M') + roman_num;
  }

  kode_ecommerce = async () => {
    const id_user = this.props.user.auth.data.id_user;
    const code = this.state.kode_ecommerce;
    // const code = "LVG8JE"
    const id_workshop = [];

    this.state.cart.map((item) => {
      id_workshop.push(item.id_workshop);
    });

    const data = {
      id_user: id_user,
      code: code,
      id_workshop: id_workshop,
    };

    if (code) {
      const api = await kodePrakerja(data);
      if (api) {
        this.setState({
          _ecommerce: api.data,
          price_ecommerce: api.data.data[0].price,
          _voucher: '',
          _promo: '',
          _referral: '',
        });
        Alert.alert('Notifikasi', 'Success');
      } else {
        Alert.alert('Notifikasi', 'Mohon cek koneksi anda');
      }
    } else {
      Alert.alert('Notifikasi', 'Silahkan mengisi field terlebih dahulu');
    }
  };

  kode_referral = async () => {
    const id_user = this.props.user.auth.data.id_user;
    const code = this.state.kode_referral;

    const data = {
      id_user: id_user,
      kode_referral: code,
    };

    if (code) {
      const api = await kodeReferral(data);
      if (api) {
        this.setState({
          _referral: api.data,
          _voucher: '',
          _promo: '',
          _ecommerce: '',
        });
        Alert.alert('Notifikasi', 'Success');
      } else {
        Alert.alert('Notifikasi', 'Mohon cek koneksi anda');
      }
    } else {
      Alert.alert('Notifikasi', 'Silahkan isi field terlebih dahulu');
    }
  };

  kode_promo = async () => {
    const year = new Date().getFullYear();
    const month =
      (new Date().getMonth() + 1 < 10 ? '0' : '') + (new Date().getMonth() + 1);
    const day =
      (new Date().getDate() + 1 < 10 ? '0' : '') + new Date().getDate();
    const now = Number(year + month + day);

    const code_promo = this.state.kode_promo;

    const data = {
      code_promo: code_promo,
    };

    if (code_promo) {
      const api = await kodePromo(data);
      if (api) {
        const exp = api.data.data[0].expired_date.replace(/-/g, '');
        const exp_convert = parseInt(exp);

        if (exp_convert >= now) {
          console.log(api.data.data[0]);
          this.setState({
            _promo: api.data.data[0],
            _voucher: '',
            _ecommerce: '',
            _referral: '',
          });
        } else {
          Alert.alert('Notifikasi', 'Kode sudah digunakan');
        }
      } else {
        Alert.alert('Notifikasi', 'Kode sudah digunakan');
      }
    } else {
      Alert.alert('Notifikasi', 'Silahkan isi field terlebih dahulu');
    }
  };

  kode_voucher = async () => {
    const code_promo = this.state.kode_voucher;

    const year = new Date().getFullYear();
    const month =
      (new Date().getMonth() + 1 < 10 ? '0' : '') + (new Date().getMonth() + 1);
    const day =
      (new Date().getDate() + 1 < 10 ? '0' : '') + new Date().getDate();
    const now = Number(year + month + day);

    const data = {
      code_promo: code_promo,
    };

    if (code_promo >= now) {
      const api = await kodeVoucher(data);
      if (api) {
        const exp = api.data.data[0].expired_date.replace(/-/g, '');
        const exp_convert = parseInt(exp);

        if (exp_convert) {
          // console.log(api.data.data[0]);
          this.setState({
            _voucher: api.data.data[0],
            _promo: '',
            _ecommerce: '',
            _referral: '',
          });
        } else {
          this.setState({
            kode_voucher: '',
          }),
            Alert.alert('Notifikasi', 'Kode sudah digunakan');
        }
      } else {
        Alert.alert('Notifikasi', 'Kode sudah digunakan');
      }
    } else {
      Alert.alert('Notifikasi', 'Silahkan isi field terlebih dahulu');
    }
  };

  grand_total = () => {
    var result = 0;
    var total_disc = this.Total_disc();
    var total = this.totalCart();

    if (total_disc >= total) {
      return result;
    } else {
      return Number(total - total_disc);
    }
  };

  renderModal = () => {
    const {modalVisible} = this.state;

    const today = new Date();
    const qty = this.totalQty();
    const total = this.totalCart();
    const grand_total = this.grand_total();
    // const voucher_promo = parseInt(this.state._promo.potongan)

    // console.log('state', this.state._promo);
    return (
      <View style={{}}>
        <NavigationEvents
          // ComponentDidMount
          onDidFocus={() => {
            // non aktifkan tombol back pada device
            BackHandler.addEventListener(
              'hardwareBackPress',
              this.onBackButton,
            );
            // get semua diary milik user
            this.getCart();
          }}
          // ComponentWillUnmount
          onWillBlur={() => {
            BackHandler.removeEventListener(
              'hardwareBackPress',
              this.onBackButton,
            );
          }}
        />

        <Modal
          animationType=""
          transparent={false}
          visible={modalVisible}
          onRequestClose={() => {
            this.setModalVisible(!modalVisible);
          }}>
          <View style={styles.centeredView}>
            <View
              style={{
                backgroundColor: 'white',
                borderRadius: 10,
                marginHorizontal: 20,
              }}>
              <ScrollView showsVerticalScrollIndicator={false} style={{}}>
                <View style={{flexDirection: 'row', marginVertical: 20}}>
                  <Text style={{fontWeight: 'bold', flex: 1, fontSize: 20}}>
                    Result
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.setModalVisible(!modalVisible);
                    }}
                    style={{}}>
                    <Text
                      style={{
                        padding: 10,
                        fontWeight: 'bold',
                        textAlign: 'right',
                      }}>
                      X
                    </Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{justifyContent: 'space-around', marginBottom: 20}}>
                  {/* <View style={{ marginBottom: 20, alignItems: 'center', justifyContent: 'center' }}>
                              <Text style={{ marginBottom: 10, fontSize: 18 }}>No order</Text>
                              <Text style={{ fontWeight: 'bold', textAlign: 'right' }}>{no_invoice}</Text>
                           </View> */}
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={{flex: 1, fontSize: 15}}>Disc Voucher</Text>
                    <Text style={{fontSize: 15}}>
                      Rp, {Number(this.Disc_voucher()).toLocaleString('IN')}
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={{flex: 1, fontSize: 15}}>Disc Promo</Text>
                    <Text style={{fontSize: 15}}>
                      Rp, {Number(this.Disc_promo()).toLocaleString('IN')}
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={{flex: 1, fontSize: 15}}>Disc Ecommerce</Text>
                    <Text style={{fontSize: 15}}>
                      Rp,{' '}
                      {Number(this.state.price_ecommerce).toLocaleString('IN')}
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={{flex: 1, fontSize: 15}}>Kode Referral</Text>
                    {this.state._referral ? (
                      <Text style={{fontSize: 15, color: 'green'}}>
                        SUCCESS
                      </Text>
                    ) : (
                      <Text style={{fontSize: 15, color: 'red'}}>NONE</Text>
                    )}
                  </View>

                  <View style={{marginVertical: 10}}>
                    <View style={{flexDirection: 'row', marginBottom: 5}}>
                      <Text style={{flex: 1, fontSize: 15, fontWeight: 'bold'}}>
                        Total Disc
                      </Text>
                      <Text
                        style={{
                          fontSize: 15,
                          fontWeight: 'bold',
                          color: 'red',
                        }}>
                        ( - Rp, {this.Total_disc().toLocaleString('IN')})
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={{flex: 1, fontSize: 15, fontWeight: 'bold'}}>
                        Total Harga
                      </Text>
                      <Text style={{fontSize: 15, fontWeight: 'bold'}}>
                        Rp, {total.toLocaleString('IN')}
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Text style={{flex: 1, fontSize: 18, fontWeight: '300'}}>
                      Person
                    </Text>
                    <Text style={{fontSize: 18, fontWeight: '300'}}>
                      {qty} / Person
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text
                      style={{
                        flex: 1,
                        fontSize: 18,
                        fontWeight: 'bold',
                        color: '#078b6d',
                      }}>
                      Grand Total
                    </Text>
                    <Text
                      style={{
                        fontSize: 18,
                        fontWeight: 'bold',
                        color: '#078b6d',
                      }}>
                      Rp, {grand_total.toLocaleString('IN')}
                    </Text>
                  </View>
                </View>
                <View>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <TextInput
                      style={{
                        padding: 15,
                        marginVertical: 5,
                        borderRadius: 10,
                        borderWidth: 1,
                        flex: 1,
                      }}
                      value={this.state.kode_voucher}
                      placeholder="Kode Voucher"
                      onChangeText={(text) =>
                        this.setState({kode_voucher: text})
                      }
                    />
                    <TouchableOpacity
                      onPress={() => this.kode_voucher()}
                      style={{
                        padding: 15,
                        marginVertical: 5,
                        borderRadius: 10,
                        borderWidth: 1,
                        backgroundColor: '#078b6d',
                      }}>
                      <Text style={{color: 'white', fontWeight: 'bold'}}>
                        Apply
                      </Text>
                    </TouchableOpacity>
                  </View>

                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <TextInput
                      style={{
                        padding: 15,
                        marginVertical: 5,
                        borderRadius: 10,
                        borderWidth: 1,
                        flex: 1,
                      }}
                      value={this.state.kode_promo}
                      placeholder="Kode Promo"
                      onChangeText={(text) => this.setState({kode_promo: text})}
                    />
                    <TouchableOpacity
                      onPress={() => this.kode_promo()}
                      style={{
                        padding: 15,
                        marginVertical: 5,
                        borderRadius: 10,
                        borderWidth: 1,
                        backgroundColor: '#078b6d',
                      }}>
                      <Text style={{color: 'white', fontWeight: 'bold'}}>
                        Apply
                      </Text>
                    </TouchableOpacity>
                  </View>

                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <TextInput
                      style={{
                        padding: 15,
                        marginVertical: 5,
                        borderRadius: 10,
                        borderWidth: 1,
                        flex: 1,
                      }}
                      value={this.state.kode_ecommerce}
                      placeholder="Kode ecommerce"
                      onChangeText={(text) =>
                        this.setState({kode_ecommerce: text})
                      }
                    />
                    <TouchableOpacity
                      onPress={() => this.kode_ecommerce()}
                      style={{
                        padding: 15,
                        marginVertical: 5,
                        borderRadius: 10,
                        borderWidth: 1,
                        backgroundColor: '#078b6d',
                      }}>
                      <Text style={{color: 'white', fontWeight: 'bold'}}>
                        Apply
                      </Text>
                    </TouchableOpacity>
                  </View>

                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <TextInput
                      style={{
                        padding: 15,
                        marginVertical: 5,
                        borderRadius: 10,
                        borderWidth: 1,
                        flex: 1,
                      }}
                      value={this.state.kode_referral}
                      placeholder="Kode Referral"
                      onChangeText={(text) =>
                        this.setState({kode_referral: text})
                      }
                    />
                    <TouchableOpacity
                      onPress={() => this.kode_referral()}
                      style={{
                        padding: 15,
                        marginVertical: 5,
                        borderRadius: 10,
                        borderWidth: 1,
                        backgroundColor: '#078b6d',
                      }}>
                      <Text style={{color: 'white', fontWeight: 'bold'}}>
                        Apply
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <TouchableOpacity
                  onPress={() => this.checkout()}
                  style={{
                    flex: 1,
                    backgroundColor: '#078b6d',
                    minHeight: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 10,
                    marginBottom: 20,
                  }}>
                  <Text style={{color: 'white', fontSize: 15}}>
                    Submit to Checkout
                  </Text>
                </TouchableOpacity>
              </ScrollView>
            </View>
          </View>
        </Modal>
      </View>
    );
  };

  checkout = async () => {
    const id_user = this.props.user.auth.data.id_user;
    const today = new Date();
    const date =
      today.getFullYear().toString() +
      (today.getMonth() + 1).toString() +
      today.getDate().toString();
    const rom_year = this.integer_to_roman(
      parseInt(today.getFullYear().toString().substring(2, 4)),
    );
    const rom_month = this.integer_to_roman(
      parseInt(today.getMonth().toString()),
    );

    const total = this.grand_total();
    const nomer = today.getTime().toString();
    const no_invoice =
      'SKILLS/' +
      date +
      '/' +
      rom_year +
      '/' +
      rom_month +
      '/' +
      id_user +
      '-' +
      nomer;
    const id_workshop = [];
    const person = [];
    const tanggal = [];

    this.state.cart.map((item) => {
      id_workshop.push(item.id_workshop);
      person.push(item.quant);
      tanggal.push(item.multi_tanggal);
    });

    this.setState({
      modalVisible: false,
    });

    // console.log('api', api.data);

    this.props.navigation.navigate('Pay');
  };

  renderEmpty = () => {
    return (
      <SafeAreaView
        style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh.bind(this)}
            />
          }>
          <Text>Cart Kosong</Text>
        </ScrollView>
      </SafeAreaView>
    );
  };

  render() {
    if (this.props.user.auth.data.id_user) {
      // console.log('render', total);
      if (this.state.cart !== '') {
        const qty = this.totalQty();
        const total = this.totalCart();
        return (
          <SafeAreaView style={{flex: 1}}>
            <NavigationEvents
              // ComponentDidMount
              onDidFocus={() => {
                // non aktifkan tombol back pada device
                BackHandler.addEventListener(
                  'hardwareBackPress',
                  this.onBackButton,
                );
                // get semua diary milik user
                this.getCart();
              }}
              // ComponentWillUnmount
              onWillBlur={() => {
                BackHandler.removeEventListener(
                  'hardwareBackPress',
                  this.onBackButton,
                );
              }}
            />
            <ScrollView style={{flex: 1}}>
              <Text
                style={{
                  padding: 15,
                  fontSize: 20,
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}>
                Cart
              </Text>
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh.bind(this)}
              />
              {this.state.cart ? this.renderCard() : this.renderEmpty()}
              {this.state.cart ? this.renderModal() : null}
            </ScrollView>
            <View style={{}}>
              <ImageBackground
                imageStyle={{}}
                style={{flexDirection: 'row', padding: 10}}
                source={{
                  uri:
                    'https://img.freepik.com/free-vector/abstract-galaxy-background_1199-247.jpg?size=626&ext=jpg',
                }}>
                <View style={{flex: 3}}>
                  <Text style={styles.text}>Qty : {qty} pcs</Text>
                  <Text style={styles.text}>Berat : ~ Kg</Text>
                  <Text style={styles.text}>
                    Grand Total : Rp {total.toLocaleString('IN')}
                  </Text>
                </View>
                <View style={{flex: 1, justifyContent: 'space-around'}}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setModalVisible(true);
                    }}
                    style={{
                      justifyContent: 'center',
                      height: 50,
                      alignItems: 'center',
                      backgroundColor: '#078b6d',
                      borderRadius: 5,
                    }}>
                    <Text style={styles.text}>Checkout</Text>
                  </TouchableOpacity>
                </View>
              </ImageBackground>
            </View>
          </SafeAreaView>
        );
      } else {
        // console.log('render else')
        return this.renderEmpty();
      }
    } else {
      return (
        <LoginPage {...this.props} />
        // <SafeAreaView>
        //    <View style={{ alignItems: 'center', justifyContent: 'center', height: '100%' }}
        //       refreshControl={
        //          <RefreshControl
        //             refreshing={this.state.refreshing}
        //             onRefresh={this._onRefresh.bind(this)}
        //          />
        //       }
        //    >
        //       <Text>Silahkan Login terlebih dahulu</Text>
        //       <TouchableOpacity onPress={() => { this.props.navigation.navigate('Login') }} style={{ backgroundColor: 'red', padding: 10, margin: 20, paddingHorizontal: 20, borderRadius: 50 }}>
        //          <Text style={{ color: 'white' }}>Login</Text>
        //       </TouchableOpacity>
        //       <Text>OR</Text>
        //       <TouchableOpacity onPress={this.refreshScreen} style={{ backgroundColor: 'red', padding: 10, margin: 20, paddingHorizontal: 20, borderRadius: 50 }}>
        //          <Text style={{ color: 'white' }}>Refresh Page</Text>
        //       </TouchableOpacity>
        //    </View>
        // </SafeAreaView>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.auth,
  };
};

const styles = StyleSheet.create({
  centeredView: {
    // flex: 1,
    justifyContent: 'center',
  },
  text: {
    color: 'white',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 16,
  },
});

export default connect(mapStateToProps)(Cart);
