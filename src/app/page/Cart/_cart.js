import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, Image, ImageBackground, Alert, SafeAreaView, RefreshControl, Modal } from 'react-native'
import { CheckBox, Card, Icon } from 'native-base';
import { connect } from 'react-redux'
import { getCartByIdUser } from '../../../api/cart_api';
import { ScrollView, TextInput } from 'react-native-gesture-handler';

import { updateCart, deleteCart } from '../../../api/cart_api'
import { workshopListAll } from '../../../api/home_api'



class Cart extends Component {
   constructor(props) {
      super(props);
      this.getCart()
      this.state = {
         cart: '',
         count: 1,
         check: true,
         total_qty: 0,
         total_price: 0,
         modalVisible: false,
         quant: 0,
         voucher: '',
         promo: '',
         ecommerce: '',
         referral: '',
         workshop: '',
         isLoading: false,
         total: [],
         total_rp: []
      };
   }

   componentDidMount() {
      // this.getCart()
      this.getWorkshop()
   }

   setModalVisible = (visible) => {
      this.setState({ modalVisible: visible });
   }

   alertButton(item) {
      Alert.alert(
         'Alert',
         'Delete this cart ?',
         [
            { text: 'NO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
            { text: 'YES', onPress: () => this.onButtonDelete(item) },
         ]
      );
   }

   onButtonDelete = async (item) => {
      const data = {
         id_cart: item
      }
      // console.log(item);
      const api = await deleteCart(data)

      { this.getCart() }
   }

   _onRefresh() {
      this.setState({ refreshing: true });
      this.getCart().then(() => {
         this.setState({ refreshing: false });
      });
      console.log(this.state.cart);
   }

   getWorkshop = async () => {
      const api_workshop = await workshopListAll()
      this.setState({
         workshop: api_workshop.data
      })
      // console.log('wk', api_workshop.data);
   }

   getCart = async () => {
      const data = {
         id_user: this.props.user.auth.data.id_user
      }

      const api = await getCartByIdUser(data)
      this.setState({
         cart: api.data.data,
         quant: api.data.data.quant
      })
      // console.log('cart', api.data.data);
   }

   _toCheckout = () => {
      this.props.navigation.navigate('Checkout')
   }

   _onPressAdd = async (item) => {
      const id_user = this.props.user.auth.data.id_user
      const id_cart = item.id_cart
      const quant = item.quant

      this.setState({
         isLoading: true
      })

      const data = {
         id_user: id_user,
         id_cart: id_cart,
         quant: parseInt(quant) + 1
      }
      console.log(data.quant + 1);

      const api = await updateCart(data)
      this.setState({
         cart: api.data.data,
         isLoading: false
      })
      this.getCart()
   }

   _onPressMin = async (item) => {
      const id_user = this.props.user.auth.data.id_user
      const id_cart = item.id_cart
      const quant = item.quant

      this.setState({
         isLoading: true
      })

      const data = {
         id_user: id_user,
         id_cart: id_cart,
         quant: parseInt(quant) - 1
      }
      if (data.quant < 1) {
         this.alertButton(id_cart)
      } else {
         const api = await updateCart(data)
         this.setState({
            cart: api.data.data,
            isLoading: false
         })
      }
      { this.getCart() }
   }

   totalQty = () => {
      var total = 0

      for (let i = 0; i < this.state.cart.length; i++) {
         const jumlah = this.state.cart[i].quant
         total = total + Number(jumlah)
      }
      return (
         total
      )
   }

   totalCart = () => {
      var id_user = this.props.user.auth.data.id_user,
      var total = 0
      var cart = this.state.cart

      console.log(cart);

      // var id_user = this.props.user.auth.data.id_user
      // var total = 0

      // for (let i = 0; i < this.state.cart.length; i++) {
      //    if (this.state.cart[i].id_user === id_user) {
      //       for (let j = 0; j < this.state.workshop.length; j++) {
      //          if (this.state.cart[i].id_workshop === this.state.workshop[j].id) {
      //             const jumlah = this.state.cart[i].quant * this.state.workshop[j].price
      //             total = total + jumlah
      //          }
      //       }
      //    }
      // }
      // return (
      //    total
      // )
   }

   renderTanggal = (item) => {
      const multi = item
      const ada = this.state.cart.multi_tanggal

      // console.log('tanggal', multi);
      if (ada == undefined) {
         return (
            multi.map((item) => {
               return (
                  <TouchableOpacity
                     style={{ backgroundColor: "#078b6d", padding: 5, borderRadius: 25, paddingHorizontal: 15, marginBottom: 5, marginHorizontal: 5, paddingTop: 7 }}
                  // onPress={() => this.pushTanggal(item)}
                  >
                     <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={{ color: 'white' }}>{item}</Text>
                        <CheckBox checked={true} />
                     </View>
                  </TouchableOpacity>
               )
            })
         )
      }
   }

   total = (val) => {
      console.log(val);
      // this.state.total.push(val)
      // console.log(Number(this.state.total_rp[this.state.total_rp.length - 1]) / 2);
   }

   renderList = () => {
      const source = 'https://skills.id/admin/source/images/thumbnail/'
      return (
         this.state.cart !== '' ?
            this.state.cart.map((item) => {
               // const multi = item.multi_tanggal.split(",")
               // console.log(multi.length);
               return (
                  <View style={{ marginHorizontal: 10, alignSelf: 'center', flexDirection: 'row' }}>
                     <TouchableOpacity
                        style={{ borderRadius: 10, flex: 1 }}>
                        <Card rounded style={{ borderRadius: 10, flex: 1, flexDirection: 'row' }}>
                           <View style={{ flex: 2.5 }}>
                              <Image
                                 style={{ flex: 1, borderTopLeftRadius: 10, borderBottomLeftRadius: 10 }}
                                 source={{ uri: source + item.image }} />
                           </View>
                           <View style={{ flex: 5, margin: 10, justifyContent: 'space-between' }}>
                              <View>
                                 <Text numberOfLines={2} style={{ fontSize: 15, textAlign: 'left', fontWeight: 'bold' }}> {item.title} </Text>
                                 <View>
                                    <Text numberOfLines={1} style={{ fontSize: 12, color: '#078b6d', fontStyle: 'italic', marginBottom: 5 }}>Author : {item.name} </Text>
                                 </View>
                                 {/* <Text style={{ textDecorationLine: 'line-through', fontSize: 12 }}> Rp. {item.Disc} </Text> */}
                                 {/* <Text style={{ fontSize: 12 }}> {item.multi_tanggal.split(",")} </Text> */}
                                 {
                                    item.multi_tanggal !== undefined ?
                                       this.renderTanggal(item.multi_tanggal.split(",")) : null
                                 }
                                 <Text style={{ fontSize: 12 }}> Rp. {parseInt(item.price).toLocaleString("IN")} / Person </Text>
                                 <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 15, flex: 1 }}>Total</Text>
                                    {
                                       item.multi_tanggal !== undefined ?
                                          <Text style={{ fontSize: 15 }}>Rp {Number(item.price * item.multi_tanggal.split(",").length * item.quant).toLocaleString('IN')} </Text>
                                          : null
                                    }
                                    {
                                       item.multi_tanggal !== undefined ?
                                          this.total(Number(item.price * item.multi_tanggal.split(",").length * item.quant))
                                          : null
                                    }
                                 </View>
                              </View>

                           </View>
                           <View style={{ margin: 5, justifyContent: 'space-around', padding: 5, borderRadius: 5, alignItems: 'center', borderStyle: 'solid', borderWidth: 1, borderColor: 'lightgrey', marginRight: 5, width: '10%' }}>
                              <TouchableOpacity onPress={() => this._onPressAdd(item)} style={{ width: '100%' }}>
                                 <Icon style={{ fontSize: 20, textAlign: 'center' }} name="add" />
                              </TouchableOpacity>
                              <Text>{item.quant}</Text>
                              <TouchableOpacity onPress={() => this._onPressMin(item)} style={{ width: '100%' }}>
                                 <Icon style={{ fontSize: 20, textAlign: 'center' }} name="ios-remove" />
                              </TouchableOpacity>
                           </View>
                        </Card>
                     </TouchableOpacity>
                  </View>
               )
            }) : null
      )
   }

   cartEducator = () => {
      return (
         <SafeAreaView style={{ alignItems: 'center', justifyContent: 'center', flex: 1, height: '100%' }}>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}><Text>cart kosong</Text></View>
         </SafeAreaView>
      )
   }

   integer_to_roman(num) {
      if (typeof num !== 'number')
         return false;

      var digits = String(+num).split(""),
         key = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM",
            "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC",
            "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"],
         roman_num = "",
         i = 3;
      while (i--)
         roman_num = (key[+digits.pop() + (i * 10)] || "") + roman_num;
      return Array(+digits.join("") + 1).join("M") + roman_num;
   }

   renderModal = () => {
      const { modalVisible } = this.state;

      const id_user = this.props.user.auth.data.id_user
      const today = new Date();
      const date = today.getFullYear().toString() + (today.getMonth() + 1).toString() + today.getDate().toString();
      const rom_year = this.integer_to_roman(parseInt(today.getFullYear().toString().substring(2, 4)))
      const rom_month = this.integer_to_roman(parseInt(today.getMonth().toString()))
      const qty = this.totalQty()
      const total = this.totalCart()
      const nomer = today.getTime().toString();
      const no_invoice = "SKILLS/" + date + "/" + rom_year + "/" + rom_month + "/" + id_user + "-" + nomer

      // console.log(no_invoice);
      return (
         <SafeAreaView>
            <Modal
               animationType="slide"
               transparent={true}
               visible={modalVisible}
               onRequestClose={() => {
                  Alert.alert("Modal has been closed.");
               }}
            >
               <View style={styles.centeredView}>
                  <View style={{ backgroundColor: 'white', minHeight: '50%', borderTopLeftRadius: 25, borderTopRightRadius: 25, marginBottom: 80, margin: 5 }}>
                     <ScrollView style={{ margin: 20 }}>
                        <View style={{ flexDirection: 'row' }}>
                           <Text style={{ padding: 10, fontWeight: 'bold', flex: 1, fontSize: 20 }}>Result</Text>
                           <TouchableOpacity
                              onPress={() => {
                                 this.setModalVisible(!modalVisible);
                              }}
                              style={{}}>
                              <Text style={{ padding: 10, fontWeight: 'bold', textAlign: 'right' }}>X</Text>
                           </TouchableOpacity>
                        </View>
                        <View style={{ justifyContent: 'space-around', height: 70, marginBottom: 15 }}>
                           <View style={{ marginBottom: 20, alignItems: 'center', justifyContent: 'center' }}>
                              <Text style={{ marginBottom: 10 }}>No order</Text>
                              <Text style={{ fontWeight: 'bold', textAlign: 'right' }}>{no_invoice}</Text>
                           </View>
                           <View style={{ flexDirection: 'row' }}>
                              <Text style={{ flex: 1 }}>Total Harga</Text>
                              <Text>Rp, {total.toLocaleString()}</Text>
                           </View>

                        </View>
                        <View>
                           <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                              <TextInput
                                 style={{ padding: 15, marginVertical: 5, borderRadius: 10, borderWidth: 1, flex: 1 }}
                                 value={this.state.voucher}
                                 placeholder="Kode Voucher"
                                 onChangeText={(text) => this.setState({ voucher: text })}
                              />
                              <TouchableOpacity style={{ padding: 15, marginVertical: 5, borderRadius: 10, borderWidth: 1, backgroundColor: '#078b6d' }}>
                                 <Text style={{ color: 'white', fontWeight: 'bold' }}>Apply</Text>
                              </TouchableOpacity>
                           </View>

                           <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                              <TextInput
                                 style={{ padding: 15, marginVertical: 5, borderRadius: 10, borderWidth: 1, flex: 1 }}
                                 value={this.state.voucher}
                                 placeholder="Kode Promo"
                                 onChangeText={(text) => this.setState({ promo: text })}
                              />
                              <TouchableOpacity style={{ padding: 15, marginVertical: 5, borderRadius: 10, borderWidth: 1, backgroundColor: '#078b6d' }}>
                                 <Text style={{ color: 'white', fontWeight: 'bold' }}>Apply</Text>
                              </TouchableOpacity>
                           </View>

                           <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                              <TextInput
                                 style={{ padding: 15, marginVertical: 5, borderRadius: 10, borderWidth: 1, flex: 1 }}
                                 value={this.state.voucher}
                                 placeholder="Kode prakerja"
                                 onChangeText={(text) => this.setState({ ecommerce: text })}
                              />
                              <TouchableOpacity style={{ padding: 15, marginVertical: 5, borderRadius: 10, borderWidth: 1, backgroundColor: '#078b6d' }}>
                                 <Text style={{ color: 'white', fontWeight: 'bold' }}>Apply</Text>
                              </TouchableOpacity>
                           </View>

                           <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                              <TextInput
                                 style={{ padding: 15, marginVertical: 5, borderRadius: 10, borderWidth: 1, flex: 1 }}
                                 value={this.state.voucher}
                                 placeholder="Kode Referral"
                                 onChangeText={(text) => this.setState({ referral: text })}
                              />
                              <TouchableOpacity style={{ padding: 15, marginVertical: 5, borderRadius: 10, borderWidth: 1, backgroundColor: '#078b6d' }}>
                                 <Text style={{ color: 'white', fontWeight: 'bold' }}>Apply</Text>
                              </TouchableOpacity>
                           </View>
                        </View>
                     </ScrollView>
                  </View>
               </View>
            </Modal>
         </SafeAreaView>
      )
   }

   render() {
      const { name } = this.props.user.auth.data
      const { modalVisible } = this.state;
      const qty = this.totalQty()
      const total = this.totalCart()

      // console.log('render', this.state.quant);

      if (this.state.isLoading !== true) {
         return (
            <SafeAreaView style={{ flex: 1 }}>
               <View style={{ marginHorizontal: 15, justifyContent: 'center' }}>
                  <Text style={{ fontSize: 30, fontWeight: 'bold', color: 'black', marginVertical: 10 }}>Cart</Text>
               </View>
               <ScrollView style={{ height: '100%' }}
                  refreshControl={
                     <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh.bind(this)}
                     />
                  }
               >
                  <View style={{ marginRight: 10 }}>
                     {
                        this.state.cart ?
                           this.renderList() :
                           this.cartEducator()
                     }

                  </View>
                  {this.renderModal()}
               </ScrollView>

               <View style={{ padding: 5 }}>
                  <ImageBackground
                     imageStyle={{ borderTopLeftRadius: 10, borderTopRightRadius: 10 }}
                     style={{ flexDirection: 'row', padding: 10 }}
                     source={{ uri: 'https://img.freepik.com/free-vector/abstract-galaxy-background_1199-247.jpg?size=626&ext=jpg' }}
                  >
                     <View style={{ flex: 3 }}>
                        <Text style={styles.text}>Qty : {qty} pcs</Text>
                        <Text style={styles.text}>Weight : - Kg</Text>
                        <Text style={styles.text}>Grand Total : Rp {total.toLocaleString()}</Text>
                     </View>
                     <View style={{ flex: 1, justifyContent: 'space-around' }}>
                        <TouchableOpacity
                           onPress={() => {
                              this.setModalVisible(true);
                           }}
                           style={{ justifyContent: 'center', height: 50, alignItems: "center", backgroundColor: '#078b6d', borderRadius: 10 }}>
                           <Text style={styles.text}>Checkout</Text>
                        </TouchableOpacity>
                     </View>
                  </ImageBackground>
               </View>
            </SafeAreaView>
         )
      } else {
         return (
            <SafeAreaView style={{ alignItems: 'center', justifyContent: 'center', height: '100%' }}>
               <ScrollView style={{ flex: 1 }}>
                  <RefreshControl
                     refreshing={this.state.refreshing}
                     onRefresh={this._onRefresh.bind(this)}
                  />
                  <Text>Silahkan Login terlebih dahulu</Text>
                  <TouchableOpacity onPress={() => { this.props.navigation.navigate('Login') }} style={{ backgroundColor: 'red', padding: 10, margin: 20, paddingHorizontal: 20, borderRadius: 50 }}>
                     <Text style={{ color: 'white' }}>Login</Text>
                  </TouchableOpacity>
               </ScrollView>
            </SafeAreaView>
         )
      }
   }
}

const mapStateToProps = state => {
   return {
      user: state.auth
   }
}

export default connect(mapStateToProps)(Cart)

const styles = StyleSheet.create({
   text: {
      color: 'white',
      fontStyle: 'normal'
   },
   centeredView: {
      flex: 1,
      justifyContent: 'flex-end'
   },
   modalView: {
      minHeight: '50%',
      margin: 5,
      marginBottom: 90,
      backgroundColor: "white",
      borderRadius: 20,
      padding: 20,
      shadowColor: "#000",
      shadowOffset: {
         width: 0,
         height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5
   },
   openButton: {
      backgroundColor: "#F194FF",
      borderRadius: 20,
      padding: 10,
      elevation: 2
   },
   textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
   },
   modalText: {
      marginBottom: 15,
      textAlign: "center"
   }
})