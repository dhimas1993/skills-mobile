import React, { Component } from 'react'
import { Text, StyleSheet, SafeAreaView, View, TouchableOpacity, PermissionsAndroid } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'

import Carousel from '../../components/Carousel'
import Hightlight from '../../components/Highlight'
import Product from '../../components/Product'
import Educator from '../../components/Educator'
import Workshop from '../../components/Prakerja'
import Hotworkshop from '../../components/Hot_workshop'
import Upcomming from '../../components/UpComming'
import Article from '../../components/Article'
import Category from '../../components/Category'

export default class Home extends Component {
   constructor(props) {
      super(props);
      this.state = {
      };
   }

   componentDidMount() {
      this.requestCameraPermission()
   }

   requestCameraPermission = async () => {
      try {
         const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
               title: "Cool Photo App Camera Permission",
               message:
                  "Cool Photo App needs access to your camera " +
                  "so you can take awesome pictures.",
               buttonNeutral: "Ask Me Later",
               buttonNegative: "Cancel",
               buttonPositive: "OK"
            }
         );
         if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log("You can use the camera");
         } else {
            console.log("Camera permission denied");
         }
      } catch (err) {
         console.warn(err);
      }
   };



   render() {
      return (
         <SafeAreaView>
            <ScrollView style={{ backgroundColor: 'white' }} showsVerticalScrollIndicator={false}>
               <Carousel {...this.props} />
               {/* <Hightlight {...this.props} /> */}
               {/* <Product {...this.props} /> */}
               <Category {...this.props} />
               {/* <Educator {...this.props} /> */}
               <Workshop {...this.props} />
               <Upcomming {...this.props} />
               <Hotworkshop {...this.props} />
               <Article {...this.props} />
            </ScrollView>
         </SafeAreaView>
      )
   }
}

const styles = StyleSheet.create({
   container: {
      height: 50,
      padding: 10,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'pink'
   }
})
