import React, { Component } from 'react';
import { Image, View, Text, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { DeckSwiper, Card, Icon } from 'native-base'

const asu = 'https://m.ayosemarang.com/images-semarang/post/articles/2020/05/01/56273/deddy-corbuzier.jpg';
const content1 = 'https://images.unsplash.com/photo-1587994122928-eb0b136b4d3b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=3302&q=80'
const content2 = 'https://images.unsplash.com/photo-1587614295506-f03c0e6f5b44?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60'
const content3 = 'https://images.unsplash.com/photo-1588981686673-5120ed7a819a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60'

const cards = [
   {
      text: 'Deddy Corbuzier',
      name: 'Profesional Magician',
      image: 'https://m.ayosemarang.com/images-semarang/post/articles/2020/05/01/56273/deddy-corbuzier.jpg',
   },
   {
      text: 'Renata Moeloek',
      name: 'Profesional Chef',
      image: 'https://akcdn.detik.net.id/community/media/visual/2020/01/21/a6963226-4a53-4d18-ad76-ccd50e6e991b.png?w=900&q=70',
   },
   {
      text: 'Capt Vincent',
      name: 'Pilot',
      image: 'https://blue.kumparan.com/image/upload/fl_progressive,fl_lossy,c_fill,q_auto:best,w_640/v1559032458/zwdwnjgvqo8oowd2auxj.jpg',
   },
];


export default class DeckSwiperAdvancedExample extends Component {
   render() {
      return (
         <View>
            <View style={{ padding: 10 }}>
               <ScrollView showsVerticalScrollIndicator={false}>
                  {
                     cards.map((item) => {
                        return (
                           <Card style={styles.card}>
                              <View style={styles.cardContainer1}>
                                 <View style={styles.cardContainerTop}>
                                    <View style={styles.containerProfile1}>
                                       <Image style={styles.avatar} source={{ uri: item.image }} />
                                       <View style={{ flex: 1 }}>
                                          <Text style={styles.facilitatorName}>{item.text}</Text>
                                          <Text style={styles.position}>{item.name}</Text>
                                       </View>
                                    </View>
                                    <View style={{ flex: 1 }}>
                                       <Text style={styles.titleVideo1}>Learn The Basic of Video Editing With Adobe Premiere</Text>
                                    </View>
                                 </View>
                                 <View style={styles.cardContainer2}>
                                    <View style={styles.cardBottom}>
                                       <View><Image style={styles.gambar1} source={{ uri: content1 }} /></View>
                                    </View>
                                    <View style={styles.cardBottomRight}>

                                       <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                          <View style={{ flex: 1, height: "60%", alignItems: 'center' }}>
                                             <Image style={styles.gambar2} source={{ uri: content2 }} />
                                             {/* <Text style={{ fontSize: 8,flexWrap: "wrap",padding: 3 }}>Learn The Basic of Video Editing With Adobe Premiere</Text> */}
                                          </View>
                                          <View style={{ flex: 1, height: "60%", alignItems: 'center' }}>
                                             <Image style={[styles.gambar2]} source={{ uri: content3 }} />
                                             {/* <Text style={{ fontSize: 8,flexWrap: "wrap",padding: 3 }}>Learn The Basic of Video Editing With Adobe Premiere</Text> */}
                                          </View>
                                       </View>
                                       <Text style={{ alignSelf: 'center', padding: 5, fontSize: 12 }}> Bundling 6 part video</Text>
                                       <View>
                                          <TouchableOpacity
                                             style={{
                                                backgroundColor: "red", borderRadius: 50, justifyContent: 'center', alignItems: 'center', height: 25
                                             }}>
                                             <Text style={{ color: 'white', justifyContent: 'center', alignItems: 'center', fontSize: 11 }}>Register Now!!!</Text>
                                          </TouchableOpacity>
                                       </View>
                                    </View>
                                 </View>
                              </View>
                           </Card>
                        )
                     })
                  }
               </ScrollView>
            </View>
         </View>
      );
   }
}


const styles = StyleSheet.create({
   container: {
      padding: 10
   },
   title: {
      marginBottom: 5, fontSize: 18
   },
   card: {
      borderRadius: 7, padding: 5
   },
   cardContainer1: {
      width: '100%', padding: 5
   },
   cardContainer2: {
      flex: 1, flexDirection: 'row'
   },
   cardContainerTop: {
      flexDirection: 'row', height: 55, justifyContent: "center"
   },
   containerProfile1: {
      flex: 1, flexDirection: 'row'
   },
   avatar: {
      height: 50, width: 50, borderRadius: 50
   },
   facilitatorName: {
      paddingLeft: 10, paddingTop: 2, fontSize: 13
   },
   position: {
      paddingLeft: 10, paddingTop: 2, fontSize: 10, flexWrap: "wrap"
   },
   titleVideo1: {
      paddingLeft: 10, paddingTop: 2, fontSize: 12
   },
   cardBottom: {
      width: '50%'
   },
   cardBottomRight: {
      flex: 1, justifyContent: 'space-between'
   },
   gambar1: {
      height: '100%',
      width: '95%',
      borderRadius: 6,
   },
   gambar2: {
      height: 55,
      width: '95%',
      borderRadius: 5
   },
   dot: {
      width: 10,
      height: 10,
      backgroundColor: 'grey',
      borderRadius: 100,
      marginRight: 10,
      marginTop: 10
   }
})