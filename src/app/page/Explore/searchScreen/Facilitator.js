import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, TouchableOpacity } from 'react-native'
import { Card, Icon } from 'native-base';

const asu = 'https://images.unsplash.com/photo-1503428593586-e225b39bddfe?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60';

export default class Facilitator extends Component {
   constructor(props) {
      super(props);
      this.state = {
         searchKey: '',
      }
   }

   _toFacilitatorPage = (param) => {
      this.props.navigation.navigate('FacilitatorPage', { detail_edu: param })
   }

   render() {
      const { email, id, id_user, id_spekaer, name, photo, sum_wk } = this.props.search

      return (
         <View>
            <Card style={{ borderRadius: 10, padding: 5, elevation: 5 }}>
               <View style={{ width: '100%', padding: 5 }}>
                  <View style={{ flexDirection: 'row', justifyContent: "center", flex: 1 }}>
                     <View style={{ flex: 3, flexDirection: 'row' }}>
                        <Image style={{ width: 70, borderRadius: 5 }} source={{ uri: 'https://skills.id/admin/source/images/speaker/' + photo }} />
                        <View style={{ flex: 1 }}>
                           <Text style={{ paddingLeft: 10, fontSize: 12, fontWeight: 'bold' }} numberOfLines={2}>{name}</Text>
                           <Text style={{ paddingLeft: 10, fontSize: 12, flexWrap: "wrap", fontStyle: 'italic' }}>{email}</Text>
                           <View style={{ flexDirection: "row", paddingLeft: 10, marginVertical: 5 }}>
                              <Icon name="star" style={{ color: 'orange', fontSize: 15, fontWeight: '500' }} />
                              <Icon name="star" style={{ color: 'orange', fontSize: 15, fontWeight: '500' }} />
                              <Icon name="star" style={{ color: 'orange', fontSize: 15, fontWeight: '500' }} />
                              <Icon name="star" style={{ color: 'orange', fontSize: 15, fontWeight: '500' }} />
                              <Icon name="star" style={{ color: 'orange', fontSize: 15, fontWeight: '500' }} />
                           </View>
                        </View>
                     </View>
                     <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', width: '100%' }}>
                        <Text style={{ fontSize: 12, fontWeight: 'bold' }}>{sum_wk} Class</Text>
                        <Text style={{ fontSize: 12, fontWeight: 'bold' }}>1 Video</Text>
                        <TouchableOpacity
                           onPress={() => this._toFacilitatorPage(this.props.search)}
                        >
                           <View style={{ backgroundColor: 'red', padding: 10, margin: 10, borderRadius: 5 }}>
                              <Text style={{ color: 'white', fontWeight: 'bold' }}>Detail</Text>
                           </View>
                        </TouchableOpacity>
                     </View>
                  </View>
               </View>
            </Card>
         </View>
      )
   }
}

const styles = StyleSheet.create({
   carousel: {
      backgroundColor: 'pink',
      height: 160,
   },
   navigasi: {
      height: 60,
      flexDirection: 'row',
      alignItems: 'center',
   },
   gambar1: {
      height: 130,
      width: 183,
      borderRadius: 6,
   },
   gambar2: {
      height: 55,
      width: 80,
      borderRadius: 6,
      justifyContent: "center",
      alignItems: 'center'
   },
   gambar1: {
      height: '100%',
      width: '95%',
      borderRadius: 6,
   },
   gambar2: {
      height: 55,
      width: '95%',
      borderRadius: 6
   }
})
