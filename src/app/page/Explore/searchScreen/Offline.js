import React, { Component } from 'react';
import { Image, View, Text, StyleSheet, TouchableOpacity, ScrollView, Modal } from 'react-native';
import { DeckSwiper, Card, Icon, List, CheckBox, ListItem } from 'native-base'

import Icons from 'react-native-vector-icons/FontAwesome5';

export default class DeckSwiperAdvancedExample extends Component {
   constructor(props) {
      console.log('object', props.search)
      super(props);
      this.state = {
      }
   }

   _toWorkshopPage = (param) => {
      console.log(param);
      this.props.navigation.navigate('WorkshopPage', param)
   }

   renderTanggal = (item) => {
      const start1 = 1
      const multi = item.split(", ")

      const year = new Date().getFullYear()
      const month = (new Date().getMonth() + 1 < 10 ? '0' : '') + (new Date().getMonth() + 1)
      const day = (new Date().getDate() + 1 < 10 ? '0' : '') + (new Date().getDate())
      const now = Number(year + month + day)

      // console.log(now)
      if (multi) {
         return (
            multi.length
         )
      } else {
         return (
            start1
         )
      }
   }

   render() {
      const { id, name, start_date, multi_tanggal, email, image, kota, level, title, photo } = this.props.search
      const pecah = multi_tanggal.split(", ");
      // console.log('liat lengt', pecah.length);
      console.log(name);

      return (
         // <View>
         //    <Text>mantap</Text>
         // </View>
         <Card style={styles.card}>
            <View style={styles.cardContainer1}>
               <View style={styles.cardContainerTop}>
                  <View style={styles.containerProfile1}>
                     <Image style={styles.avatar} source={{ uri: 'https://skills.id/admin/source/images/speaker/' + photo }} />
                     <View style={{ flex: 1 }}>
                        <Text style={styles.facilitatorName} numberOfLines={1}>{name}</Text>
                        <Text style={styles.position}>{level}</Text>
                     </View>
                  </View>
                  <View style={{ flex: 1 }}>
                     <Text style={styles.titleVideo1} numberOfLines={2}>{title}</Text>
                  </View>
               </View>
               <View style={styles.cardContainer2}>
                  <View style={styles.cardBottom}>
                     <View><Image style={styles.gambar1} source={{ uri: 'https://skills.id/admin/source/images/thumbnail/' + image }} /></View>
                  </View>
                  <View style={styles.cardBottomRight}>
                     <View style={{ flex: 1, marginBottom: 10 }}>
                        <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 10, paddingBottom: 10 }}>
                           <Icons name="map-marker-alt" style={{ fontSize: 15, marginRight: 10 }} />
                           <Text>{kota}</Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 10 }}>
                           <Icons name="calendar-alt" style={{ fontSize: 15, marginRight: 8 }} />
                           <Text>{this.renderTanggal(multi_tanggal)} Schedule
                                       </Text>
                        </View>
                     </View>
                     <View>
                        <TouchableOpacity
                           onPress={() => this._toWorkshopPage(this.props.search)}
                           style={{ backgroundColor: "red", borderRadius: 50, justifyContent: 'center', alignItems: 'center' }}>
                           <Text style={{ color: 'white', justifyContent: 'center', alignItems: 'center', fontSize: 13, paddingVertical: 5, fontWeight: '500' }}>Detail</Text>
                        </TouchableOpacity>
                     </View>
                  </View>
               </View>
            </View>
         </Card>
      )
   }
}

const styles = StyleSheet.create({
   container: {
      padding: 10
   },
   title: {
      marginBottom: 5, fontSize: 18
   },
   card: {
      borderRadius: 7, padding: 5
   },
   cardContainer1: {
      width: '100%', padding: 5
   },
   cardContainer2: {
      flex: 1, flexDirection: 'row'
   },
   cardContainerTop: {
      flexDirection: 'row', height: 55, justifyContent: "center"
   },
   containerProfile1: {
      flex: 1, flexDirection: 'row'
   },
   avatar: {
      height: 50, width: 50, borderRadius: 50
   },
   facilitatorName: {
      paddingLeft: 10, paddingTop: 2, fontSize: 13, fontWeight: 'bold'
   },
   position: {
      paddingLeft: 10, paddingTop: 2, fontSize: 13, flexWrap: "wrap"
   },
   titleVideo1: {
      paddingLeft: 10, paddingTop: 2, fontSize: 13, fontWeight: 'bold'
   },
   cardBottom: {
      width: '50%'
   },
   cardBottomRight: {
      flex: 1, justifyContent: 'space-between'
   },
   gambar1: {
      height: '100%',
      width: '95%',
      borderRadius: 6,
   },
   gambar2: {
      height: 55,
      width: '95%',
      borderRadius: 5
   },
   dot: {
      width: 10,
      height: 10,
      backgroundColor: 'grey',
      borderRadius: 100,
      marginRight: 10,
      marginTop: 10
   },
   centeredView: {
      flex: 1,
      justifyContent: 'flex-end',
      backgroundColor: 'black',
      opacity: 0.9
   },
   modalView: {
      margin: 20,
      backgroundColor: "white",
      borderRadius: 20,
      padding: 10,
      alignItems: "center",
      shadowColor: "black",
      shadowOffset: {
         width: 0,
         height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5
   },
   openButton: {
      backgroundColor: "#F194FF",
      borderRadius: 20,
      padding: 10,
      elevation: 2,
      marginBottom: 20

   },
   textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
   },
   modalText: {
      marginBottom: 15,
      textAlign: "center"
   }
})