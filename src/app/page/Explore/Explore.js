import React, { Component } from 'react';
import { Container, Content, Tab, Tabs, TabHeading, Card, Icon } from 'native-base';
import { StyleSheet, TextInput, TouchableOpacity, Text, SafeAreaView, View, Alert, ScrollView, Image, ActivityIndicator } from 'react-native'

import Tab1 from './searchScreen/Facilitator';
import Tab2 from './searchScreen/Online';
import Tab3 from './searchScreen/Offline';
import Tab4 from './searchScreen/Ecommerce';

import { facilitatorListAll, workshopListAll } from '../../../api/home_api'

export default class TabsAdvancedExample extends Component {
   constructor(props) {
      super(props);
      this.state = {
         searchKey: '',
         isLoading: false,
         key2: '',
         facilitator: [],
         search_facilitator: [],
         workshop: [],
         search_workshop: []
      }
   }

   async componentDidMount() {
      const api_facilitator = await facilitatorListAll()
      const api_workshop = await workshopListAll()
      this.setState({
         facilitator: api_facilitator.data,
         search_facilitator: api_facilitator.data,
         workshop: api_workshop.data,
         search_workshop: api_workshop.data,
         isLoading: true
      })
   }

   btnSearch = () => {
      const keyword = this.state.searchKey

      var arrFacilitator = this.state.search_facilitator.filter(item => {
         return item.name.toLowerCase().includes(keyword.toLowerCase())
      })
      var arrWorkshop = this.state.search_workshop.filter((item) => {
         return item.title.toLowerCase().includes(keyword.toLowerCase())
      })
      this.setState({
         facilitator: arrFacilitator,
         workshop: arrWorkshop
      })
   }

   renderFacilitator = () => {
      return this.state.facilitator.map(item => {
         return (
            <Tab1 search={item} {...this.props} />
         )
      })
   }

   renderWorkshop = () => {
      return this.state.workshop.map(item => {
         return (
            <Tab3 search={item} {...this.props} />
         )
      })
   }

   renderNull = () => {
      return (
         <View style={{ justifyContent: 'center', alignItems: 'center', height: '100%', paddingTop: 200 }}>
            <Text>data kosong</Text>
         </View>
      )
   }

   isLoading = () => {
      return (
         <View style={{ justifyContent: 'center', alignItems: 'center', height: '100%', paddingTop: 200 }}>
            <ActivityIndicator size="large" color="#0000ff" />
         </View>
      )
   }

   renderList = () => {
      return (
         <Tabs tabBarUnderlineStyle={{ backgroundColor: '#078b6d' }}>
            <Tab heading={
               <TabHeading style={{ backgroundColor: 'white' }}>
                  <Text style={{ color: 'black' }}>Facilitator</Text>
               </TabHeading>}>
               <ScrollView>
                  <View style={{ padding: 10 }}>
                     {
                        this.state.isLoading === false ?
                           this.isLoading() : this.renderFacilitator()
                     }
                  </View>
               </ScrollView>
            </Tab>
            <Tab heading={
               <TabHeading style={{ backgroundColor: 'white' }}>
                  <Text style={{ color: 'black' }}>Workshop</Text>
               </TabHeading>}>
               <ScrollView>
                  <View style={{ padding: 10 }}>
                     {
                        this.state.isLoading === false ?
                           this.isLoading() : this.renderWorkshop()
                     }
                  </View>
               </ScrollView>
            </Tab>
            {/* <Tab heading={
               <TabHeading style={{ backgroundColor: 'white' }}>
                  <Text style={{ color: 'black' }}>Video</Text>
               </TabHeading>}>
               <Tab2 {...this.props} />
            </Tab>
            <Tab heading={
               <TabHeading style={{ backgroundColor: 'white' }}>
                  <Text style={{ color: 'black' }}>Ecommerce</Text>
               </TabHeading>}>
               <Tab4 {...this.props} />
            </Tab> */}
         </Tabs>
      )
   }

   render() {
      return (
         <Container>
            <SafeAreaView>
               <View style={{ flexDirection: 'row', paddingHorizontal: 15, paddingVertical: 15, justifyContent: 'center', backgroundColor: 'white', borderColor: '#078b6d' }}>
                  <TextInput
                     style={{ borderColor: '#078b6d', borderWidth: 2, flex: 3, borderRadius: 50, paddingHorizontal: 20, backgroundColor: 'white', height: 50 }}
                     placeholder="Search"
                     onChangeText={(text) => this.setState({ searchKey: text })}
                  />
                  <View style={{ flex: 1, justifyContent: 'center', height: 50 }}>
                     <TouchableOpacity style={styles.Button}
                        onPress={() => { this.btnSearch() }}
                     >
                        <Text style={styles.textButton}>Search</Text>
                     </TouchableOpacity>
                  </View>
               </View>
               <View style={{ justifyContent: 'center', alignItems: 'center', height: '90%', width: '100%' }}>
                  {
                     this.renderList()
                  }
               </View>
            </SafeAreaView>
         </Container>
      );
   }
}

const styles = StyleSheet.create({
   button: {
      backgroundColor: 'pink', alignContent: 'center', justifyContent: 'center', borderRadius: 10, marginRight: 5, marginVertical: 5,

   },
   textButton: {
      color: '#078b6d',
      textAlign: 'center',
      fontSize: 15,
      paddingVertical: 5,
   }
})
