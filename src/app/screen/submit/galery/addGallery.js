import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, TouchableOpacity, Alert, ActivityIndicator } from 'react-native'
import { SafeAreaView } from 'react-navigation'
import { ScrollView } from 'react-native-gesture-handler'
import ImagePicker from 'react-native-image-picker';
import { DatePicker, Picker, Icon, } from 'native-base'
import Icons from 'react-native-vector-icons/AntDesign';

import HeaderApp from '../../../components/HeaderApp'
import { getWorkshop } from '../../../../api/Workshop_api'
import { createGallery } from '../../../../api/gallery_api';

export default class addGallery extends Component {
   constructor(props) {
      // console.log('add', props.navigation.state.params.data);
      super(props);
      this.state = {
         id_user: props.navigation.state.params.data,
         photo: '',
         trainer: '',
         selected: '',
         workshop: '',
         id_workhsop: '',
         isLoading: false
      };
   }

   componentDidMount() {
      this.getWorkshop()
   }

   getWorkshop = async () => {
      const data = this.state.id_user
      const api = await getWorkshop({ id_user: data })
      this.setState({
         workshop: api.data.data
      })
      // console.log("api wk", api.data.data);
   }

   onValueChange(value) {
      this.setState({
         selected: value.title,
         id_workhsop: value.id
      });
   }

   openImage = () => {
      // More info on all the options is below in the API Reference... just some common use cases shown here
      const options = {
         title: 'Select Avatar',
         customButtons: [
            {
               name: 'fb', title: 'Choose Photo from Facebook',
               name: 'google drive', title: 'Choose Photo from Google Drive'
            }
         ],
         storageOptions: {
            skipBackup: true,
            path: 'images',
            quality: 0.2
         },
      };
      /**
       * The first arg is the options object for customization (it can also be null or omitted for default options),
       * The second arg is the callback which sends object: response (more info in the API Reference)
       */
      ImagePicker.showImagePicker(options, (response) => {
         console.log('Response = ', response);

         if (response.didCancel) {
            console.log('User cancelled image picker');
         } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
         } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
         } else {
            // const source = { uri: response.uri };

            // You can also display the image using data:
            const source = response;
            this.setState({
               photo: source,
            });
         }
      });
   }

   deleteImage = () => {
      this.setState({
         photo: '',
      })
   }

   onButtonClick = async () => {
      this.setState({
         isLoading: true
      })

      if (this.state.photo === '' || this.state.selected === '') {
         Alert.alert('Notifikasi', 'Mohon Pilih foto terlebih dahulu')
         this.setState({
            isLoading: false
         })
      } else {
         const data = {
            gambar: this.state.photo,
            id_user: this.state.id_user,
            id_workshop: this.state.id_workhsop
         }
         console.log('data', data);
         const result = await createGallery(data)
         if (result) {
            this.setState({
               isLoading: false
            })
         }
         console.log('hasil update', data);
         // Alert.alert('Berhasil');
         this.props.navigation.navigate('Gallery')

      }
   }

   render() {
      // console.log('state wk', this.state.id_workhsop);
      if (this.state.isLoading == false) {
         return (
            <SafeAreaView style={{ flex: 1 }}>
               <HeaderApp
                  props={this.props.navigation}
                  tittle="Add Gallery"
               />
               <ScrollView>
                  <View style={[styles.container]}>
                     <View style={{ justifyContent: 'center', alignItems: 'center', height: 80 }}>
                        <Text style={{ fontSize: 25, fontWeight: 'bold' }}>Add Gallery</Text>
                        <Text style={{ color: 'red', fontStyle: 'italic', }}>* Pastikan data yang diisi sudah benar *</Text>
                     </View>
                     <View>
                        <Text style={styles.text}>Insert Photo Gallery</Text>
                        <View style={{ flexDirection: 'row', marginVertical: 15 }}>
                           <Image
                              style={{ backgroundColor: '#ea2c62', height: 150, width: '55%', borderRadius: 10 }}
                              source={this.state.photo}
                           />
                           <View style={{ justifyContent: 'center', width: '40%', marginLeft: 10 }}>
                              {
                                 this.state.photo ?
                                    <TouchableOpacity
                                       onPress={() => this.deleteImage()}
                                       style={{
                                          justifyContent: 'center',
                                          alignItems: 'center',
                                          width: '40%',
                                          backgroundColor: '#16a596',
                                          borderRadius: 25,
                                          paddingVertical: 15
                                       }}>
                                       <Text style={{ justifyContent: 'center', alignItems: 'center', color: 'white', fontSize: 15 }}>Cancel</Text>
                                    </TouchableOpacity> :
                                    <TouchableOpacity
                                       onPress={() => this.openImage()}
                                       style={{
                                          justifyContent: 'center',
                                          alignItems: 'center',
                                          width: '100%',
                                          backgroundColor: '#16a596',
                                          borderRadius: 25,
                                          paddingVertical: 15
                                       }}>
                                       <Text style={{ justifyContent: 'center', alignItems: 'center', color: 'white', fontSize: 15 }}>Choose Photo</Text>
                                    </TouchableOpacity>
                              }
                           </View>
                        </View>
                        <View style={{}}>
                           <Text style={styles.text}>Workshop</Text>
                           <View style={{ backgroundColor: 'white', borderRadius: 50, marginVertical: 10 }}>
                              <Picker
                                 mode="dropdown"
                                 placeholder={this.state.selected}
                                 iosHeader="Select Workshop"
                                 iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}
                                 style={{ width: undefined }}
                                 selectedValue={this.state.selected}
                                 onValueChange={this.onValueChange.bind(this)}
                              >
                                 {
                                    this.state.workshop !== '' ?
                                       this.state.workshop.map((pic) => {
                                          return (
                                             <Picker.Item label={pic.title} value={pic} />
                                          )
                                       })
                                       : null
                                 }
                              </Picker>
                           </View>
                           <TouchableOpacity
                              onPress={() => this.onButtonClick()}
                              style={{
                                 marginTop: 10,
                                 justifyContent: 'center',
                                 alignItems: 'center',
                                 width: '100%',
                                 backgroundColor: '#16a596',
                                 borderRadius: 25,
                                 paddingVertical: 15
                              }}>
                              <Text style={{ color: 'white', fontSize: 18 }}>Submit</Text>
                           </TouchableOpacity>
                        </View>

                     </View>

                  </View>
               </ScrollView>
            </SafeAreaView>
         )
      } else {
         return (
            <SafeAreaView style={{ height: '100%', justifyContent: 'center', alignItems: 'center' }}>
               <Text style={{ paddingVertical: 15 }}>Please Wait</Text>
               <ActivityIndicator size="large" />
            </SafeAreaView>
         )
      }
   }
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      justifyContent: "center",
      marginHorizontal: 15
   },
   text: {
      fontSize: 18,
      fontWeight: '600',
      color: '#16a596'
   },
   buttonText: {
      fontSize: 20,
      fontWeight: '500',
      color: '#ffffff',
      textAlign: 'center',
      alignItems: 'center',
      justifyContent: 'center'
   },
   button: {
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      backgroundColor: '#078b6d',
      borderRadius: 25,
      marginVertical: 15,
      paddingVertical: 15
   },
})
