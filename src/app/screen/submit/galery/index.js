import React, { Component } from 'react'
import { Text, StyleSheet, View, SafeAreaView, ScrollView, Image, Alert, RefreshControl } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { connect } from 'react-redux'

import HeaderApp from '../../../components/HeaderApp'

import { getGallery, deleteGallery } from '../../../../api/gallery_api'
const source = 'https://skills.id/admin/source/images/gallery/'

class Index extends Component {
   constructor(props) {
      super(props);
      console.log('PROPS', props)
      this.state = {
         id_user: this.props.user.auth.data.id_user,
         gallery: '',
         workshop: '',
         refreshing: false,
      };
   }

   componentDidMount() {
      this.getGallery()
   }

   _onRefresh() {
      this.setState({ refreshing: true });
      this.getGallery().then(() => {
         this.setState({ refreshing: false });
      });
   }

   getGallery = async () => {
      const data = this.props.user.auth.data.id_user
      const api = await getGallery({ id_user: data })
      this.setState({
         gallery: api.data.data
      })
      console.log("gallery get", api.data.data);
   }

   alertButton(item) {
      Alert.alert(
         'Notifikasi',
         'Anda yakin untuk menghapus data gallery ?',
         [
            // { text: 'Ask me later', onPress: () => console.warn('Ask me later pressed') },
            { text: 'NO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
            { text: 'YES', onPress: () => this.onButtonDelete(item) },
         ]
      );
   }

   onButtonDelete = async (item) => {
      const data = {
         id_gallery: item
      }
      console.log(item);
      const api = await deleteGallery(data)
      console.log("hasil api", api);
      { this.getGallery() }
   }

   render() {
      // console.log("gallery get", this.state.gallery);
      return (
         <SafeAreaView style={{ flex: 1, }}>
            <HeaderApp
               props={this.props.navigation}
               tittle="Gallery Page"
            />
            <ScrollView style={{ height: '100%' }}
               refreshControl={
                  <RefreshControl
                     refreshing={this.state.refreshing}
                     onRefresh={this._onRefresh.bind(this)}
                  />
               }
            >
               <View style={styles.container}>
                  {
                     this.state.gallery !== '' && this.state.gallery !== [] ?
                        this.state.gallery.map((data) => {
                           return (
                              <View style={styles.card}>
                                 <Image
                                    style={{ width: 90, borderTopLeftRadius: 10, borderBottomLeftRadius: 10 }}
                                    source={{ uri: source + data.gambar }}
                                 />
                                 <View style={[styles.content, { justifyContent: 'space-around', marginHorizontal: 10, paddingVertical: 10, minHeight: 100 }]}>
                                    <Text style={styles.title}>{data.title}</Text>
                                    <Text style={styles.host}>Author : {data.hosted_by}</Text>
                                 </View>
                                 <View>
                                    <TouchableOpacity
                                       onPress={() => this.alertButton(data.id_gallery)}
                                       style={[styles.content, { alignSelf: 'center' }]}>
                                       <Text style={styles.button}>Delete</Text>
                                    </TouchableOpacity>
                                 </View>
                              </View>
                           )
                        })
                        :
                        <View style={{ height: 200, justifyContent: 'center', alignItems: 'center' }}>
                           <Text style={{ textAlign: 'center', }}>Data Gallery Kosong</Text>
                        </View>
                  }
               </View>
            </ScrollView>
            <View
               style={{
                  width: 80,
                  height: 80,
                  borderRadius: 50,
                  backgroundColor: '#078b6d',
                  position: 'absolute',
                  bottom: 40,
                  right: 20,
               }}
            >
               <TouchableOpacity style={{
                  width: 80,
                  height: 80,
                  borderRadius: 50,
               }}
                  onPress={() => this.props.navigation.navigate('addGallery', { data: this.props.user.auth.data.id_user })}>
                  <Text style={{ textAlign: 'center', marginTop: 30, fontSize: 15, color: 'white' }}>ADD</Text>
               </TouchableOpacity>
            </View>
         </SafeAreaView>
      )
   }
}

const mapStateToProps = state => {
   return {
      user: state.auth
   }
}

export default connect(mapStateToProps, { getGallery })(Index)

const styles = StyleSheet.create({
   container: {
      marginVertical: 5,
      marginHorizontal: 15,
      marginBottom: 100
   },
   card: {
      flexDirection: 'row',
      backgroundColor: 'white',
      borderRadius: 10,
      marginBottom: 10
   },
   content: {
      flex: 1,
   },
   title: {
      fontSize: 15,
      fontWeight: '500',
   },
   host: {
      fontSize: 15,
      fontWeight: '300',
   },
   button: {
      flex: 1,
      padding: 10,
      marginRight: 15, color: '#078b6d'
   }
})
