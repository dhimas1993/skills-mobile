import React, { Component } from 'react'
import { Text, StyleSheet, View, SafeAreaView, ScrollView, Image, Alert, RefreshControl } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { connect } from 'react-redux'

import HeaderApp from '../../../components/HeaderApp'
import { getTrainer, deleteTrainer } from '../../../../api/trainier_api'

const source = 'https://skills.id/admin/source/images/speaker/'

class Index extends Component {
   constructor(props) {
      super(props);
      this.state = {
         id_user: this.props.user.auth.data.id_user,
         trainer: '',
         refreshing: false
      };
   }

   async componentDidMount() {
      this.getTrainer()
   }

   // componentDidUpdate() {
   //    this.getTrainer()
   // }

   _onRefresh() {
      this.setState({ refreshing: true });
      this.getTrainer().then(() => {
         this.setState({ refreshing: false });
      });
   }

   getTrainer = async () => {
      const data = this.props.user.auth.data.id_user
      const api = await getTrainer({ id_user: data })
      this.setState({
         trainer: api.data.data
      })
      console.log('trainer', api.data.data);
   }

   alertButton(item) {
      Alert.alert(
         'Alert Title',
         'Alert message here...',
         [
            { text: 'Ask me later', onPress: () => console.warn('Ask me later pressed') },
            { text: 'NO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
            { text: 'YES', onPress: () => this.onButtonDelete(item) },
         ]
      );
   }

   onButtonDelete = async (item) => {
      const data = {
         id: item
      }
      console.log(item);
      const api = await deleteTrainer(data)

      { this.getTrainer() }

   }

   render() {
      return (
         <SafeAreaView style={{ flex: 1, }}>
            <HeaderApp
               props={this.props.navigation}
               tittle="Trainer Page"
            />
            <ScrollView style={{ height: '100%' }}
               refreshControl={
                  <RefreshControl
                     refreshing={this.state.refreshing}
                     onRefresh={this._onRefresh.bind(this)}
                  />
               }
            >
               <View style={styles.container}>
                  {
                     this.state.trainer !== '' ?
                        this.state.trainer.map((data) => {
                           return (
                              <View style={styles.card}>
                                 <Image
                                    style={{ width: 90, borderTopLeftRadius: 10, borderBottomLeftRadius: 10 }}
                                    source={{ uri: source + data.photo }}
                                 />
                                 <View style={[styles.content, { justifyContent: 'center', marginHorizontal: 10, paddingVertical: 10 }]}>
                                    <Text style={styles.title}>{data.name}</Text>
                                    <Text>{data.email}</Text>
                                    <Text>Status : {data.status}</Text>
                                 </View>
                                 <View>
                                    <TouchableOpacity
                                       onPress={() => { this.props.navigation.navigate('editTrainer', { data: data }) }}
                                       style={[styles.content, { alignSelf: 'center' }]}>
                                       <Text style={styles.button}>Edit</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                       onPress={() => this.alertButton(data.id)}
                                       style={[styles.content, { alignSelf: 'center' }]}>
                                       <Text style={styles.button}>Delete</Text>
                                    </TouchableOpacity>
                                 </View>
                              </View>
                           )
                        })
                        : null
                  }
               </View>
            </ScrollView>
            <View
               style={{
                  width: 80,
                  height: 80,
                  borderRadius: 50,
                  backgroundColor: '#078b6d',
                  position: 'absolute',
                  bottom: 40,
                  right: 20,
               }}
            >
               <TouchableOpacity style={{
                  width: 80,
                  height: 80,
                  borderRadius: 50,
               }}
                  onPress={() => this.props.navigation.navigate('addTrainer')}>
                  <Text style={{ textAlign: 'center', marginTop: 30, fontSize: 15, color: 'white' }}>ADD</Text>
               </TouchableOpacity>
            </View>
         </SafeAreaView>
      )
   }
}

const mapStateToProps = state => {
   return {
      user: state.auth
   }
}

export default connect(mapStateToProps, { getTrainer })(Index)

const styles = StyleSheet.create({
   container: {
      marginVertical: 5,
      marginHorizontal: 15,
      marginBottom: 100
   },
   card: {
      flexDirection: 'row',
      backgroundColor: 'white',
      borderRadius: 10,
      marginBottom: 10
   },
   content: {
      flex: 1,
   },
   title: {
      fontSize: 18,
      fontWeight: 'bold',
      marginBottom: 5,
      fontStyle: 'italic'
   },
   button: {
      flex: 1,
      padding: 10,
      marginRight: 15, color: '#078b6d'
   }
})
