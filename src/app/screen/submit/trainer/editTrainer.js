import React, { Component, version } from 'react'
import { Text, StyleSheet, View, SafeAreaView, TextInput, ScrollView, Alert, Image, ActivityIndicator } from 'react-native'
import { Picker, Form } from 'native-base'
import { TouchableOpacity } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';

import { updateTrainer, updateTrainerWithPhoto } from '../../../../api/trainier_api'

class addTrainer extends Component {
   constructor(props) {
      // console.log(props.navigation.state.params.data);
      super(props);
      this.state = {
         id: props.navigation.state.params.data.id,
         id_user: props.navigation.state.params.data.id_user,
         name: props.navigation.state.params.data.name,
         email: props.navigation.state.params.data.email,
         kompetensi: props.navigation.state.params.data.competency,
         selected: props.navigation.state.params.data.status,
         photo: '',
         sertifikasi: props.navigation.state.params.data.certification,
         deskripsi: props.navigation.state.params.data.desc,
         image: props.navigation.state.params.data.photo,

         isLoading: false
      };
   }

   onValueChange(value) {
      this.setState({
         selected: value
      });
      Alert.alert(this.state.selected)
   }

   openImage = () => {
      // More info on all the options is below in the API Reference... just some common use cases shown here
      const options = {
         title: 'Select Avatar',
         customButtons: [
            {
               name: 'fb', title: 'Choose Photo from Facebook',
               name: 'google drive', title: 'Choose Photo from Google Drive'
            }
         ],
         storageOptions: {
            skipBackup: true,
            path: 'images',

         },
         quality: 0.3
      };
      /**
       * The first arg is the options object for customization (it can also be null or omitted for default options),
       * The second arg is the callback which sends object: response (more info in the API Reference)
       */
      ImagePicker.showImagePicker(options, (response) => {
         console.log('Response = ', response);

         if (response.didCancel) {
            console.log('User cancelled image picker');
         } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
         } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
         } else {
            // const source = { uri: response.uri };

            // You can also display the image using data:
            const source = response;
            this.setState({
               photo: source,
            });
         }
      });
   }

   deleteImage = () => {
      this.setState({
         photo: ''
      })
   }

   onButtonClick = async () => {
      this.setState({
         isLoading: true
      })

      if (this.state.photo == '') {
         const data = {
            id: this.state.id,
            name: this.state.name,
            email: this.state.email.trim(),
            competency: this.state.kompetensi,
            desc: this.state.deskripsi,
            certification: this.state.sertifikasi,
            status: this.state.selected,
            id_user: this.state.id_user,
         }
         const result = await updateTrainer(data)
         if (result) {
            this.setState({
               isLoading: false
            })
         }
         console.log('hasil update', result);
         Alert.alert('hasil update', result.data.message);
         { this.props.navigation.navigate('Trainer') }
      } else {
         const data = {
            id: this.state.id,
            name: this.state.name,
            email: this.state.email.trim(),
            competency: this.state.kompetensi,
            desc: this.state.deskripsi,
            certification: this.state.sertifikasi,
            status: this.state.selected,
            id_user: this.state.id_user,
            photo: this.state.photo
         }
         const result = await updateTrainerWithPhoto(data)
         if (result) {
            this.setState({
               isLoading: false
            })
         }
         // console.log('hasil update', result);
         // console.log('hasil photo', this.state.photo);
         Alert.alert('hasil update', result.data.message);
         this.props.navigation.navigate('Trainer')
      }

   }

   render() {
      if (this.state.isLoading == false) {
         return (
            <SafeAreaView style={{ height: '100%' }}>
               <ScrollView style={{ height: '100%', backgroundColor: 'white' }}>
                  <View style={[styles.container, { marginBottom: 300 }]}>
                     <View style={{ justifyContent: 'center', alignItems: 'center', height: 80, marginTop: 50 }}>
                        <Text style={{ fontSize: 25, fontWeight: 'bold' }}>Edit your trainer</Text>
                        <Text style={{ color: 'red', fontStyle: 'italic', }}>* Pastikan data yang diisi sudah benar *</Text>
                     </View>
                     <Text style={styles.text}>Fullname</Text>
                     <TextInput
                        style={styles.inputBox}
                        value={this.state.name}
                        placeholder="name"
                        onChangeText={(text) => this.setState({ name: text })}
                     />
                     <Text style={styles.text}>Email</Text>
                     <TextInput
                        editable={false}
                        style={styles.inputBox}
                        value={this.state.email}
                        placeholder="ex.name@skills.id"
                        onChangeText={(text) => this.setState({ email: text })}
                     />

                     <View style={{ marginVertical: 20 }}>
                        <Text style={styles.text}>Photo Trainer</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 15 }}>
                           <View style={{ backgroundColor: '#ea2c62', height: 150, width: '60%', borderRadius: 10 }}>
                              <Image
                                 style={{ height: 150, width: '100%', borderRadius: 15 }}
                                 source={
                                    this.state.photo

                                 }
                              />
                           </View>
                           {
                              this.state.photo ?
                                 <TouchableOpacity
                                    onPress={() => this.deleteImage()}
                                    style={{
                                       paddingHorizontal: 20,
                                       justifyContent: 'center',
                                       alignItems: 'center',
                                       width: '100%',
                                       backgroundColor: '#16a596',
                                       borderRadius: 25,
                                       paddingVertical: 15
                                    }}>
                                    <Text style={{ justifyContent: 'center', alignItems: 'center', color: 'white', fontSize: 15 }}>Cancel</Text>
                                 </TouchableOpacity>
                                 :
                                 <TouchableOpacity
                                    onPress={() => this.openImage()}
                                    style={{
                                       paddingHorizontal: 20,
                                       justifyContent: 'center',
                                       alignItems: 'center',
                                       width: '100%',
                                       backgroundColor: '#16a596',
                                       borderRadius: 25,
                                       paddingVertical: 15
                                    }}>
                                    <Text style={{ justifyContent: 'center', alignItems: 'center', color: 'white', fontSize: 15 }}>Choose Photo</Text>
                                 </TouchableOpacity>
                           }
                        </View>
                     </View>

                     <Text style={[styles.text]}>Kompetensi</Text>
                     <TextInput
                        multiline
                        style={[styles.inputBox, { paddingTop: 15, }]}
                        value={this.state.kompetensi}
                        placeholder="deskripsikan kelebihan anda"
                        onChangeText={(text) => this.setState({ kompetensi: text })}
                     />
                     <Text style={[styles.text]}>Deskripsi</Text>
                     <TextInput
                        multiline
                        style={[styles.inputBox, { paddingTop: 15, }]}
                        value={this.state.deskripsi}
                        placeholder="deskripsikan kelebihan anda"
                        onChangeText={(text) => this.setState({ deskripsi: text })}
                     />
                     <Text style={[styles.text]}>Sertifikasi</Text>
                     <TextInput
                        multiline
                        style={[styles.inputBox, { paddingTop: 15, }]}
                        value={this.state.sertifikasi}
                        placeholder="deskripsikan kelebihan anda"
                        onChangeText={(text) => this.setState({ sertifikasi: text })}
                     />

                     <Text style={[styles.text]}>Status</Text>
                     <View style={styles.inputBox}>
                        <Form>
                           <Picker
                              note
                              mode="dropdown"
                              style={{ width: 120 }}
                              selectedValue={this.state.selected}
                              onValueChange={this.onValueChange.bind(this)}
                           >
                              <Picker.Item label="Aktif" value="Aktif" />
                              <Picker.Item label="Tidak Aktif" value="Tidak Aktif" />
                           </Picker>
                        </Form>
                     </View>
                     <TouchableOpacity
                        onPress={() => this.onButtonClick()}
                        style={styles.button}>
                        <Text style={styles.buttonText}>save</Text>
                     </TouchableOpacity>
                  </View>
               </ScrollView>
            </SafeAreaView>
         )
      } else {
         return (
            <View style={[styles.containerloading, styles.horizontal]}>
               <View style={{ flexDirection: 'row' }}>
                  <Text style={{ color: 'black' }}>Silahkan Tunggu</Text>
                  <ActivityIndicator size="large" />
               </View>
            </View>
         );
      }
   }
}

export default addTrainer

const styles = StyleSheet.create({
   containerloading: {
      flex: 1,
      justifyContent: "center"
   },
   horizontal: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 10
   },
   container: {
      marginHorizontal: 15
   },
   text: {
      fontSize: 18,
      fontWeight: '600',
      color: '#16a596'
   },
   inputBox: {
      width: '100%',
      marginBottom: 25,
      borderWidth: 1.5,
      // borderStyle: 'dashed',
      borderBottomColor: '#ea2c62',
      borderTopColor: 'white',
      borderRightColor: 'white',
      borderLeftColor: 'white',
      height: 50,
      backgroundColor: 'white',
      borderRadius: 0,
      fontSize: 15,
      marginVertical: 10,
   },
   button: {
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      backgroundColor: '#078b6d',
      borderRadius: 25,
      marginVertical: 15,
      paddingVertical: 15
   },
   buttonText: {
      fontSize: 16,
      fontWeight: '500',
      color: '#ffffff',
      textAlign: 'center',
      alignItems: 'center',
   },
})
