import React, { Component } from 'react'
import { Text, StyleSheet, View, SafeAreaView, ScrollView, Image, Alert, RefreshControl } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { connect } from 'react-redux'

import HeaderApp from '../../../components/HeaderApp'
import { getWorkshop, deleteWorkshop } from '../../../../api/Workshop_api'

const source = 'https://skills.id/admin/source/images/thumbnail/'

class Index extends Component {
   constructor(props) {
      super(props);
      this.state = {
         id_user: this.props.user.auth.data.id_user,
         trainer: '',
         refreshing: false
      };
   }

   async componentDidMount() {
      this.getWorkshop()
   }

   // async componentDidUpdate() {
   //    this.getWorkshop()
   // }

   _onRefresh() {
      this.setState({ refreshing: true });
      this.getWorkshop().then(() => {
         this.setState({ refreshing: false });
      });
   }

   getWorkshop = async () => {
      const data = this.props.user.auth.data.id_user
      const api = await getWorkshop({ id_user: data })
      this.setState({
         trainer: api.data.data
      })
      console.log(api.data.data);
   }

   alertButton(item) {
      Alert.alert(
         'Alert Title',
         'Alert message here...',
         [
            { text: 'NO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
            { text: 'YES', onPress: () => this.onButtonDelete(item) },
         ]
      );
   }

   onButtonDelete = async (item) => {
      const data = {
         id: item
      }
      console.log(item);
      const api = await deleteWorkshop(data)

      this.getWorkshop()
   }

   render() {
      // console.log(this.state.trainer);
      return (
         <SafeAreaView style={{ flex: 1, }}>
            <HeaderApp
               props={this.props.navigation}
               tittle="Workshop Page"
            />
            <ScrollView style={{ height: '100%' }}
               refreshControl={
                  <RefreshControl
                     refreshing={this.state.refreshing}
                     onRefresh={this._onRefresh.bind(this)}
                  />
               }
            >
               <View style={styles.container}>
                  {
                     this.state.trainer !== '' ?
                        this.state.trainer.map((data) => {
                           return (
                              <View style={styles.card}>
                                 <Image
                                    style={{ width: 90, borderTopLeftRadius: 10, borderBottomLeftRadius: 10 }}
                                    source={{ uri: source + data.image }}
                                 />
                                 <View style={[styles.content, { justifyContent: 'center', marginHorizontal: 10, paddingVertical: 10 }]}>
                                    <Text style={styles.title}>{data.title}</Text>
                                    <Text>Price : Rp. {Number(data.price).toLocaleString()}</Text>
                                    <Text>Status : {data.status}</Text>
                                 </View>
                                 <View>
                                    <TouchableOpacity
                                       onPress={() => { this.props.navigation.navigate('editWorkshop', { data: data }) }}
                                       style={[styles.content, { alignSelf: 'center' }]}>
                                       <Text style={styles.button}>Edit</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                       onPress={() => this.alertButton(data.id)}
                                       style={[styles.content, { alignSelf: 'center' }]}>
                                       <Text style={styles.button}>Delete</Text>
                                    </TouchableOpacity>
                                 </View>
                              </View>
                           )
                        })
                        : null
                  }
               </View>
            </ScrollView>
            <View
               style={{
                  width: 80,
                  height: 80,
                  borderRadius: 50,
                  backgroundColor: '#078b6d',
                  position: 'absolute',
                  bottom: 40,
                  right: 20,
               }}
            >
               <TouchableOpacity style={{
                  width: 80,
                  height: 80,
                  borderRadius: 50,
               }}
                  onPress={() => this.props.navigation.navigate('SubmitWorkshop', { data: this.state.id_user })}>
                  <Text style={{ textAlign: 'center', marginTop: 30, fontSize: 15, color: 'white' }}>ADD</Text>
               </TouchableOpacity>
            </View>
         </SafeAreaView>
      )
   }
}

const mapStateToProps = state => {
   return {
      user: state.auth
   }
}

export default connect(mapStateToProps, { getWorkshop })(Index)

const styles = StyleSheet.create({
   container: {
      marginVertical: 5,
      marginHorizontal: 15,
      marginBottom: 100
   },
   card: {
      flexDirection: 'row',
      backgroundColor: 'white',
      borderRadius: 10,
      marginBottom: 10
   },
   content: {
      flex: 1,
   },
   title: {
      fontSize: 18,
      fontWeight: 'bold',
      marginBottom: 5,
      fontStyle: 'italic'
   },
   button: {
      flex: 1,
      padding: 10,
      marginRight: 15, color: '#078b6d'
   }
})
