import React, { Component } from 'react'
import { Text, StyleSheet, View, WebView, TextInput, ScrollView, TouchableOpacity, SafeAreaView, Image, Alert, Button } from 'react-native'
import { DatePicker, Picker, Icon, } from 'native-base'
import ImagePicker from 'react-native-image-picker';
import DateTimePicker from '@react-native-community/datetimepicker';
import Icons from 'react-native-vector-icons/AntDesign';
import HeaderApp from '../../../components/HeaderApp'

import { getTrainer } from '../../../../api/trainier_api'
import { updateWorkshopWithPhoto, updateWorkshop } from '../../../../api/Workshop_api'

export default class SubmitWorkshop extends Component {
   constructor(props) {
      console.log('editwork', props.navigation.state.params.data);
      super(props);
      this.state = {
         isLoading: false,
         trainer: '',
         photo: '',
         date: new Date(),
         date_end: new Date(),
         mode: 'date',
         mode_end: 'date',
         show: false,
         show_end: false,

         chosenDate_start: '',
         chosenDate_end: '',
         selected: null,
         id: props.navigation.state.params.data.id,
         title: props.navigation.state.params.data.title,
         desc: props.navigation.state.params.data.desc,
         hosted_by: props.navigation.state.params.data.hosted_by,
         start_date: props.navigation.state.params.data.start_date,
         end_date: props.navigation.state.params.data.end_date,
         class_size: props.navigation.state.params.data.class_size,
         min_age: props.navigation.state.params.data.min_age,
         language: props.navigation.state.params.data.language,
         price: props.navigation.state.params.data.price,
         level: props.navigation.state.params.data.price,
         status: props.navigation.state.params.data.status,
         image: props.navigation.state.params.data.image,
         location: props.navigation.state.params.data.location,
         keyword: props.navigation.state.params.data.keyword,
         gender: props.navigation.state.params.data.gender,
         what_you_learn: props.navigation.state.params.data.what_you_learn,
         terms: props.navigation.state.params.data.terms,
         slug: props.navigation.state.params.data.slug,
         category: props.navigation.state.params.data.category,
         last_update: props.navigation.state.params.data.last_update,
         id_user: props.navigation.state.params.data.id_user,
         id_speaker: props.navigation.state.params.data.id_speaker,
         counter: props.navigation.state.params.data.counter,
         inclusive: props.navigation.state.params.data.inclusive,
         what_to_bring: props.navigation.state.params.data.what_to_bring,
         time: props.navigation.state.params.data.time,
         multi_tanggal: props.navigation.state.params.data.multi_tanggal,
         kota: props.navigation.state.params.data.kota,
         subcategory: props.navigation.state.params.data.subcategory,
         time_end: props.navigation.state.params.data.time_end,
         tempat: props.navigation.state.params.data.tempat,
         status_publish: props.navigation.state.params.data.status_publish
      };
      this.setDate_start = this.setDate_start.bind(this);
      this.setDate_end = this.setDate_end.bind(this);
   }

   onValueChange_level(value) {
      this.setState({
         level: value
      });
   }

   setDate = (event, date) => {
      date = date || this.state.date;

      this.setState({
         show: Platform.OS === 'ios' ? true : false,
         date,
      });
   }

   setDate_end = (event, date_end) => {
      date_end = date_end || this.state.date_end;

      this.setState({
         show_end: Platform.OS === 'ios' ? true : false,
         date_end,
      });
   }

   show = mode => {
      this.setState({
         show: true,
         mode,
      });
   }

   show_end = mode_end => {
      this.setState({
         show_end: true,
         mode_end,
      });
   }

   datepicker = () => {
      this.show('date');
   }

   timepicker = () => {
      this.show('time');
   }

   datepicker_end = () => {
      this.show_end('date');
   }

   timepicker_end = () => {
      this.show_end('time');
   }

   setDate_start(newDate) {
      this.setState({
         chosenDate_start: newDate
      });
   }
   setDate_end(newDate) {
      this.setState({
         chosenDate_end: newDate
      });
   }

   onValueChange(value) {
      this.setState({
         selected: value
      });
   }

   async componentDidMount() {
      this.getTrainer()
      // console.log(get_trainer.data.data);
   }

   getTrainer = async () => {
      const data = {
         id_user: 224
      }
      const get_trainer = await getTrainer(data)
      this.setState({
         trainer: get_trainer.data.data,
      })
   }

   openImage = () => {
      // More info on all the options is below in the API Reference... just some common use cases shown here
      const options = {
         title: 'Select Avatar',
         customButtons: [
            {
               name: 'fb', title: 'Choose Photo from Facebook',
               name: 'google drive', title: 'Choose Photo from Google Drive'
            }
         ],
         storageOptions: {
            skipBackup: true,
            path: 'images',
            quality: 0.2
         },
      };
      /**
       * The first arg is the options object for customization (it can also be null or omitted for default options),
       * The second arg is the callback which sends object: response (more info in the API Reference)
       */
      ImagePicker.showImagePicker(options, (response) => {
         console.log('Response = ', response);

         if (response.didCancel) {
            console.log('User cancelled image picker');
         } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
         } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
         } else {
            // const source = { uri: response.uri };

            // You can also display the image using data:
            const source = response;
            this.setState({
               photo: source,
            });
         }
      });
   }

   deleteImage = async () => {
      this.setState({
         photo: ''
      })
      { this.getTrainer() }
   }

   onButtonClick = async () => {
      this.setState({
         isLoading: true
      })

      const current_date = new Date()

      const start_date_date = this.state.date.getDate();
      const start_date_month = this.state.date.getMonth() + 1;
      const start_date_year = this.state.date.getFullYear();
      const time_start = this.state.date.toLocaleTimeString().slice(0, 5);

      const end_date_date = this.state.date_end.getDate()
      const end_date_month = this.state.date_end.getMonth() + 1
      const end_date_year = this.state.date_end.getFullYear()
      const time_end = this.state.date_end.toLocaleTimeString().slice(0, 5)

      if (this.state.photo !== '') {
         this.setState({
            isLoading: true
         })

         const data = {
            id: this.state.id,
            title: this.state.title,
            desc: this.state.desc,
            hosted_by: this.state.selected,
            start_date: start_date_year + "-" + start_date_month + "-" + start_date_date,
            end_date: end_date_year + "-" + end_date_month + "-" + end_date_date,
            class_size: this.state.class_size,
            min_age: this.state.min_age,
            language: this.state.language,
            price: this.state.price,
            level: this.state.price,
            status: this.state.status,
            photo: this.state.photo,
            location: this.state.location,
            keyword: this.state.keyword,
            gender: this.state.gender,
            what_you_learn: this.state.what_you_learn,
            terms: this.state.terms,
            slug: this.state.title.replace(/\s/g, '-'),
            category: this.state.category,
            last_update: current_date.getFullYear() + "-" + current_date.getMonth() + "-" + current_date.getDate(),
            id_user: this.state.id_user,
            id_speaker: this.state.id_speaker,
            counter: this.state.counter,
            inclusive: this.state.inclusive,
            what_to_bring: this.state.what_to_bring,
            time: time_start,
            promotion: null,
            alamat: null,
            kelurahan: null,
            kecamatan: null,
            kotamadya: null,
            kodepos: null,
            promo: null,
            promo_price: null,
            multi_tanggal: this.state.multi_tanggal,
            kota: this.state.kota,
            subcategory: this.state.subcategory,
            time_end: time_end,
            tempat: this.state.tempat,
            posted: null,
            status_publish: this.state.status_publish
         }
         console.log("input backend", data);
         const result = await updateWorkshopWithPhoto(data)
         if (result) {
            this.setState({
               isLoading: false
            })
         }
         console.log('hasil update photo', result);
         Alert.alert('hasil update', result.data.message);
         { this.props.navigation.navigate('ManageWorkshop') }

      } else {
         // console.log("aku");
         const data = {
            id: this.state.id,
            title: this.state.title,
            desc: this.state.desc,
            hosted_by: this.state.selected,
            start_date: start_date_year + "-" + start_date_month + "-" + start_date_date,
            end_date: end_date_year + "-" + end_date_month + "-" + end_date_date,
            class_size: this.state.class_size,
            min_age: this.state.min_age,
            language: this.state.language,
            price: this.state.price,
            level: this.state.level,
            status: this.state.status,
            photo: this.state.photo,
            location: this.state.location,
            keyword: this.state.keyword,
            gender: this.state.gender,
            what_you_learn: this.state.what_you_learn,
            terms: this.state.terms,
            slug: this.state.title.replace(/\s/g, '-'),
            category: this.state.category,
            last_update: current_date.getFullYear() + "-" + current_date.getMonth() + "-" + current_date.getDate(),
            id_user: this.state.id_user,
            id_speaker: this.state.id_speaker,
            counter: this.state.counter,
            inclusive: this.state.inclusive,
            what_to_bring: this.state.what_to_bring,
            time: time_start,
            promotion: null,
            alamat: null,
            kelurahan: null,
            kecamatan: null,
            kotamadya: null,
            kodepos: null,
            promo: null,
            promo_price: null,
            multi_tanggal: this.state.multi_tanggal,
            kota: this.state.kota,
            subcategory: this.state.subcategory,
            time_end: time_end,
            tempat: this.state.tempat,
            posted: null,
            status_publish: this.state.status_publish
         }

         console.log("input backend", data);
         const result = await updateWorkshop(data)
         if (result) {
            this.setState({
               isLoading: false
            })
         }
         console.log('hasil update', result);
         Alert.alert('hasil update', "Berhasil update data workshop");
         { this.props.navigation.navigate('ManageWorkshop') }
      }

   }



   render() {
      const { show, show_end, date, date_end, mode, mode_end } = this.state;
      console.log("start", this.state.level);
      // console.log("end", date_end);
      const source = 'https://skills.id/admin/source/images/thumbnail/'
      return (
         <SafeAreaView style={{ flex: 1 }}>
            <HeaderApp
               props={this.props.navigation}
               tittle="Edit Workshop"
            />
            <ScrollView style={{ backgroundColor: 'white' }}>
               <Text style={{ fontSize: 20, padding: 10, textAlign: 'center' }}>Edit Workshop</Text>
               <View style={{ padding: 10 }}>
                  <Text style={styles.title}>Judul</Text>
                  <TextInput
                     style={styles.inputBox}
                     placeholder="Title"
                     value={this.state.title}
                     multiline={true}
                     onChangeText={(text) => this.setState({ title: text })}
                  />
               </View>
               <View style={{ padding: 10 }}>
                  <Text style={styles.title}>Deskripsi</Text>
                  <TextInput
                     style={styles.inputBoxLong}
                     placeholder="Type something"
                     multiline={true}
                     value={this.state.desc}
                     onChangeText={(text) => this.setState({ desc: text })}
                  />
               </View>
               <View style={{ padding: 10 }}>
                  <Text style={styles.title}>Apa yang akan dipelajari audience</Text>
                  <TextInput
                     style={styles.inputBoxLong}
                     underlineColorAndroid="transparent"
                     placeholder="Type something"
                     multiline={true}
                     value={this.state.what_you_learn}
                     onChangeText={(text) => this.setState({ what_you_learn: text })}
                  />
               </View>
               <View style={{ padding: 10 }}>
                  <Text style={styles.title}>What is the price inclusive of?</Text>
                  <TextInput
                     style={styles.inputBoxLong}
                     underlineColorAndroid="transparent"
                     placeholder="Type something"
                     multiline={true}
                     value={this.state.inclusive}
                     onChangeText={(text) => this.setState({ inclusive: text })}
                  />
               </View>
               <View style={{ padding: 10 }}>
                  <Text style={styles.title}>Apa yang harus dibawa peserta</Text>
                  <TextInput
                     style={styles.inputBoxLong}
                     underlineColorAndroid="transparent"
                     placeholder="Type something"
                     numberOfLines={10}
                     multiline={true}
                     value={this.state.what_to_bring}
                     onChangeText={(text) => this.setState({ what_to_bring: text })}
                  />
               </View>
               <View style={{ padding: 10 }}>
                  <Text style={styles.title}>Syarat dan Ketentuan</Text>
                  <TextInput
                     style={styles.inputBoxLong}
                     underlineColorAndroid="transparent"
                     placeholder="Type something"
                     numberOfLines={10}
                     multiline={true}
                     value={this.state.terms}
                     onChangeText={(text) => this.setState({ terms: text })}
                  />
               </View>

               <View style={{ padding: 10 }}>
                  <Text style={styles.title}>Speaker</Text>
                  <View style={{ backgroundColor: 'white', borderRadius: 50 }}>
                     <Picker
                        mode="dropdown"
                        placeholder={this.state.hosted_by}
                        iosHeader="Select Speaker"
                        iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}
                        style={{ width: undefined }}
                        selectedValue={this.state.selected}
                        onValueChange={this.onValueChange.bind(this)}
                     >
                        {
                           this.state.trainer !== '' ?
                              this.state.trainer.map((pic) => {
                                 return (
                                    <Picker.Item label={pic.name} value={pic.name} />
                                 )
                              })
                              : null
                        }
                     </Picker>
                  </View>
               </View>
               <View style={{ padding: 10 }}>
                  <Text style={styles.title}>Harga</Text>
                  <TextInput
                     style={styles.inputBox}
                     underlineColorAndroid="transparent"
                     keyboardType={"numeric"}
                     placeholder={Number(this.state.price).toLocaleString()}
                     multiline={true}
                     value={this.state.price}
                     onChangeText={(text) => this.setState({ price: text })}
                  />
                  <Text style={{ alignSelf: 'flex-end' }}>Masukan tanpa (,)</Text>
               </View>

               <View style={{ padding: 10 }}>
                  <View style={{ flex: 1 }}>
                     <Text style={styles.title}>Waktu mulai</Text>
                     <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                        <View style={{ flex: 1 }}>
                           <View>
                              <Button onPress={this.datepicker} title="Pilih Tanggal" />
                           </View>
                           <View>
                              <Button onPress={this.timepicker} title="Pilih Jam" />
                           </View>
                           {
                              show && <DateTimePicker value={date}
                                 mode={mode}
                                 is24Hour={false}
                                 display="default"
                                 onChange={this.setDate} />
                           }
                        </View>
                     </View>
                  </View>
                  <View style={{}}>
                     <View style={{ flex: 1 }}>
                        <Text style={styles.title}>Waktu Selesai</Text>
                        <View>
                           <Button onPress={this.datepicker_end} title="Pilih Tanggal" />
                        </View>
                        <View>
                           <Button onPress={this.timepicker_end} title="Pilih Jam" />
                        </View>
                        {
                           show_end && <DateTimePicker value={date_end}
                              mode={mode_end}
                              is24Hour={false}
                              display="default"
                              onChange={this.setDate_end} />
                        }
                     </View>
                  </View>
               </View>

               <View>
                  <Text style={styles.text}>Cover Workshop</Text>
                  <View style={{ flexDirection: 'row', marginHorizontal: 10, marginVertical: 15 }}>
                     <Image
                        style={{ flex: 1, backgroundColor: 'grey', height: 150, borderRadius: 20 }}
                        source={
                           this.state.photo !== '' ?
                              this.state.photo :
                              { uri: source + this.state.image }

                        }
                     />
                     <View style={{ justifyContent: 'center' }}>
                        {
                           this.state.photo ?
                              <TouchableOpacity
                                 onPress={() => this.deleteImage()}
                                 style={{ backgroundColor: '#078b6d', margin: 10, borderRadius: 10, padding: 15, height: 50, justifyContent: 'center' }}>
                                 <Icons name="delete" style={styles.buttonText} />
                              </TouchableOpacity> :
                              <TouchableOpacity
                                 onPress={() => this.openImage()}
                                 style={{ backgroundColor: '#078b6d', margin: 10, borderRadius: 10, padding: 15, height: 50, }}>
                                 {/* <Text style={styles.buttonText}>Choose Photo</Text> */}
                                 <Icons name="pluscircle" style={styles.buttonText} />
                              </TouchableOpacity>
                        }
                     </View>
                  </View>
               </View>

               <View style={{ padding: 10, flexDirection: 'row', justifyContent: 'flex-start' }}>
                  <View style={{ flex: 1 }}>
                     <Text style={styles.title}>Min Usia</Text>
                     <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TextInput
                           style={[styles.inputBox, { width: '80%' }]}
                           underlineColorAndroid="transparent"
                           placeholder="Type"
                           multiline={true}
                           value={this.state.min_age}
                           onChangeText={(text) => this.setState({ min_age: text })}
                        />
                     </View>
                  </View>
                  <View style={{ flex: 1 }}>
                     <Text style={styles.title}>Bahasa</Text>
                     <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TextInput
                           style={[styles.inputBox, { width: '80%' }]}
                           underlineColorAndroid="transparent"
                           placeholder="Type"
                           multiline={true}
                           value={this.state.language}
                           onChangeText={(text) => this.setState({ language: text })}
                        />
                     </View>
                  </View>
               </View>

               <View style={{ padding: 10, flexDirection: 'row', justifyContent: 'flex-start' }}>
                  <View style={{ flex: 1 }}>
                     <Text style={styles.title}>Kapasitas</Text>
                     <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TextInput
                           style={[styles.inputBox, { width: '80%' }]}
                           underlineColorAndroid="transparent"
                           placeholder="Type"
                           multiline={true}
                           value={this.state.class_size}
                           onChangeText={(text) => this.setState({ class_size: text })}
                        />
                     </View>
                  </View>
                  <View style={{ flex: 1 }}>
                     <Text style={styles.title}>Level</Text>
                     <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white', borderRadius: 10, height: 50 }}>
                        {/* <Form> */}
                        <Picker
                           note
                           mode="dropdown"
                           iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}
                           style={[styles.inputBox, { width: '80%' }]}
                           placeholder={this.state.kota}
                           selectedValue={this.state.selected_level}
                           onValueChange={this.onValueChange_level.bind(this)}
                        >
                           <Picker.Item label="Beginner" value="Beginner" />
                           <Picker.Item label="Intermediate" value="Intermediate" />
                           <Picker.Item label="Expert" value="Expert" />
                        </Picker>
                        {/* </Form> */}
                     </View>
                  </View>
               </View>

               <View style={{ padding: 10, flexDirection: 'row', justifyContent: 'flex-start' }}>
                  <View style={{ flex: 1 }}>
                     <Text style={styles.title}>Kategory</Text>
                     <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TextInput
                           style={[styles.inputBox, { width: '80%' }]}
                           underlineColorAndroid="transparent"
                           placeholder="Type"
                           multiline={true}
                           value={this.state.category}
                           onChangeText={(text) => this.setState({ category: text })}
                        />
                     </View>
                  </View>
                  <View style={{ flex: 1 }}>
                     <Text style={styles.title}>Kota</Text>
                     <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TextInput
                           style={[styles.inputBox, { width: '80%' }]}
                           underlineColorAndroid="transparent"
                           placeholder="Type"
                           multiline={true}
                           value={this.state.kota}
                           onChangeText={(text) => this.setState({ kota: text })}
                        />
                     </View>
                  </View>
               </View>

               <View style={{ padding: 10 }}>
                  <Text style={styles.title}>Nama Tempat</Text>
                  <TextInput
                     style={styles.inputBox}
                     underlineColorAndroid="transparent"
                     placeholder="Type something"
                     numberOfLines={10}
                     multiline={true}
                     value={this.state.tempat}
                     onChangeText={(text) => this.setState({ tempat: text })}
                  />
               </View>
               <View style={{ padding: 10 }}>
                  <Text style={styles.title}>Nama Tempat</Text>
                  <TextInput
                     style={styles.inputBoxLong}
                     underlineColorAndroid="transparent"
                     placeholder="Type something"
                     multiline={true}
                     value={this.state.location}
                     onChangeText={(text) => this.setState({ location: text })}
                  />
               </View>

               <View style={{ padding: 15, marginTop: 10, bottom: 0 }}>
                  <TouchableOpacity
                     onPress={() => this.onButtonClick()}
                     style={{ backgroundColor: 'red', height: 50, borderRadius: 25, justifyContent: 'center', alignItems: 'center' }}>
                     <Text style={{ color: 'white', fontSize: 18 }}>Submit</Text>
                  </TouchableOpacity>
               </View>
            </ScrollView>
         </SafeAreaView>
      );
   }
}

const styles = StyleSheet.create({
   title: {
      fontSize: 18,
      fontWeight: '600',
      color: '#16a596'
   },
   buttonText: {
      fontSize: 20,
      fontWeight: '500',
      color: '#ffffff',
      textAlign: 'center',
      alignItems: 'center',
      justifyContent: 'center'
   },
   inputBox: {
      width: '100%',
      marginBottom: 25,
      borderWidth: 1.5,
      // borderStyle: 'dashed',
      borderBottomColor: '#ea2c62',
      borderTopColor: 'white',
      borderRightColor: 'white',
      borderLeftColor: 'white',
      height: 50,
      backgroundColor: 'white',
      borderRadius: 0,
      fontSize: 15,
      marginVertical: 10,
   },
   inputBoxLong: {
      width: '100%',
      marginBottom: 25,
      borderWidth: 1.5,
      // borderStyle: 'dashed',
      borderBottomColor: '#ea2c62',
      borderTopColor: 'white',
      borderRightColor: 'white',
      borderLeftColor: 'white',
      height: 50,
      backgroundColor: 'white',
      borderRadius: 0,
      fontSize: 15,
      marginVertical: 10,
   },
})
