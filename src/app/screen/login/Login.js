import React, { Component } from 'react';
import { View, Text, SafeAreaView } from 'react-native'
import { Container, Header, Tab, Tabs, TabHeading, Icon, Content } from 'native-base';
import Tab1 from './loginScreen/LoginPage';
import Tab2 from './loginScreen/RegisterPageParticipant';
import Tab3 from './loginScreen/RegisterPageFacilitator'

export default class Login extends Component {
   render() {
      return (
         <SafeAreaView style={{ flex: 1 }}>
            <Tabs tabBarUnderlineStyle={{ backgroundColor: '#078b6d' }}>
               <Tab heading=
                  {
                     <TabHeading style={{ backgroundColor: 'white' }}>
                        <Text style={{ color: 'black', fontSize: 15 }}>Login</Text>
                     </TabHeading>
                  }>
                  <Tab1 {...this.props} />
               </Tab>
               <Tab heading=
                  {
                     <TabHeading style={{ backgroundColor: 'white' }}>
                        <Text style={{ color: 'black', fontSize: 13 }}>Sign up Participant</Text>
                     </TabHeading>
                  }>
                  <Tab2 {...this.props} />
               </Tab>
               <Tab heading=
                  {
                     <TabHeading style={{ backgroundColor: 'white' }}>
                        <Text style={{ color: 'black', fontSize: 13 }}>Sign up Facilitator</Text>
                     </TabHeading>
                  }>
                  <Tab3 {...this.props} />
               </Tab>
            </Tabs>
         </SafeAreaView>
      );
   }
}