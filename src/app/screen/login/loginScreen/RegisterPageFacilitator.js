import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, TextInput, Alert, ActivityIndicator, SafeAreaView, Image } from 'react-native'
import { Form, Picker, Icon, Container, Content, DatePicker, Label } from 'native-base'

import { regist_facilitator } from '../../../../api/profile_api'
import { ScrollView } from 'react-native-gesture-handler';

export default class LoginPage extends Component {
   constructor(props) {
      super(props);
      this.state = {
         isLoading: false,
         selected: undefined,
         status: '',
         chosenDate: new Date(),
         name: '',
         email: '',
         no_handphone: '',
         address: '',
         tgl_lahir: '',
         password: '',
         password1: '',
         no_npwp: '',
         nama_npwp: '',
         alamat_npwp: '',
         nama_bank: '',
         no_rekening_bank: '',
         nama_rekening_bank: '',
      };
      this.setDate = this.setDate.bind(this);
   }

   componentDidMount() {
      console.log('facilitator')
   }

   setDate(newDate) {
      console.log('ini tanggal', newDate.getDate());
      console.log('ini tanggal', newDate.getMonth());
      console.log('ini tanggal', newDate.getFullYear());

      const date = newDate.getFullYear() + '-' + newDate.getMonth() + '-' + newDate.getDate()
      this.setState({ chosenDate: date });
   }

   roleValueChange(value) {
      this.setState({
         status: value
      });
   }

   validasi_input = () => {
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      if (this.state.name == '') {
         Alert.alert('Notifikasi', 'Kolom name masih kosong')
      } else if (re.test(this.state.email) == false) {
         Alert.alert('Notifikasi', 'Format email salah')
      } else if (this.state.password == '') {
         Alert.alert('Notifikasi', 'Kolom password masih kosong')
      } else if (this.state.no_handphone == '') {
         Alert.alert('Notifikasi', 'Kolom No Handphone masih kosong')
      } else if (this.state.address == '') {
         Alert.alert('Notifikasi', 'Kolom Address masih kosong')
      } else if (this.state.chosenDate == '') {
         Alert.alert('Notifikasi', 'Kolom tanggal lahir masih kosong')
      } else {
         this.onButtonClick()
      }
   }

   Alert = () =>
      Alert.alert(
         "Notifikasi",
         "Registrasi Berhasil",
         [
            { text: "OK", onPress: () => this.props.navigation.navigate('Verified') }
         ],
         { cancelable: false }
      );

   onButtonClick = async () => {

      this.setState({
         isLoading: true
      })

      const data = {
         name: this.state.name,
         email: this.state.email,
         password: this.state.password,
         no_handphone: this.state.no_handphone,
         location: this.state.address,
         date_of_birth: this.state.chosenDate.toString(),
      }

      if (this.state.password == this.state.password1) {
         const result = await regist_facilitator(data)
         if (result.data.message == 'email sudah ada') {
            this.setState({
               isLoading: false
            })
            Alert.alert("Notifikasi', 'Silahkan gunakan email yang lain atau segera konfirmasi Account anda")
         } else {
            { this.Alert() }
         }
      } else {
         Alert.alert("Notifikasi", "Mohon ulangi confirmasi email anda")
      }
      console.log(data);
   }


   render() {
      if (this.state.isLoading == false) {
         return (
            <SafeAreaView>
               <ScrollView style={{ height: '100%', backgroundColor: 'white' }}>
                  <View style={{}}>
                     <View style={{ justifyContent: 'center', alignItems: 'center', padding: 20, marginTop: 10 }}>
                        {/* <Image style={{ height: 110, width: 117, padding: 10 }} source={{ uri: 'https://skills.id/source/edumy/images/icon_new.png' }} />
                        <Text style={{ fontSize: 20, fontWeight: '500', marginTop: 20 }}>Login to your Account</Text> */}
                        <Text style={{ fontSize: 35, fontWeight: 'bold', color: '#16a596' }}>Register</Text>
                        <View style={{ flexDirection: 'row', marginVertical: 10 }}>
                           <Text style={{ fontSize: 17 }}>Have an account?</Text>
                           <TouchableOpacity onPress={() => this.props.navigation.navigate('LoginPage')} >
                              <Text style={{ fontSize: 17, marginLeft: 5, color: '#16a596' }}>Sign In!</Text>
                           </TouchableOpacity>
                        </View>
                     </View>
                     <View style={styles.container1}>
                        <View style={{}}>
                           <View style={{ flexDirection: 'row' }}>
                              <Text style={styles.title}>Name</Text>
                              <Text style={{ color: '#ea2c62', marginLeft: 10, fontSize: 15, marginTop: 2 }}>( * )</Text>
                           </View>
                           <TextInput
                              style={styles.inputBox}
                              placeholderTextColor="grey"
                              style={styles.inputBox}
                              placeholder="Full Name"
                              onChangeText={(text) => this.setState({ name: text })}
                           />
                           <View style={{ flexDirection: 'row' }}>
                              <Text style={styles.title}>Email</Text>
                              <Text style={{ color: '#ea2c62', marginLeft: 10, fontSize: 15, marginTop: 2 }}>( * )</Text>
                           </View>
                           <TextInput
                              placeholderTextColor="grey"
                              style={styles.inputBox}
                              placeholder="Email"
                              onChangeText={(text) => this.setState({ email: text })}
                           />
                           <View style={{ flexDirection: 'row' }}>
                              <Text style={styles.title}>No Handphone</Text>
                              <Text style={{ color: '#ea2c62', marginLeft: 10, fontSize: 15, marginTop: 2 }}>( * )</Text>
                           </View>
                           <TextInput
                              placeholderTextColor="grey"
                              style={styles.inputBox}
                              placeholder="No Handphone"
                              onChangeText={(text) => this.setState({ no_handphone: text })}
                           />
                           <View style={{ flexDirection: 'row' }}>
                              <Text style={styles.title}>Address</Text>
                              <Text style={{ color: '#ea2c62', marginLeft: 10, fontSize: 15, marginTop: 2 }}>( * )</Text>
                           </View>
                           <TextInput
                              placeholderTextColor="grey"
                              style={styles.inputBox}
                              placeholder="Address"
                              onChangeText={(text) => this.setState({ address: text })}
                           />
                           <View style={{ flexDirection: 'row' }}>
                              <Text style={styles.title}>Tanggal Lahir</Text>
                              <Text style={{ color: '#ea2c62', marginLeft: 10, fontSize: 15, marginTop: 2 }}>( * )</Text>
                           </View>
                           <View style={[styles.inputBox, { paddingTop: 4 }]}>
                              <DatePicker
                                 defaultDate={new Date()}
                                 locale={"en"}
                                 timeZoneOffsetInMinutes={undefined}
                                 modalTransparent={false}
                                 animationType={"fade"}
                                 androidMode={"default"}
                                 placeHolderText="Select date"
                                 textStyle={{ color: "green" }}
                                 placeHolderTextStyle={{ color: "grey", marginLeft: -10 }}
                                 onDateChange={this.setDate}
                                 disabled={false}
                              />
                           </View>
                           <View style={{ flexDirection: 'row' }}>
                              <Text style={styles.title}>Password</Text>
                              <Text style={{ color: '#ea2c62', marginLeft: 10, fontSize: 15, marginTop: 2 }}>( * )</Text>
                           </View>
                           <TextInput placeholderTextColor="grey"
                              style={styles.inputBox}
                              underlineColorAndroid='rgba(0,0,0,0)'
                              placeholder="Password"
                              secureTextEntry={true}
                              onChangeText={(text) => this.setState({ password: text })}
                           />
                           <Text style={styles.title}>Confirm Password</Text>
                           <TextInput style={styles.inputBox}
                              placeholderTextColor="grey"
                              underlineColorAndroid='rgba(0,0,0,0)'
                              placeholder="Password"
                              secureTextEntry={true}
                              onChangeText={(text) => this.setState({ password1: text })}
                           />
                           {/* <View style={{ flexDirection: 'row' }}>
                              <View style={{ flex: 1, }}>
                                 <Text >No NPWP</Text>
                                 <TextInput
                                    placeholderTextColor="grey"
                                    style={styles.inputBox}
                                    placeholder="No NPWP"
                                    onChangeText={(text) => this.setState({ domisili: text })}
                                 />
                                 <Text>Nama NPWP</Text>
                                 <TextInput
                                    placeholderTextColor="grey"
                                    style={styles.inputBox}
                                    placeholder="Nama NPWP"
                                    onChangeText={(text) => this.setState({ domisili: text })}
                                 />
                                 <Text>Alamat NPWP</Text>
                                 <TextInput
                                    style={styles.inputBox}
                                    placeholder="Alamat NPWP"
                                    onChangeText={(text) => this.setState({ domisili: text })}
                                 />
                              </View>
                              <View style={{ flex: 1, paddingLeft: 5 }}>
                                 <Text>Nama Bank</Text>
                                 <TextInput
                                    placeholderTextColor="grey"
                                    style={styles.inputBox}
                                    placeholder="Nama Bank"
                                    onChangeText={(text) => this.setState({ domisili: text })}
                                 />
                                 <Text>No Rekening Bank</Text>
                                 <TextInput
                                    placeholderTextColor="grey"
                                    style={styles.inputBox}
                                    placeholder="No Rekening Bank"
                                    onChangeText={(text) => this.setState({ domisili: text })}
                                 />
                                 <Text>Nama Rekening Bank</Text>
                                 <TextInput
                                    placeholderTextColor="grey"
                                    style={styles.inputBox}
                                    placeholder="Nama Rekening Bank"
                                    onChangeText={(text) => this.setState({ domisili: text })}
                                 />
                              </View>
                           </View> */}
                        </View>
                        <TouchableOpacity style={styles.button} onPress={() => this.validasi_input()}>
                           <Text style={styles.buttonText}>Register Now</Text>
                        </TouchableOpacity>
                     </View>
                     {/* <View style={{ marginBottom: 100 }} /> */}
                  </View>
               </ScrollView>
            </SafeAreaView>
         )
      } else {
         return (
            <View style={[styles.container_loading, styles.horizontal]}>
               <Text style={{ textAlign: 'center', margin: 20, fontSize: 20 }}>Loading . . </Text>
               <ActivityIndicator size="large" color="#078b6d" />
            </View>
         )
      }
   }
}

const styles = StyleSheet.create({
   title: {
      fontSize: 18,
      fontWeight: '600',
      color: '#16a596'
   },
   container_loading: {
      flex: 1,
      justifyContent: "center"
   },
   horizontal: {
      justifyContent: "center",
   },
   container: {
      justifyContent: 'center',
      alignItems: 'center'
   },
   container1: {
      width: '100%',
      padding: 10,
      marginHorizontal: 0,
      paddingHorizontal: 20,
      marginBottom: 20,
   },
   inputBox: {
      width: '100%',
      marginBottom: 25,
      borderWidth: 1.5,
      // borderStyle: 'dashed',
      borderBottomColor: '#ea2c62',
      borderTopColor: 'white',
      borderRightColor: 'white',
      borderLeftColor: 'white',
      height: 50,
      backgroundColor: 'white',
      borderRadius: 0,
      fontSize: 15,
      marginVertical: 10,
   },
   button: {
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      backgroundColor: '#16a596',
      borderRadius: 25,
      paddingVertical: 15
   },
   buttonText: {
      fontSize: 18,
      fontWeight: '500',
      color: 'white',
      textAlign: 'center',
      alignItems: 'center',
   },
   buttonText1: {
      fontSize: 16,
      textAlign: 'right',
      marginHorizontal: '10%'
   },
   signupTextCont: {
      flexGrow: 1,
      alignItems: 'flex-end',
      justifyContent: 'center',
      paddingVertical: 16,
      flexDirection: 'row'
   },
   buttonfb: {
      width: '85%',
      backgroundColor: '#3b5998',
      borderRadius: 100,
      marginVertical: 10,
      paddingVertical: 15,
   },
   buttong: {
      width: '85%',
      backgroundColor: '#e74c3c',
      borderRadius: 100,
      marginVertical: 10,
      paddingVertical: 15
   },
})
