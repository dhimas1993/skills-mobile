import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, TextInput, Alert, Image, ScrollView, Modal } from 'react-native'
import { Content } from 'native-base'
import { connect } from 'react-redux'
import { postLoginRequest } from '../../../../store/auth/action'
import { SafeAreaView } from 'react-navigation'
import { TouchableHighlight } from 'react-native-gesture-handler'

class LoginPage extends Component {
   constructor(props) {
      super(props);
      this.state = {
         id_user: this.props.user.auth.data.id_user,
         email: '',
         pass: '',
         login: true,
         loading: false,
         user: [],
         modalVisible: false
      }
   }

   setModalVisible = (visible) => {
      this.setState({ modalVisible: visible });
   }

   validasi_input = () => {
      if (this.state.email == '') {
         Alert.alert('Notifikasi', 'Kolom email masih kosong')
      } else if (this.state.pass == '') {
         Alert.alert('Notifikasi', 'Kolom password masih kosong')
      } else {
         { this.onButtonClick() }
      }
   }

   _toMore = () => {
      // console.log("id_user", this.state.id_user)
      // if (this.state.id_user !== undefined) {
      this.props.navigation.navigate('More')
      // } else {
      // this.props.navigation.navigate('More')
      // Alert.alert("Notifikasi", "GAGAl")
      // }
   }

   _toParticipant = () => {
      const { modalVisible } = this.state;
      this.props.navigation.navigate('RegisParticipant')
      this.setModalVisible(!modalVisible)
   }

   _toFacilitator = () => {
      const { modalVisible } = this.state;
      this.props.navigation.navigate('RegisFacilitator')
      this.setModalVisible(!modalVisible)
   }

   renderModal = () => {
      const { modalVisible } = this.state;
      return (
         <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
               Alert.alert("Modal has been closed.");
            }}
         >
            <TouchableOpacity
               onPress={() => {
                  this.setModalVisible(!modalVisible);
               }}
               style={styles.centeredView}>
               <View style={styles.modalView}>
                  <Text style={styles.modalText}>Register</Text>
                  <Text style={{ fontSize: 20, fontWeight: '400' }}>As</Text>

                  <TouchableOpacity style={styles.openButton}
                     onPress={() => this._toParticipant()}
                     style={styles.button}>
                     <Text style={styles.buttonText}>Participant</Text>
                  </TouchableOpacity>
                  <View style={{}}>
                     <Text style={{ textAlign: "center", fontSize: 18 }}>------- OR --------</Text>
                  </View>
                  <TouchableOpacity
                     onPress={() => this._toFacilitator()}
                     style={styles.buttong}>
                     <Text style={styles.buttonText}>Facilitator</Text>
                  </TouchableOpacity>

                  {/* <TouchableOpacity
                     style={{ position: 'absolute', right: 0, padding: 5, backgroundColor: '#52057b', borderRadius: 50, margin: 10 }}
                     onPress={() => {
                        this.setModalVisible(!modalVisible);
                     }}
                  >
                     <Text style={styles.textStyle}>X </Text>
                  </TouchableOpacity> */}
               </View>
            </TouchableOpacity>
         </Modal>
      )
   }

   renderList = () => {
      return (
         <View style={{ marginBottom: 100 }}>
            {
               this.renderModal()
            }
            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
               <Image style={{ height: 110, width: 117, padding: 10 }} source={{ uri: 'https://skills.id/source/edumy/images/icon_new.png' }} />
               <Text style={{ fontSize: 20, fontWeight: '500', marginTop: 20 }}>Login to your Account</Text>
               <View style={{ flexDirection: 'row', marginVertical: 10 }}>
                  {/* <Text style={{ fontSize: 17 }}>Don't Have an account?</Text>
                     <TouchableOpacity>
                        <Text style={{ fontSize: 17, marginLeft: 5, color: 'blue' }}>Sign Up!</Text>
                     </TouchableOpacity> */}
               </View>
            </View>
            <View style={styles.container}>
               <TextInput
                  value={this.state.email}
                  style={styles.inputBox}
                  placeholderTextColor="grey"
                  placeholder="Ex : skills@gmail.com"
                  onChangeText={(text) => this.setState({ email: text })}
               />
               <TextInput
                  value={this.state.pass}
                  onChangeText={(text) => this.setState({ pass: text })}
                  style={styles.inputBox}
                  placeholderTextColor="grey"
                  placeholder="Password"
                  secureTextEntry={true}
               />
               <TouchableOpacity onPress={() => this.validasi_input()} style={styles.button}>
                  <Text style={styles.buttonText}>Login</Text>
               </TouchableOpacity>
            </View>
            {/* <TouchableOpacity>
                  <Text style={styles.buttonText1}> Forgot Password</Text>
               </TouchableOpacity> */}
            <View style={{ height: 180, marginVertical: 10 }}>
               <View style={{}}>
                  <Text style={{ textAlign: "center", fontSize: 18 }}>------- OR --------</Text>
               </View>
               <View style={styles.container}>
                  <TouchableOpacity style={styles.openButton}
                     onPress={() => { this.setModalVisible(true) }}
                     style={styles.buttong}>
                     <Text style={styles.buttonText}>Register</Text>
                  </TouchableOpacity>
               </View>
            </View>

            <View style={styles.signupTextCont}>
               <Text style={styles.signupText}>Copyright Skills.ID © 2020. All Rights Reserved.</Text>
            </View>
         </View>
      )
   }

   onButtonClick = async () => {
      var username = this.state.email
      var password = this.state.pass

      // await this.props.onLoginUser(data)

      try {
         const data = {
            user: username,
            pass: password
         }
         // Login di app
         const api = await this.props.onLoginUser(
            data
         )

         await console.log('res', api)
         // Pindah ke halaman utama
         this._toMore()
      } catch (err) {
         // jika terjadi error pada block kode 'try', akan kita munculkan pesan errornya
         alert("Notfikasi app", err)
      }
   }

   render() {
      // console.log('loading', this.state.id_user);
      return (
         <SafeAreaView>
            <ScrollView style={{ height: '100%', backgroundColor: 'white' }} >
               {
                  this.renderList()
               }
            </ScrollView>
         </SafeAreaView>
      )
   }
}

const mapDispatchToProps = dispatch => {
   // console.log('ini data ke action', data);
   return {
      onLoginUser: (data) => { dispatch(postLoginRequest(data)) }
   }
}

const mapStateToProps = (state) => {
   // console.log('cacad', state.auth.auth.data.id_user)
   if (state.auth.auth.isLoggedIn == true && state.auth.auth.data !== []) {
      // Alert.alert('notifikasi', "= ada")
   } else if (state.auth.auth.isLoggedIn == false && state.auth.auth.log !== '') {
      Alert.alert('notifikasi', "tidak ada")
   }
   return {
      user: state.auth
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)

const styles = StyleSheet.create({
   centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 22
   },
   modalView: {
      width: '80%',
      margin: 10,
      backgroundColor: "white",
      borderRadius: 20,
      padding: 20,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
         width: 0,
         height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5
   },
   textStyle: {
      fontSize: 10,
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
   },
   modalText: {
      letterSpacing: 5,
      fontSize: 30, fontWeight: 'bold',
      marginBottom: 15,
      textAlign: "center",
      color: '#ea2c62'
   },
   container: {
      justifyContent: 'center',
      alignItems: 'center'
   },
   inputBox: {
      width: '80%',
      // borderWidth: 1.5,
      // borderColor: '#ea2c62',
      // borderTopColor: 'white',
      // borderRightColor: 'white',
      // borderLeftColor: 'white',
      height: 50,
      backgroundColor: '#f4f4f2',
      borderRadius: 50,
      paddingHorizontal: 20,
      fontSize: 15,
      marginVertical: 10,
   },
   button: {
      marginTop: 30,
      width: '80%',
      height: 50,
      backgroundColor: '#16a596',
      borderRadius: 25,
      paddingHorizontal: 20,
      fontSize: 15,
      marginVertical: 10,
      justifyContent: 'center'
   },
   buttonText: {
      fontSize: 15,
      color: '#ffffff',
      textAlign: 'center',
      paddingHorizontal: 0,
   },
   buttonText1: {
      fontSize: 16,
      textAlign: 'right',
      marginHorizontal: '10%',
      fontStyle: 'italic'
   },
   signupTextCont: {
      alignItems: 'center',
      justifyContent: 'center',
   },
   buttonfb: {
      marginTop: 30,
      width: '80%',
      height: 50,
      backgroundColor: '#078b6d',
      borderRadius: 25,
      paddingHorizontal: 20,
      fontSize: 15,
      marginVertical: 10,
      justifyContent: 'center'
   },
   buttong: {
      marginTop: 15,
      width: '80%',
      height: 50,
      backgroundColor: '#ea2c62',
      borderRadius: 25,
      paddingHorizontal: 20,
      fontSize: 15,
      marginVertical: 10,
      justifyContent: 'center'
   },
})
