import React, { Component } from 'react'
import { Text, StyleSheet, View, ImageBackground, TouchableOpacity, Image } from 'react-native'
import { Container, Content } from 'native-base'
import VideoDetail from './VideoDetail'

const DATA = [
   {
      id: '1',
      title: 'ongol ongol',
   },
   {
      id: '2',
      title: 'Second Item',
   },
   {
      id: '3',
      title: 'Third Item',
   },
   {
      id: '4',
      title: 'empat',
   },
   {
      id: '5',
      title: 'lima Item',
   },
   {
      id: '6',
      title: 'enam Item',
   },
   {
      id: '7',
      title: 'tujuh Item',
   },
   {
      id: '8',
      title: 'delapan Item',
   },
   {
      id: '9',
      title: 'sembilan Item',
   },
   {
      id: '10',
      title: '10 Item',
   },

];

const asu = '../../../assets/picture/1.jpg';

export default class videoPage extends Component {
   render() {
      return (
         <Container>
            <View>
               <View style={{ height: 95, width: '100%', flexDirection: 'row' }}>
                  <View style={{ height: '100%', width: '20%' }}>
                     <Image style={{ borderRadius: 50, width: '80%', height: '65%', marginLeft: 15, marginTop: 10 }} source={require(asu)} />
                  </View>
                  <View style={{ height: '100%', width: '100%', flex: 1 }}>
                     <View style={{ height: '50%', flex: 1, flexDirection: 'row', justifyContent: 'space-between', padding: 10 }}>
                        <View style={{}}>
                           <Text style={{}}>Marlyn Monroe</Text>
                           <Text style={{ fontSize: 10, opacity: 0.6 }}>Profesional Design Graphic</Text>
                        </View>
                        <View>
                           <Text>Video</Text>
                        </View>
                     </View>
                     <View style={{ height: '50%', flex: 1, padding: 10 }}>
                        <Text style={{ marginTop: -10, fontSize: 12 }}>Learn The Basic Of Video Editing With Adobe Premiere Pro</Text>
                     </View>
                  </View>
               </View>
               <View style={{ width: '100%', height: 220, marginTop: -10 }}>

                  <VideoDetail />
                  {/* <ImageBackground source={require('../../picture/1.jpg')} style={{ width: '100%',height: '100%',justifyContent: 'center',alignItems: 'center',marginBottom: 20 }} imageStyle={{ borderRadius: 7 }}>
                     <View style={{ width: 100,height: 70,backgroundColor: 'black',justifyContent: 'center',alignItems: 'center',opacity: 0.6 }}>
                        <Text style={{ color: 'white',fontSize: 32 }}>LOGO</Text>
                     </View>
                  </ImageBackground> */}
               </View>
            </View>
            <Content style={{ marginTop: 20 }}>
               {
                  DATA.map((asu) => {
                     return (
                        <TouchableOpacity>
                           <View style={styles.item} >
                              <Text>Logo : </Text>
                              <Text style={styles.title}>{asu.title}</Text>
                           </View>
                        </TouchableOpacity>
                     )
                  })
               }
            </Content>
         </Container>
      )
   }
}

const styles = StyleSheet.create({

   item: {
      backgroundColor: 'lightgrey',
      padding: 20,
      marginVertical: 5,
      marginHorizontal: 10,
      borderRadius: 7,
      flexDirection: 'row'
   },
   title: {
      fontSize: 15,
   },
});
