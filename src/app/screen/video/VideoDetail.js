// import React,{ Component } from 'react';
// import { View,StyleSheet,Dimensions,TouchableNativeFeedback,Icon } from "react-native";
// import Video from 'react-native-video';

// const { width } = Dimensions.get('window')
// const sampleVideo = require('../../../assets/video/satu.mp4')

// export default class VideoDetail extends Component {
//    constructor(p) {
//       super(p);
//       this.state = {
//          currentTime: 0,
//          duration: 0.1,
//          paused: false,
//          overlay: false,
//       }
//    }

//    render() {
//       const { currentTime,duration,paused,overlay } = this.state
//       return (
//          <View style={styles.container}>
//             <View style={{ width,height: width * .6,backgroundColor: 'black' }}>
//                <Video style={styles.backgroundVideo}
//                   id="video"
//                   fullscreenOrientation="all"
//                   fullscreenAutorotate={true}
//                   onBuffer={this.onBuffer}
//                   onError={this.videoError}
//                   source={sampleVideo}
//                   resizeMode='contain'
//                   rate={1}
//                   volume={1}
//                   muted={false}
//                   ignoreSilentSwitch={null}
//                   fullscreen={true}
//                   repeat={true}
//                   controls={true}
//                />
//                <View style={styles.overlay}>
//                   {
//                      !overlay ?
//                         <View style={{ backgroundColor: '#0006' }}>
//                         </View> :
//                         <View style={styles.overlaySet}>
//                            <TouchableNativeFeedback onPress={this.youtubeSeekLeft}> <View style={{ flex: 1 }} /> </TouchableNativeFeedback>
//                            <TouchableNativeFeedback onPress={this.youtubeSeekRight}> <View style={{ flex: 1 }} /> </TouchableNativeFeedback>
//                         </View>
//                   }
//                </View>
//             </View>
//          </View>
//       );
//    }
// }

// const styles = StyleSheet.create({
//    backgroundVideo: {
//       flex: 1,
//       position: 'absolute',
//       top: 0,
//       left: 0,
//       bottom: 0,
//       right: 0,
//    },
//    overlay: {
//       ...StyleSheet.absoluteFillObject
//    },
//    overlaySet: {
//       flex: 1,
//       flexDirection: 'row'
//    },
//    icon: {
//       color: 'white',
//       flex: 1,
//       textAlign: 'center',
//       textAlignVertical: 'center',
//       fontSize: 25
//    }
// })
