import React, { Component } from 'react'
import { Text, StyleSheet, View, SafeAreaView, TextInput, TouchableHighlight, TouchableOpacity, ScrollView, Image, TouchableWithoutFeedback } from 'react-native'
import { Icon } from 'native-base'

export default class myVideo extends Component {
   state = {
      value: '',
      data: [
         {
            id: 1,
            title: 'Body works with love',
            pict: 'https://images.unsplash.com/photo-1516481265257-97e5f4bc50d5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2250&q=80',
            author: 'Deddy Courbuzier',
            qty: 6,
            status: 'download'
         },
         {
            id: 1,
            title: 'Body works with professional',
            pict: 'https://images.unsplash.com/photo-1516481157630-05bc0aeb8b19?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2250&q=80',
            author: 'Deddy Courbuzier',
            qty: 6,
            status: 'download'
         },
         {
            id: 1,
            title: 'Body works with love',
            pict: 'https://images.unsplash.com/photo-1516481265257-97e5f4bc50d5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2250&q=80',
            author: 'Deddy Courbuzier',
            qty: 6,
            status: 'download'
         },
      ]
   }

   render() {
      const value = this.state
      return (
         <SafeAreaView>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 15 }}>
               <TextInput style={{ height: 50, backgroundColor: 'white', elevation: 10, width: '80%', borderRadius: 7, padding: 10 }}
                  value={value} />
               <TouchableOpacity>
                  <Icon name="search" style={{ padding: 10 }} />
               </TouchableOpacity>
            </View>

            <View style={{ flexDirection: 'row', padding: 15, justifyContent: 'space-around' }}>
               <TouchableOpacity style={{}}>
                  <Text style={{ fontSize: 15, }}>Your Videos</Text>
               </TouchableOpacity>
               <TouchableOpacity style={{}}>
                  <Text style={{ fontSize: 15, }}>Filter</Text>
               </TouchableOpacity>
            </View>

            <ScrollView>
               <View style={{ padding: 15 }}>
                  {
                     this.state.data.map((item) => {
                        return (
                           <TouchableWithoutFeedback >
                              <View style={{ backgroundColor: 'white', width: '100%', flexDirection: 'row', marginBottom: 15, height: 100, elevation: 3, borderRadius: 5 }}>
                                 <Image source={{ uri: item.pict }}
                                    style={{ width: '45%', padding: 10, borderTopLeftRadius: 5, borderBottomLeftRadius: 5 }} />
                                 <View style={{ flex: 1, paddingHorizontal: 10 }}>
                                    <Text style={{ fontSize: 15, }}>{item.title}</Text>
                                    <Text style={{ fontSize: 12 }}>{item.qty} Videos</Text>
                                    <Text style={{ fontSize: 12 }}>{item.author}</Text>
                                 </View>
                              </View>
                           </TouchableWithoutFeedback>
                        )
                     })
                  }
               </View>
            </ScrollView>
         </SafeAreaView>
      )
   }
}

const styles = StyleSheet.create({})
