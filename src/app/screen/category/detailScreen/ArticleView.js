import React, { Component } from 'react'
import { Text, StyleSheet, View, SafeAreaView, Image, TouchableOpacity, ActivityIndicator } from 'react-native'
import { Container, Content, Tab, Tabs, TabHeading, Card, Icon, Picker } from 'native-base';

import { workshopByName, articleByName } from '../../../../api/home_api'
import { TextInput } from 'react-native-gesture-handler';

export default class ArticleView extends Component {
   constructor(props) {
      super(props);
      this.state = {
         cat_id: this.props.navigation.state.params.detail_cat.category,
         blog: [],
         isLoading: false,
         total_data_blog: '',
      };
      // nama kategory yang dibawa dari page sebelumnya
      // console.log(this.state.cat_id);
   }

   async componentDidMount() {
      const api1 = await articleByName(this.state.cat_id)
      this.setState({
         blog: api1.data,
         total_data_blog: api1.data.length,
         isLoading: true
      })
      // console.log(this.state.blog);
   }

   _toArticlePage = (param) => {
      this.props.navigation.navigate('ArticlePage', { detail_article: param })
   }

   isLoading = () => {
      return (
         <View style={{ justifyContent: 'center', alignItems: 'center', height: '100%', marginTop: 100 }}>
            <ActivityIndicator size="large" color="#0000ff" />
         </View>
      )
   }

   renderList = () => {
      if (this.state.blog == '') {
         return (
            <View style={{ justifyContent: 'center', alignItems: 'center', height: '100%', marginTop: 135 }}>
               <Text>Data Kosong</Text>
            </View>
         )
      } else {
         return (
            this.state.blog.map((item) => {
               return (
                  <Card style={styles.card}>
                     <View style={styles.content}>
                        <Image style={styles.picture} source={{ uri: 'https://skills.id/admin/source/images/blog/' + item.banner }} />
                        <View style={styles.text}>
                           <View style={{ flex: 1, justifyContent: 'space-between', marginHorizontal: 10 }}>
                              <View style={{ flex: 1, justifyContent: 'center' }}>
                                 <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ color: 'orange' }}>{item.category}</Text>
                                    <Text style={{ fontStyle: 'italic', fontSize: 13 }}>{item.posted_date}</Text>
                                 </View>
                                 <Text style={{ fontSize: 15, fontWeight: 'bold', paddingBottom: 10, paddingTop: 3, flexWrap: 'wrap' }} numberOfLines={2}>{item.title}</Text>

                                 <Text style={{ paddingBottom: 5, fontStyle: 'italic', color: 'grey' }}>{item.posted_by}</Text>
                              </View>
                              <View>
                                 <TouchableOpacity
                                    onPress={() => this._toArticlePage(item)}
                                    style={{ backgroundColor: "red", borderRadius: 50, justifyContent: 'center', alignItems: 'center', height: 30 }}>
                                    <Text style={{ color: 'white', justifyContent: 'center', alignItems: 'center', fontSize: 14 }}>Detail</Text>
                                 </TouchableOpacity>
                              </View>
                           </View>
                        </View>
                     </View>
                  </Card>
               )
            })
         )
      }
   }

   render() {
      return (
         <Content>
            <View style={styles.container}>
               {
                  this.state.isLoading === false ?
                     this.isLoading() : this.renderList()

               }
            </View>
         </Content>
      )
   }
}

const styles = StyleSheet.create({
   container: {
      padding: 10

   },
   card: {
      marginHorizontal: 10,
      marginBottom: 10,
      padding: 10,
      paddingHorizontal: 10,
      borderRadius: 10,
   },
   content: {
      flex: 1,
      flexDirection: 'row',
      borderColor: 'lightgrey',

   },
   picture: {
      flex: 2,
      borderRadius: 10
   },
   text: {
      flex: 3,
   }
})
