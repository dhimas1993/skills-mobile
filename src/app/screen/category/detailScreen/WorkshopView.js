import React, { Component } from 'react'
import { Text, StyleSheet, View, SafeAreaView, Image, TouchableOpacity, ActivityIndicator } from 'react-native'
import { Content, Card, Icon, Picker } from 'native-base';

import Icons from 'react-native-vector-icons/FontAwesome5';
import { workshopByName, workshopCity } from '../../../../api/home_api'

export default class WorkshopView extends Component {
   constructor(props) {
      console.log('props', props.navigation.state.params)
      super(props);
      this.state = {
         cat_id: this.props.navigation.state.params.detail_cat.category,
         workshop: '',
         kota: '',
         api_kota: '',
         isLoading: false
      };
   }

   componentDidMount() {
      this.getWorkshop()
      this.getKota()
   }


   bankValueChange(value) {
      this.setState({
         kota: value
      });
   }

   getKota = async () => {
      const api = await workshopCity(this.state.cat_id)
      this.setState({
         api_kota: api.data,
      })
      // console.log(api.data)
   }

   getWorkshop = async () => {
      this.setState({
         isLoading: true
      })

      const api = await workshopByName(this.state.cat_id)
      this.setState({
         workshop: api.data,
         isLoading: false
      })
      // console.log("api", api.data);
   }

   _toWorkshopPage = (param) => {
      this.props.navigation.navigate('WorkshopPage', param)
   }

   isLoading = () => {
      return (
         <View style={{ justifyContent: 'center', alignItems: 'center', height: '100%', marginTop: 100 }}>
            <ActivityIndicator size="large" color="#0000ff" />
         </View>
      )
   }

   renderTanggal = (item) => {
      const start1 = 1
      const multi = item.split(", ")

      const year = new Date().getFullYear()
      const month = (new Date().getMonth() + 1 < 10 ? '0' : '') + (new Date().getMonth() + 1)
      const day = (new Date().getDate() + 1 < 10 ? '0' : '') + (new Date().getDate())
      const now = Number(year + month + day)

      // console.log(now)
      if (multi) {
         return (
            multi.length
         )
      } else {
         return (
            start1
         )
      }
   }

   renderLIst = () => {
      // console.log('wk', this.state.workshop)
      if (this.state.workshop == '') {
         return (
            <View style={{ justifyContent: 'center', alignItems: 'center', height: '100%', marginTop: 100 }}>
               <Text>Data Kosong</Text>
            </View>
         )
      } else {
         return (
            this.state.workshop.map((item) => {
               if (item.kota === this.state.kota) {
                  return (
                     <Card style={styles.card}>
                        <View style={styles.cardContainer1}>
                           <View style={styles.cardContainerTop}>
                              <View style={styles.containerProfile1}>
                                 <Image style={styles.avatar} source={{ uri: 'https://skills.id/admin/source/images/speaker/' + item.photo }} />
                                 <View style={{ flex: 1 }}>
                                    <Text style={styles.facilitatorName} numberOfLines={1}>{item.name}</Text>
                                    <Text style={styles.position}>{item.level}</Text>
                                 </View>
                              </View>
                              <View style={{ flex: 1 }}>
                                 <Text style={styles.titleVideo1} numberOfLines={2}>{item.title}</Text>
                              </View>
                           </View>
                           <View style={styles.cardContainer2}>
                              <View style={styles.cardBottom}>
                                 <View><Image style={styles.gambar1} source={{ uri: 'https://skills.id/admin/source/images/thumbnail/' + item.image }} /></View>
                              </View>
                              <View style={styles.cardBottomRight}>
                                 <View style={{ flex: 1, marginBottom: 10 }}>
                                    <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 10, paddingBottom: 10 }}>
                                       <Icons name="map-marker-alt" style={{ fontSize: 15, marginRight: 10 }} />
                                       <Text>{item.kota}</Text>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 10 }}>
                                       <Icons name="calendar-alt" style={{ fontSize: 15, marginRight: 8 }} />
                                       <Text>{this.renderTanggal(item.multi_tanggal)} Schedule
                                       </Text>
                                    </View>
                                 </View>
                                 <View>
                                    <TouchableOpacity
                                       onPress={() => this._toWorkshopPage(item)}
                                       style={{ backgroundColor: "red", borderRadius: 50, justifyContent: 'center', alignItems: 'center' }}>
                                       <Text style={{ color: 'white', justifyContent: 'center', alignItems: 'center', fontSize: 13, paddingVertical: 5, fontWeight: '500' }}>Detail</Text>
                                    </TouchableOpacity>
                                 </View>
                              </View>
                           </View>
                        </View>
                     </Card>
                  )
               } else if (this.state.kota == '') {
                  return (
                     <Card style={styles.card}>
                        <View style={styles.cardContainer1}>
                           <View style={styles.cardContainerTop}>
                              <View style={styles.containerProfile1}>
                                 <Image style={styles.avatar} source={{ uri: 'https://skills.id/admin/source/images/speaker/' + item.photo }} />
                                 <View style={{ flex: 1 }}>
                                    <Text style={styles.facilitatorName} numberOfLines={1}>{item.name}</Text>
                                    <Text style={styles.position}>{item.level}</Text>
                                 </View>
                              </View>
                              <View style={{ flex: 1 }}>
                                 <Text style={styles.titleVideo1} numberOfLines={2}>{item.title}</Text>
                              </View>
                           </View>
                           <View style={styles.cardContainer2}>
                              <View style={styles.cardBottom}>
                                 <View><Image style={styles.gambar1} source={{ uri: 'https://skills.id/admin/source/images/thumbnail/' + item.image }} /></View>
                              </View>
                              <View style={styles.cardBottomRight}>
                                 <View style={{ flex: 1, marginBottom: 10 }}>
                                    <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 10, paddingBottom: 10 }}>
                                       <Icons name="map-marker-alt" style={{ fontSize: 15, marginRight: 10 }} />
                                       <Text>{item.kota}</Text>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 10 }}>
                                       <Icons name="calendar-alt" style={{ fontSize: 15, marginRight: 8 }} />
                                       <Text>{this.renderTanggal(item.multi_tanggal)} Schedule
                                       </Text>
                                    </View>
                                 </View>
                                 <View>
                                    <TouchableOpacity
                                       onPress={() => this._toWorkshopPage(item)}
                                       style={{ backgroundColor: "red", borderRadius: 50, justifyContent: 'center', alignItems: 'center' }}>
                                       <Text style={{ color: 'white', justifyContent: 'center', alignItems: 'center', fontSize: 13, paddingVertical: 5, fontWeight: '500' }}>Detail</Text>
                                    </TouchableOpacity>
                                 </View>
                              </View>
                           </View>
                        </View>
                     </Card>
                  )
               }
            })
         )
      }
   }

   render() {
      return (
         <Content>
            <View style={styles.navigasi}>
               <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', flex: 1 }}>
                  <Picker
                     mode="dropdown"
                     iosIcon={<Icon name="arrow-down" />}
                     placeholder="Pilih kota"
                     placeholderStyle={{ color: "#bfc6ea" }}
                     placeholderIconColor="#007aff"
                     style={{ flex: 1 }}
                     selectedValue={this.state.kota}
                     onValueChange={this.bankValueChange.bind(this)}
                  >
                     {/* <Picker.Item label='kota' value='kota' /> */}
                     {
                        this.state.api_kota ?
                           this.state.api_kota.map(item => {
                              return (
                                 <Picker.Item label={item.kota} value={item.kota} />
                              )
                           }) : null
                     }
                  </Picker>
               </View>
            </View>
            <View style={{ padding: 10 }}>
               {
                  this.state.isLoading == false ? this.renderLIst() : this.isLoading()
               }
            </View>
         </Content>
      )
   }
}

const styles = StyleSheet.create({
   card: {
      borderRadius: 10, padding: 5
   },
   cardContainer1: {
      width: '100%', padding: 5
   },
   cardContainer2: {
      flex: 1, flexDirection: 'row'
   },
   cardContainerTop: {
      flexDirection: 'row', height: 55, justifyContent: "center"
   },
   containerProfile1: {
      flex: 1, flexDirection: 'row'
   },
   avatar: {
      height: 50, width: 50, borderRadius: 50
   },
   facilitatorName: {
      paddingLeft: 10, paddingTop: 2, fontSize: 15, fontWeight: 'bold'
   },
   position: {
      paddingLeft: 10, paddingTop: 2, fontSize: 13, flexWrap: "wrap", fontStyle: 'italic'
   },
   titleVideo1: {
      paddingLeft: 10, paddingTop: 2, fontSize: 15, fontWeight: 'bold'
   },
   cardBottom: {
      width: '50%'
   },
   cardBottomRight: {
      flex: 1, justifyContent: 'space-between'
   },
   gambar1: {
      height: '100%',
      width: '95%',
      borderRadius: 6,
   },
   gambar2: {
      height: 55,
      width: '95%',
      borderRadius: 5
   },
   dot: {
      width: 10,
      height: 10,
      backgroundColor: 'grey',
      borderRadius: 100,
      marginRight: 10,
      marginTop: 10
   },
   centeredView: {
      flex: 1,
      justifyContent: 'flex-end',
      backgroundColor: 'black',
      opacity: 0.9
   },
   modalView: {
      margin: 20,
      backgroundColor: "white",
      borderRadius: 20,
      padding: 10,
      alignItems: "center",
      shadowColor: "black",
      shadowOffset: {
         width: 0,
         height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5
   },
   openButton: {
      backgroundColor: "#F194FF",
      borderRadius: 20,
      padding: 10,
      elevation: 2,
      marginBottom: 20

   },
   textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
   },
   modalText: {
      marginBottom: 15,
      textAlign: "center"
   }
});
