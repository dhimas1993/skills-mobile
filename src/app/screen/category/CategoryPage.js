import React, { Component } from 'react'
import { Text, StyleSheet, View, SafeAreaView, TouchableOpacity, ScrollView } from 'react-native'

import { categoryHome } from '../../../api/home_api'
import Carousel from '../../components/Carousel'
import { Container, Content } from 'native-base'

import HeaderApp from '../../components/HeaderApp'

import Icons from 'react-native-vector-icons/FontAwesome5';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class CategoryPage extends Component {
   constructor(props) {
      super(props);
      this.state = {
         category: []
      };
   }

   async componentDidMount() {
      const api = await categoryHome()
      this.setState({
         category: api.data,
      })
      console.log("get", api.data);
   }

   _toCategoryDetailPage = (param) => {
      this.props.navigation.navigate('DetailCategoryPage', { detail_cat: param })
   }

   render() {
      return (
         <Container>
            <Content>
               <HeaderApp
                  props={this.props.navigation}
                  tittle="Category" />
               <Carousel />
               <SafeAreaView>
                  <View style={{ marginBottom: 20 }}>
                     <Text style={{ fontSize: 25, marginLeft: 10 }}>Category</Text>
                  </View>
                  <ScrollView>
                     <View style={{ width: '100%', flexWrap: 'wrap', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly' }}>
                        {
                           this.state.category.map((item) => {
                              return (
                                 <View style={{ width: 120, height: 140, justifyContent: 'center', alignItems: 'center' }}>
                                    <TouchableOpacity onPress={() => this._toCategoryDetailPage(item)}>
                                       <View style={{ flex: 1 }}>
                                          <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', padding: 20 }}>
                                             {
                                                item.category == 'Music' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='music' /> :
                                                   item.category == 'Art and Craft' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='paint-brush' /> :
                                                      item.category == 'Food and Beverages' ? <Icon style={{ fontSize: 25, color: '#52057b' }} name='cutlery' /> :
                                                         item.category == 'Design' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='pencil-ruler' /> :
                                                            item.category == 'Photography' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='camera' /> :
                                                               item.category == 'Sport' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='basketball-ball' /> :
                                                                  item.category == 'Business' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='briefcase' /> :
                                                                     item.category == 'Fashion' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='tshirt' /> :
                                                                        item.category == 'Lifestyle' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='smile' /> :
                                                                           item.category == 'Technology' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='laptop' /> :
                                                                              item.category == 'Specific' ? <Icons style={{ fontSize: 25, color: '#52057b' }} name='ellipsis-h' /> :
                                                                                 <Text>Gagal Loading</Text>
                                             }
                                             <Text style={{ fontSize: 13, marginTop: 10, textAlign: 'center' }}>{item.category}</Text>
                                          </View>
                                       </View>
                                    </TouchableOpacity>
                                 </View>
                              )
                           })
                        }
                     </View>
                  </ScrollView>
               </SafeAreaView>
            </Content>
         </Container>


      )
   }
}

const styles = StyleSheet.create({})
