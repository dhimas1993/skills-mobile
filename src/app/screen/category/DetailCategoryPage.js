import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, ImageBackground, SafeAreaView } from 'react-native'
import { Container, Tab, Tabs, TabHeading, Icon, Picker } from 'native-base';

import { workshopByName, articleByName } from '../../../api/home_api'
import HeaderApp from '../../components/HeaderApp'
import WorkshopView from './detailScreen/WorkshopView';
import ArticleView from './detailScreen/ArticleView';
import workshop from '../submit/workshop';

const asu1 = 'https://images.unsplash.com/photo-1453090927415-5f45085b65c0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60'

export default class detailCategory extends Component {
   constructor(props) {
      super(props);

      this.state = {
         cat_id: this.props.navigation.state.params.detail_cat.category,
         total_data_blog: null,
         total_data_workshop: null,
         workshop: ''
      };
      // nama kategory yang dibawa dari page sebelumnya
      // console.log(this.state.cat_id);
   }

   async componentDidMount() {
      const api1 = await articleByName(this.state.cat_id)
      const api = await workshopByName(this.state.cat_id)
      this.setState({
         total_data_blog: api1.data.length,
         total_data_workshop: api.data.length,
         workshop: api.data
      })

      console.log('cdm', this.state.workshop);
   }

   refreshScreen = () => {
      this.setState({ lastRefresh: Date(Date.now()).toString() })
   }

   render() {
      // console.log('render', this.props)
      return (
         <Container>
            <SafeAreaView style={{ flex: 1, height: '100%' }}>
               <HeaderApp
                  props={this.props.navigation}
                  tittle="Category" />
               <View style={styles.carousel}>
                  <ImageBackground source={{ uri: asu1 }} style={{ width: '100%', height: '100%' }} imageStyle={{}}>
                     <Text style={{ textAlign: 'center', color: "white", fontSize: 30, marginTop: 40 }}>
                        {this.state.cat_id}
                     </Text>
                  </ImageBackground>
               </View>

               <Tabs tabBarUnderlineStyle={{ backgroundColor: '#078b6d' }}>
                  <Tab heading={
                     <TabHeading style={{ backgroundColor: 'white' }}>
                        <Text style={{ color: 'black' }}> {this.state.total_data_blog} Article { }</Text>
                     </TabHeading>}>
                     <ArticleView {...this.props} />
                  </Tab>
                  <Tab heading={
                     <TabHeading style={{ backgroundColor: 'white' }}>
                        <Text style={{ color: 'black' }}> {this.state.total_data_workshop} Workshop</Text>
                     </TabHeading>}>
                     <WorkshopView {...this.props} />
                  </Tab>
               </Tabs>
            </SafeAreaView>
         </Container>
      )
   }
}

const styles = StyleSheet.create({
   carousel: {
      backgroundColor: 'pink',
      height: 120,
      justifyContent: 'center'
   },
   navigasi: {
      height: 60,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between'
   },
   gambar1: {
      height: 130,
      width: 183,
      borderRadius: 6,
   },
   gambar2: {
      height: 55,
      width: 80,
      borderRadius: 6,
      justifyContent: "center",
      alignItems: 'center'
   },
   gambar1: {
      height: '100%',
      width: '95%',
      borderRadius: 6,
   },
   gambar2: {
      height: 55,
      width: '95%',
      borderRadius: 6
   },
   container: {
      padding: 10
   },
   title: {
      marginBottom: 5, fontSize: 18
   },
   card: {
      borderRadius: 7, padding: 5
   },
   cardContainer1: {
      width: '100%', padding: 5
   },
   cardContainer2: {
      flex: 1, flexDirection: 'row'
   },
   cardContainerTop: {
      flexDirection: 'row', height: 55, justifyContent: "center"
   },
   containerProfile1: {
      flex: 1, flexDirection: 'row'
   },
   avatar: {
      height: 50, width: 50, borderRadius: 50
   },
   facilitatorName: {
      paddingLeft: 10, paddingTop: 2, fontSize: 13
   },
   position: {
      paddingLeft: 10, paddingTop: 2, fontSize: 10, flexWrap: "wrap"
   },
   titleVideo1: {
      paddingLeft: 10, paddingTop: 2, fontSize: 12
   },
   cardBottom: {
      width: '50%'
   },
   cardBottomRight: {
      flex: 1, justifyContent: 'space-between'
   },
   gambar1: {
      height: '100%',
      width: '95%',
      borderRadius: 6,
   },
   gambar2: {
      height: 55,
      width: '95%',
      borderRadius: 5
   },
   dot: {
      width: 10,
      height: 10,
      backgroundColor: 'grey',
      borderRadius: 100,
      marginRight: 10,
      marginTop: 10
   },
   centeredView: {
      flex: 1,
      justifyContent: 'flex-end',
      backgroundColor: 'black',
      opacity: 0.9
   },
   modalView: {
      margin: 20,
      backgroundColor: "white",
      borderRadius: 20,
      padding: 10,
      alignItems: "center",
      shadowColor: "black",
      shadowOffset: {
         width: 0,
         height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5
   },
   openButton: {
      backgroundColor: "#F194FF",
      borderRadius: 20,
      padding: 10,
      elevation: 2,
      marginBottom: 20

   },
   textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
   },
   modalText: {
      marginBottom: 15,
      textAlign: "center"
   }
});
