import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, TextInput, ScrollView, SafeAreaView, Alert, Image } from 'react-native'
import { Form, Picker, Icon, DatePicker } from 'native-base'
import HeaderApp from '../../components/HeaderApp';
import ImagePicker from 'react-native-image-picker';

import { editProfile, changePhoto, changeCover } from '../../../api/profile_api'

export default class EditAccount extends Component {
   constructor(props) {
      // tarik data dari page profile
      // console.log('editnew', props.navigation.state.params.detail_profile);

      super(props);
      this.state = {
         isLoading: false,
         chosenDate: new Date(),
         selected: undefined,
         user: this.props.navigation.state.params.detail_profile,
         // state personal
         name: this.props.navigation.state.params.detail_profile.name,
         alamat_personal: this.props.navigation.state.params.detail_profile.alamat_personal,
         phone: this.props.navigation.state.params.detail_profile.phone,
         date_of_birth: this.props.navigation.state.params.detail_profile.date_of_birth,
         // state banking
         no_npwp: this.props.navigation.state.params.detail_profile.no_npwp,
         nama_npwp: this.props.navigation.state.params.detail_profile.nama_npwp,
         alamat_npwp: this.props.navigation.state.params.detail_profile.alamat_npwp,
         nama_bank: this.props.navigation.state.params.detail_profile.nama_bank,
         nomer_rekening: this.props.navigation.state.params.detail_profile.nomer_rekening,
         nama_rekening: this.props.navigation.state.params.detail_profile.nama_rekening,
         // state company
         desc: this.props.navigation.state.params.detail_profile.desc,
         website: this.props.navigation.state.params.detail_profile.website,
         location: this.props.navigation.state.params.detail_profile.location,
         company_name: this.props.navigation.state.params.detail_profile.company_name,
         company_email: this.props.navigation.state.params.detail_profile.company_email,
         // photo dan cover
         logo: '',
         cover: ''
      };
      this.setDate = this.setDate.bind(this);
   }

   setDate(newDate) {
      this.setState({ chosenDate: newDate });
   }

   openImage = () => {
      // More info on all the options is below in the API Reference... just some common use cases shown here
      const options = {
         title: 'Select Avatar',
         customButtons: [
            {
               name: 'fb', title: 'Choose Photo from Facebook',
               name: 'google drive', title: 'Choose Photo from Google Drive'
            }
         ],
         storageOptions: {
            skipBackup: true,
            path: 'images',
         },
         quality: 0.3
      };
      /**
       * The first arg is the options object for customization (it can also be null or omitted for default options),
       * The second arg is the callback which sends object: response (more info in the API Reference)
       */
      ImagePicker.showImagePicker(options, (response) => {
         console.log('Response = ', response);

         if (response.didCancel) {
            console.log('User cancelled image picker');
         } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
         } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
         } else {
            // const source = { uri: response.uri };

            // You can also display the image using data:
            const source = response;
            this.setState({
               logo: source,
            });
         }
      });
   }

   openCover = () => {
      // More info on all the options is below in the API Reference... just some common use cases shown here
      const options = {
         title: 'Select Avatar',
         customButtons: [
            {
               name: 'fb', title: 'Choose Photo from Facebook',
               name: 'google drive', title: 'Choose Photo from Google Drive'
            }
         ],
         storageOptions: {
            skipBackup: true,
            path: 'images',
         },
         quality: 0.3
      };
      /**
       * The first arg is the options object for customization (it can also be null or omitted for default options),
       * The second arg is the callback which sends object: response (more info in the API Reference)
       */
      ImagePicker.showImagePicker(options, (response) => {
         console.log('Response = ', response);

         if (response.didCancel) {
            console.log('User cancelled image picker');
         } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
         } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
         } else {
            // const source = { uri: response.uri };

            // You can also display the image using data:
            const source = response;
            this.setState({
               cover: source,
            });
         }
      });
   }

   validasiForm = () => {
      // if (this.state.name == '') {
      //    Alert.alert('Notifikasi', 'Column name mohon untuk diisi')
      // } else if (this.state.alamat_personal == '') {
      //    Alert.alert('Notifikasi', 'Column address mohon untuk diisi')
      // } else if (this.state.email == '') {
      //    Alert.alert('Notifikasi', 'Column Email mohon untuk diisi')
      // } else if (this.state.phone == '') {
      //    Alert.alert('Notifikasi', 'Column Phone mohon untuk diisi')
      // } else if (this.state.date_of_birth == '') {
      //    Alert.alert('Notifikasi', 'Column Tanggal Lahir mohon untuk diisi')
      // } else if (this.state.no_npwp == '') {
      //    Alert.alert('Notifikasi', 'Column Nomer NPWP mohon untuk diisi')
      // } else if (this.state.nama_npwp == '') {
      //    Alert.alert('Notifikasi', 'Column Nama NPWP mohon untuk diisi')
      // } else if (this.state.alamat_npwp == '') {
      //    Alert.alert('Notifikasi', 'Column Alamat NPWP mohon untuk diisi')
      // } else if (this.state.nama_bank == '') {
      //    Alert.alert('Notifikasi', 'Column Nama Bank mohon untuk diisi')
      // } else if (this.state.nomer_rekening == '') {
      //    Alert.alert('Notifikasi', 'Column Nomer Rekening mohon untuk diisi')
      // } else if (this.state.nama_rekening == '') {
      //    Alert.alert('Notifikasi', 'Column Nama Rekening mohon untuk diisi')
      // } else if (this.state.website == '') {
      //    Alert.alert('Notifikasi', 'Column Website mohon untuk diisi')
      // } else if (this.state.location == '') {
      //    Alert.alert('Notifikasi', 'Column Location mohon untuk diisi')
      // } else if (this.state.company_name == '') {
      //    Alert.alert('Notifikasi', 'Column Nama Company mohon untuk diisi')
      // } else if (this.state.company_email == '') {
      //    Alert.alert('Notifikasi', 'Column Email Company mohon untuk diisi')
      // } else {
      { this.onButtonClick() }
      // }
   }

   validasiSave = () =>
      Alert.alert(
         "Notifikasi",
         "Update Berhasil dan perubahan akan disimpan",
         [
            {
               text: "Cancel",
               onPress: () => console.log("Cancel Pressed"),
               style: "cancel"
            },
            { text: "OK", onPress: () => this.props.navigation.navigate('MyProfile') }
         ],
         { cancelable: false }
      );

   onButtonClick = async () => {

      this.setState({
         isLoading: true
      })

      const date = this.state.chosenDate.getDate()
      const month = this.state.chosenDate.getMonth()
      const year = this.state.chosenDate.getFullYear()

      const tanggal = year + '-' + (month + 1) + '-' + date

      const data = {
         id_user: this.state.user.id_user,

         name: this.state.name,
         alamat_personal: this.state.alamat_personal,
         phone: this.state.phone,
         date_of_birth: tanggal,

         no_npwp: this.state.no_npwp,
         nama_npwp: this.state.nama_npwp,
         alamat_npwp: this.state.alamat_npwp,
         nama_bank: this.state.nama_bank,
         nomer_rekening: this.state.nomer_rekening,
         nama_rekening: this.state.nama_rekening,

         status: 'educator',
         desc: this.state.desc,
         location: this.state.location,
         company_name: this.state.company_name,
         website: this.state.website,
         company_email: this.state.company_email
      }

      console.log('result', data);
      const result = await editProfile(data)
      if (result) {
         this.validasiSave()
      } else {
         Alert.alert('Notifikasil', 'Update yang dilakuakn gagal')
      }
      console.log('hasil update', result.data.data[0]);
   }

   saveLogo = async () => {
      const data = {
         id_user: this.state.user.id_user,
         name: this.state.name,
         status: 'educator',
         avatar: this.state.logo
      }

      const result = await changePhoto(data)
      if (result) {
         Alert.alert('Notifikasil', 'Selamat update berhasil')
         this.props.navigation.navigate('MyProfile')
      } else {
         Alert.alert('Notifikasil', 'update gagal')
      }
      // console.log('hasil update', result.data);
   }

   saveCover = async () => {
      const data = {
         id_user: this.state.user.id_user,
         name: this.state.name,
         cover: this.state.cover
      }

      const result = await changeCover(data)
      if (result) {
         Alert.alert('Notifikasil', 'Selamat update berhasil')
         this.props.navigation.navigate('MyProfile')
      } else {
         Alert.alert('Notifikasil', 'Selamat update gagal')
      }
      // console.log('hasil update', result.data);
   }


   render() {
      // console.log(this.state.login)
      return (
         <SafeAreaView style={{ height: '100%' }}>
            <HeaderApp
               props={this.props.navigation}
               tittle="Edit Profile"
            />
            <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: 'white', height: '100%' }}>
               {/* <View style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}>
                  <Text style={{ fontSize: 25, fontWeight: 'bold' }}>Edit your account</Text>
                  <Text style={{ color: 'red', fontStyle: 'italic', }}>* Pastikan data yang diisi sudah benar *</Text>
               </View> */}

               <View style={styles.container1}>
                  <View style={{ paddingVertical: 20 }}>
                     <Text style={[styles.title, { fontSize: 25, alignSelf: 'center', fontWeight: 'bold', color: '#52057B' }]}>Data Personal</Text>

                  </View>
                  <Text style={styles.title}>Fullname</Text>
                  <TextInput
                     style={styles.inputBox}
                     value={this.state.name}
                     placeholder="name"
                     onChangeText={(text) => this.setState({ name: text })}
                  />
                  <Text style={styles.title}>Alamat</Text>
                  <TextInput
                     style={styles.inputBox}
                     value={this.state.alamat_personal}
                     placeholder="name"
                     onChangeText={(text) => this.setState({ alamat_personal: text })}
                  />
                  <Text style={styles.title}>No. phone</Text>
                  <TextInput
                     style={styles.inputBox}
                     value={this.state.phone}
                     placeholder="name"
                     onChangeText={(text) => this.setState({ phone: text })}
                  />
                  <Text style={styles.title}>Tanggal Lahir</Text>
                  <View style={{ backgroundColor: 'white', padding: 5, margin: 10, borderRadius: 25 }}>
                     <DatePicker
                        defaultDate={new Date()}
                        locale={"en"}
                        timeZoneOffsetInMinutes={undefined}
                        modalTransparent={false}
                        animationType={'slide'}
                        androidMode={"default"}
                        value={this.state.date_of_birth}
                        placeHolderText={this.state.date_of_birth}
                        textStyle={{ color: "green" }}
                        placeHolderTextStyle={{ color: "#black" }}
                        onDateChange={this.setDate}
                        disabled={false}
                     />
                  </View>

                  <View style={{ flexDirection: 'row', height: 200, marginVertical: 20 }}>
                     <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', padding: 15 }}>
                        <Text style={[styles.title, { padding: 10, marginTop: 10 }]}>Edit Logo</Text>
                        <Image
                           source={
                              this.state.logo ? this.state.logo :
                                 { uri: 'https://skills.id/admin/source/images/educator/logo/' + this.state.user.logo }
                           }
                           style={{ height: '60%', backgroundColor: 'grey', width: '100%', borderRadius: 10 }} />
                        <View>
                           {
                              this.state.logo ?
                                 <TouchableOpacity
                                    onPress={() => this.saveLogo()}
                                    style={[styles.button, { margin: 10, paddingHorizontal: 10 }]}>
                                    <Text style={[styles.buttonText, { fontSize: 15 }]}>Save</Text>
                                 </TouchableOpacity> :
                                 <TouchableOpacity
                                    onPress={() => this.openImage()}
                                    style={[styles.button, { margin: 10, paddingHorizontal: 10 }]}>
                                    <Text style={[styles.buttonText, { fontSize: 15 }]}>Choose Photo</Text>
                                 </TouchableOpacity>
                           }
                        </View>
                     </View>
                     <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', padding: 15 }}>
                        <Text style={[styles.title, { padding: 10, marginTop: 10 }]}>Edit Cover</Text>
                        <Image
                           source={
                              this.state.cover ? this.state.cover :
                                 { uri: 'https://skills.id/admin/source/images/educator/cover/' + this.state.user.cover }
                           }
                           style={{ height: '60%', backgroundColor: 'grey', width: '100%', borderRadius: 10 }} />
                        <View>
                           {
                              this.state.cover ?
                                 <TouchableOpacity
                                    onPress={() => this.saveCover()}
                                    style={[styles.button, { margin: 10, paddingHorizontal: 10 }]}>
                                    <Text style={[styles.buttonText, { fontSize: 15 }]}>Save</Text>
                                 </TouchableOpacity> :
                                 <TouchableOpacity
                                    onPress={() => this.openCover()}
                                    style={[styles.button, { margin: 10, paddingHorizontal: 10 }]}>
                                    <Text style={[styles.buttonText, { fontSize: 15 }]}>Choose Photo</Text>
                                 </TouchableOpacity>
                           }
                        </View>
                     </View>
                  </View>

                  <View style={{ paddingVertical: 20 }}>
                     <Text style={[styles.title, { fontSize: 25, alignSelf: 'center', fontWeight: 'bold', color: '#52057B', marginTop: 50 }]}>Data Bank</Text>
                     {/* <View
                        style={{
                           borderBottomColor: 'lightgrey',
                           borderBottomWidth: 3,
                        }}
                     /> */}
                  </View>

                  <View style={{}}>
                     <View style={{}}>
                        <Text style={styles.title}>Nomor NPWP</Text>
                        <TextInput
                           style={styles.inputBox}
                           value={this.state.no_npwp}
                           placeholder="name"
                           onChangeText={(text) => this.setState({ no_npwp: text })}
                        />
                        <Text style={styles.title}>Nama NPWP</Text>
                        <TextInput
                           style={styles.inputBox}
                           value={this.state.nama_npwp}
                           placeholder="name"
                           onChangeText={(text) => this.setState({ nama_npwp: text })}
                        />
                        <Text style={styles.title}>Alamat NPWP</Text>
                        <TextInput
                           multiline={true}
                           style={styles.inputBox}
                           value={this.state.alamat_npwp}
                           placeholder="name"
                           onChangeText={(text) => this.setState({ alamat_npwp: text })}
                        />
                     </View>
                     <View style={{}}>
                        <Text style={styles.title}>Nama Bank</Text>
                        <TextInput
                           style={styles.inputBox}
                           value={this.state.nama_bank}
                           placeholder="name"
                           onChangeText={(text) => this.setState({ nama_bank: text })}
                        />
                        <Text style={styles.title}>Nomor Rekening Bank</Text>
                        <TextInput
                           style={styles.inputBox}
                           value={this.state.nomer_rekening}
                           placeholder="name"
                           onChangeText={(text) => this.setState({ nomer_rekening: text })}
                        />
                        <Text style={styles.title}>Nama Rekening Bank</Text>
                        <TextInput
                           style={styles.inputBox}
                           value={this.state.nama_rekening}
                           placeholder="name"
                           onChangeText={(text) => this.setState({ nama_rekening: text })}
                        />
                     </View>
                  </View>
                  <View style={{ paddingVertical: 20 }}>
                     <Text style={[styles.title, { fontSize: 25, alignSelf: 'center', fontWeight: 'bold', color: '#52057B', marginTop: 50 }]}>Data Company</Text>
                     {/* <View
                        style={{
                           borderBottomColor: 'lightgrey',
                           borderBottomWidth: 3,
                        }}
                     /> */}
                  </View>
                  <Text style={styles.title}>Nama Perusahaan</Text>
                  <TextInput
                     style={styles.inputBox}
                     value={this.state.company_name}
                     placeholder="name"
                     onChangeText={(text) => this.setState({ company_name: text })}
                  />
                  <Text style={styles.title}>Deskripsi Perusahaan</Text>
                  <TextInput
                     multiline
                     style={[styles.inputBox, { minHeight: 100 }]}
                     value={this.state.desc}
                     placeholder="deskripsi"
                     onChangeText={(text) => this.setState({ desc: text })}
                  />
                  <Text style={styles.title}>Lokasi Perusahaan</Text>
                  <TextInput
                     style={styles.inputBox}
                     value={this.state.location}
                     placeholder="name"
                     onChangeText={(text) => this.setState({ location: text })}
                  />
                  <Text style={styles.title}>Website</Text>
                  <TextInput
                     style={styles.inputBox}
                     value={this.state.website}
                     placeholder="name"
                     onChangeText={(text) => this.setState({ website: text })}
                  />
                  <Text style={styles.title}>Email Perusahaan</Text>
                  <TextInput
                     style={styles.inputBox}
                     value={this.state.company_email}
                     placeholder="email perusahaan"
                     onChangeText={(text) => this.setState({ company_email: text })}
                  />
                  <TouchableOpacity
                     onPress={() => this.validasiForm()}
                     style={styles.button}>
                     <Text style={styles.buttonText}>Submit</Text>
                  </TouchableOpacity>
               </View>
            </ScrollView>
         </SafeAreaView>
      )
   }
}


const styles = StyleSheet.create({
   container: {
      justifyContent: 'center',
      alignItems: 'center'
   },
   container1: {
      marginHorizontal: 15,
      marginBottom: 150
   },
   title: {
      fontSize: 18,
      fontWeight: '600',
      color: '#16a596'
   },
   inputBox: {
      width: '100%',
      marginBottom: 25,
      borderWidth: 1.5,
      // borderStyle: 'dashed',
      borderBottomColor: '#ea2c62',
      borderTopColor: 'white',
      borderRightColor: 'white',
      borderLeftColor: 'white',
      height: 50,
      backgroundColor: 'white',
      borderRadius: 0,
      fontSize: 15,
      marginVertical: 10,
   },
   button: {
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      backgroundColor: '#16a596',
      borderRadius: 25,
      paddingVertical: 15
   },
   buttonText: {
      fontSize: 18,
      fontWeight: '500',
      color: 'white',
      textAlign: 'center',
      alignItems: 'center',
   },
   buttonText1: {
      fontSize: 16,
      textAlign: 'right',
      marginHorizontal: '10%'
   },
   signupTextCont: {
      flexGrow: 1,
      alignItems: 'flex-end',
      justifyContent: 'center',
      paddingVertical: 16,
      flexDirection: 'row'
   },
   buttonfb: {
      width: '85%',
      backgroundColor: '#3b5998',
      borderRadius: 100,
      marginVertical: 10,
      paddingVertical: 15,
   },
   buttong: {
      width: '85%',
      backgroundColor: '#e74c3c',
      borderRadius: 100,
      marginVertical: 10,
      paddingVertical: 15
   },
})
