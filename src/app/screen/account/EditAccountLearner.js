import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, TextInput, ScrollView, SafeAreaView, PermissionsAndroid, Alert, ActivityIndicator, Image } from 'react-native'
import { Form, Picker, Icon } from 'native-base'
import ImagePicker from 'react-native-image-picker';
import HeaderApp from '../../components/HeaderApp';

import { editProfile, editProfileWithPhoto } from '../../../api/profile_api'


export default class EditAccount extends Component {
   constructor(props) {
      // tarik data dari page profile
      // console.log('editnew', props.navigation.state.params.detail_profile);
      super(props);
      this.state = {
         selected: undefined,
         user: this.props.navigation.state.params.detail_profile,
         name: this.props.navigation.state.params.detail_profile.name,
         address: this.props.navigation.state.params.detail_profile.address,
         no_handphone: this.props.navigation.state.params.detail_profile.handphone,
         email: this.props.navigation.state.params.detail_profile.email,
         photo: '',
         image: this.props.navigation.state.params.detail_profile.photo,
         loading: false
      };
      // console.log('learner', this.state.user);

   }

   requestCameraPermission = async () => {
      try {
         const granted = await PermissionsAndroid.request(
            [
               PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
               PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
               PermissionsAndroid.PERMISSIONS.CAMERA
            ],
            {
               title: "Cool Photo App Camera Permission",
               message:
                  "Cool Photo App needs access to your camera " +
                  "so you can take awesome pictures.",
               buttonNeutral: "Ask Me Later",
               buttonNegative: "Cancel",
               buttonPositive: "OK"
            }
         );
         if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log("You can use the camera");
         } else {
            console.log("Camera permission denied");
         }
      } catch (err) {
         console.warn(err);
      }
      console.log("Camera permission denied");
   };

   onValueChange(value) {
      this.setState({
         selected: value
      });
   }

   validasiForm = () => {
      if (this.state.name == '') {
         Alert.alert('Notifikasi', 'Column name mohon untuk diisi')
      } else if (this.state.address == '') {
         Alert.alert('Notifikasi', 'Column address mohon untuk diisi')
      } else if (this.state.no_handphone == '') {
         Alert.alert('Notifikasi', 'Column name mohon untuk diisi')
      } else {
         { this.onButtonClick() }
      }
   }

   openImage = () => {
      // More info on all the options is below in the API Reference... just some common use cases shown here
      const options = {
         title: 'Select Avatar',
         customButtons: [
            {
               name: 'fb', title: 'Choose Photo from Facebook',
               name: 'google drive', title: 'Choose Photo from Google Drive'
            }
         ],
         storageOptions: {
            skipBackup: true,
            path: 'images',
            quality: 0.2
         },
      };
      /**
       * The first arg is the options object for customization (it can also be null or omitted for default options),
       * The second arg is the callback which sends object: response (more info in the API Reference)
       */
      ImagePicker.showImagePicker(options, (response) => {
         console.log('Response = ', response);

         if (response.didCancel) {
            console.log('User cancelled image picker');
         } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
         } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
         } else {
            // const source = { uri: response.uri };
            // You can also display the image using data:
            const source = response;
            this.setState({
               photo: source,
            });
         }
      });
   }

   deleteImage = async () => {
      this.setState({
         photo: ''
      })
      { this.getTrainer() }
   }

   onButtonClick = async () => {
      this.setState({
         loading: true
      })

      if (this.state.photo == '') {
         const data = {
            name: this.state.name,
            email: this.state.email,
            address: this.state.address,
            no_handphone: this.state.no_handphone,
            id_user: this.state.user.id_user,
            status: 'learner'
         }
         console.log('ini', this.state.photo)
         const result = await editProfile(data)
         if (result) {
            this.setState({
               loading: false
            })
            Alert.alert('Notifikasil', 'Berhasil')
            this.props.navigation.navigate('MyProfile')
         } else {
            this.setState({
               loading: false
            })
            Alert.alert('Notifikasil', 'Selamat update gagal')
         }
      } else {
         const data = {
            photo: this.state.photo,
            slug: this.state.name.replace(' ', '-'),
            name: this.state.name,
            email: this.state.email,
            address: this.state.address,
            no_handphone: this.state.no_handphone,
            id_user: this.state.user.id_user,
            status: 'learner'
         }
         console.log('ini photo', this.state.photo)
         const result = await editProfileWithPhoto(data)

         console.log('result', data)
         if (result) {
            this.setState({
               loading: false
            })
            Alert.alert('Notifikasil', 'Selamat update berhasil')
            this.props.navigation.navigate('MyProfile')
         } else {
            this.setState({
               loading: false
            })
            Alert.alert('Notifikasil', 'Selamat update gagal')
         }
      }

   }

   render() {

      const source = 'https://skills.id/admin/source/images/learner/'
      console.log(this.state.photo)
      return (
         <SafeAreaView style={{ flex: 1 }}>
            <HeaderApp
               props={this.props.navigation}
               tittle="Edit Account"
            />
            {
               this.state.loading == false ?
                  <ScrollView showsVerticalScrollIndicator={false} style={{ height: '100%', backgroundColor: 'white' }}>
                     <View style={{ justifyContent: 'center', alignItems: 'center', height: 80 }}>
                        <Text style={{ fontSize: 25, fontWeight: 'bold' }}>Edit your account</Text>
                     </View>

                     <View style={styles.container1}>
                        <Text style={styles.title}>Fullname</Text>
                        <TextInput
                           style={styles.inputBox}
                           value={this.state.name}
                           placeholder="name"
                           onChangeText={(text) => this.setState({ name: text })}
                        />
                        <Text style={styles.title}>Address</Text>
                        <TextInput
                           style={styles.inputBox}
                           value={this.state.address}
                           placeholder="name"
                           onChangeText={(text) => this.setState({ address: text })}
                        />
                        <Text style={styles.title}>No Handphone</Text>
                        <TextInput
                           style={styles.inputBox}
                           value={this.state.no_handphone}
                           placeholder="name"
                           onChangeText={(text) => this.setState({ no_handphone: text })}
                        />
                        <View style={{ marginVertical: 20, marginHorizontal: 10 }}>
                           <Text style={styles.title}>Profile Picture</Text>
                           <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                              <View style={{ backgroundColor: 'lightgrey', height: 150, width: '55%', borderRadius: 15 }}>
                                 <Image
                                    style={{ height: 150, width: '100%', borderRadius: 15 }}
                                    source={
                                       this.state.photo !== '' ?
                                          this.state.photo :
                                          { uri: source + this.state.image }
                                    }
                                 />
                              </View>
                              {
                                 this.state.photo ?
                                    <TouchableOpacity
                                       onPress={() => this.deleteImage()}
                                       style={[styles.button, { flex: 1, marginHorizontal: 5 }]}>
                                       <Text style={[styles.buttonText, { fontSize: 12 }]}>Cancel</Text>
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity
                                       // onPress={() => this.requestCameraPermission()}
                                       onPress={() => this.openImage()}
                                       style={[styles.button, { flex: 1, marginHorizontal: 5 }]}>
                                       <Text style={[styles.buttonText, { fontSize: 12 }]}>Choose Photo</Text>
                                    </TouchableOpacity>
                              }
                           </View>
                        </View>
                        <TouchableOpacity style={styles.button}
                           onPress={() => this.validasiForm()}
                        >
                           <Text style={styles.buttonText}>Update Profile</Text>
                        </TouchableOpacity>
                     </View>
                  </ScrollView> :
                  <View style={{ justifyContent: 'center', alignItems: 'center', height: '100%' }}>
                     <Text style={{ paddingVertical: 10 }}>Mohon Tunggu</Text>
                     <ActivityIndicator size="large" color="#0000ff" />
                  </View>
            }
         </SafeAreaView>
      )
   }
}


const styles = StyleSheet.create({
   container: {
      justifyContent: 'center',
      alignItems: 'center'
   },
   container1: {
      marginHorizontal: 15,
      marginBottom: 20
   },
   title: {
      fontSize: 18,
      fontWeight: '600',
      color: '#16a596'
   },
   inputBox: {
      width: '100%',
      marginBottom: 25,
      borderWidth: 1.5,
      // borderStyle: 'dashed',
      borderBottomColor: '#ea2c62',
      borderTopColor: 'white',
      borderRightColor: 'white',
      borderLeftColor: 'white',
      height: 50,
      backgroundColor: 'white',
      borderRadius: 0,
      fontSize: 15,
      marginVertical: 10,
   },
   button: {
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      backgroundColor: '#16a596',
      borderRadius: 25,
      paddingVertical: 15
   },
   buttonText: {
      fontSize: 18,
      fontWeight: '500',
      color: 'white',
      textAlign: 'center',
      alignItems: 'center',
   },
   buttonText1: {
      fontSize: 16,
      textAlign: 'right',
      marginHorizontal: '10%'
   },
   signupTextCont: {
      flexGrow: 1,
      alignItems: 'flex-end',
      justifyContent: 'center',
      paddingVertical: 16,
      flexDirection: 'row'
   },
   buttonfb: {
      width: '85%',
      backgroundColor: '#3b5998',
      borderRadius: 100,
      marginVertical: 10,
      paddingVertical: 15,
   },
   buttong: {
      width: '85%',
      backgroundColor: '#e74c3c',
      borderRadius: 100,
      marginVertical: 10,
      paddingVertical: 15
   },
})
