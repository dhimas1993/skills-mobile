import React, { Component } from 'react'
import {
   StyleSheet,
   Text,
   View,
   Image,
   TouchableOpacity,
   ImageBackground,
   Alert, SafeAreaView, ScrollView, RefreshControl, ActivityIndicator
} from 'react-native';
import HTMLView from 'react-native-htmlview';
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker';

import Icons from 'react-native-vector-icons/FontAwesome5';
import { getProfile, changeCover } from '../../../api/profile_api'
import HeaderApp from '../../components/HeaderApp';

import LoginPage from '../login/loginScreen/LoginPage'


class Account extends Component {
   constructor(props) {
      super(props)
      this.state = {
         lastRefresh: Date(Date.now()).toString(),
         user: {},
         isLoading: false,
         photo: '',
         refreshing: false
      }
      this.refreshScreen = this.refreshScreen.bind(this)
   }

   _onRefresh() {
      this.setState({ refreshing: true });
      this.getProfile().then(() => {
         this.setState({ refreshing: false });
      });
   }

   openImage = () => {
      // More info on all the options is below in the API Reference... just some common use cases shown here
      const options = {
         title: 'Select Avatar',
         customButtons: [
            {
               name: 'fb', title: 'Choose Photo from Facebook'
            }
         ],
         storageOptions: {
            skipBackup: true,
            path: 'images',
            quality: 0.3
         },
      };

      /**
       * The first arg is the options object for customization (it can also be null or omitted for default options),
       * The second arg is the callback which sends object: response (more info in the API Reference)
       */
      ImagePicker.showImagePicker(options, (response) => {
         console.log('Response = ', response);

         if (response.didCancel) {
            console.log('User cancelled image picker');
         } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
         } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
         } else {
            // const source = { uri: response.uri };

            // You can also display the image using data:
            const source = response;
            this.setState({
               photo: source,
            });
         }
      });
   }

   getProfile = async () => {

      this.setState({
         isLoading: true
      })

      const data = {
         id_user: this.props.user.auth.data.id_user
      }
      const api = await getProfile(data)
      if (api.status == 200) {
         // console.log('hasil api', api.data[0]);
         this.setState({
            user: api.data[0],
            isLoading: false
         })
      } else {
         console.log('api gagal');
      }
   }

   componentDidMount() {
      this.getProfile()
   }

   async refreshScreen() {
      const data = {
         id_user: this.props.user.auth.data.id_user
      }
      const api = await getProfile(data)
      if (api.status == 200) {
         // console.log('hasil api', api.data[0]);
         this.setState({
            user: api.data[0],
            isLoading: true
         })
      } else {
         console.log('api gagal');
      }
   }

   _toEditProfile = (param) => {
      const { as_educator } = this.state.user
      if (as_educator == "1") {
         this.props.navigation.navigate('EditProfileEducator', { detail_profile: param })
      } else {
         this.props.navigation.navigate('EditProfileLearner', { detail_profile: param })
      }
   }

   renderEducator = () => {
      const {
         name, email, logo, cover, location, desc, company_name, tagline, phone, facebook_url, twitter_url, linked_url, google_url, as_educator, as_learner, date_of_birth,
         photo, address, handphone, tgl_register, alamat_personal, company_email, nama_npwp, no_npwp, alamat_npwp, nama_bank, nomer_rekening, nama_rekening
      } = this.state.user

      console.log("desc", desc)
      return (
         <SafeAreaView style={{ flex: 1 }}>
            <HeaderApp
               props={this.props.navigation}
               tittle="Profile"
            />
            <ScrollView style={{}}
               refreshControl={
                  <RefreshControl
                     refreshing={this.state.refreshing}
                     onRefresh={this._onRefresh.bind(this)}
                  />
               }
            >
               <View style={styles.container}>
                  <ImageBackground style={styles.header}
                     source={
                        cover ?
                           { uri: 'https://skills.id/admin/source/images/educator/cover/' + cover } :
                           null
                     } />
                  <Image style={styles.avatar}
                     source={
                        logo ?
                           { uri: 'https://skills.id/admin/source/images/educator/logo/' + logo } :
                           { uri: 'https://www.pngfind.com/pngs/m/110-1102775_download-empty-profile-hd-png-download.png' }
                     } />
                  <View style={styles.body}>
                     <View style={styles.bodyContent}>
                        <Text style={[styles.name, { color: 'white' }]}>{name}</Text>
                        <Text style={[styles.name, { fontSize: 15, color: 'white' }]}>{email}</Text>
                        <View style={{ flexDirection: 'row', marginHorizontal: 10, justifyContent: 'space-around', marginTop: 15, width: '100%' }}>
                           <TouchableOpacity onPress={() => this.props.navigation.navigate('Password', { detail_profile: this.state.user })}
                              style={{ backgroundColor: '#ea2c62', paddingHorizontal: 20, borderRadius: 50, width: '45%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                              <Text style={{ color: 'white', fontWeight: '500', textAlign: 'center' }}>Change Password</Text>
                           </TouchableOpacity>
                           <TouchableOpacity onPress={() => this._toEditProfile(this.state.user)}
                              style={{ backgroundColor: '#ea2c62', paddingHorizontal: 45, borderRadius: 50, width: '45%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                              <Text style={{ color: 'white', fontWeight: '500', textAlign: 'center' }}>Edit Profile</Text>
                           </TouchableOpacity>
                        </View>
                        <View style={{ marginVertical: 0, alignSelf: 'center', }}>
                           {/* <HTMLView
                              value={desc}
                              stylesheet={styles}
                           /> */}
                        </View>

                        <View style={{ marginTop: 30, marginHorizontal: 30, marginBottom: 10 }}>
                           <Text style={{ fontSize: 25, fontWeight: 'bold', marginLeft: 20, marginBottom: 5 }}>Account Info</Text>
                           <View style={{ flexDirection: 'row', marginTop: 15, alignItems: 'center' }}>
                              <Icons name='user-tie' style={{ width: '20%', fontSize: 25, textAlign: 'center', alignItems: 'center', color: '#078b6d' }} />
                              <View style={{ width: '100%', justifyContent: 'space-evenly' }}>
                                 <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Name</Text>
                                 <Text style={{ fontSize: 12 }}>{name}</Text>
                              </View>
                           </View>
                           <View style={{ flexDirection: 'row', marginTop: 15, alignItems: 'center' }}>
                              <Icons name='mail-bulk' style={{ width: '20%', fontSize: 25, textAlign: 'center', justifyContent: 'center', color: '#078b6d' }} />
                              <View style={{ width: '100%', justifyContent: 'space-evenly' }}>
                                 <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Email</Text>
                                 <Text style={{ fontSize: 12 }}>{email}</Text>
                              </View>
                           </View>
                           <View style={{ flexDirection: 'row', marginTop: 15, alignItems: 'center' }}>
                              <Icons name='phone' style={{ width: '20%', fontSize: 25, textAlign: 'center', justifyContent: 'center', color: '#078b6d' }} />
                              <View style={{ width: '100%', justifyContent: 'space-evenly' }}>
                                 <Text style={{ fontSize: 15, fontWeight: 'bold' }}>No Handphone</Text>
                                 <Text style={{ fontSize: 12 }}>{location}</Text>
                              </View>
                           </View>
                           <View style={{ flexDirection: 'row', marginTop: 15, alignItems: 'center' }}>
                              <Icons name='map' style={{ width: '20%', fontSize: 25, textAlign: 'center', justifyContent: 'center', color: '#078b6d' }} />
                              <View style={{ width: '100%', justifyContent: 'space-evenly' }}>
                                 <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Address</Text>
                                 <Text style={{ fontSize: 12 }}>{alamat_personal}</Text>
                              </View>
                           </View>
                        </View>

                        <View style={{ marginTop: 20, marginHorizontal: 30, marginBottom: 30 }}>
                           <Text style={{ fontSize: 25, fontWeight: 'bold', marginLeft: 20, marginBottom: 5 }}>Company Info</Text>
                           <View style={{ flexDirection: 'row', marginTop: 15, alignItems: 'center' }}>
                              <Icons name='building' style={{ width: '20%', fontSize: 25, textAlign: 'center', alignItems: 'center', color: '#078b6d' }} />
                              <View style={{ width: '100%', justifyContent: 'space-evenly' }}>
                                 <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Company Name</Text>
                                 <Text style={{ fontSize: 12 }}>{company_name}</Text>
                              </View>
                           </View>
                           <View style={{ flexDirection: 'row', marginTop: 15, alignItems: 'center' }}>
                              <Icons name='mail-bulk' style={{ width: '20%', fontSize: 25, textAlign: 'center', justifyContent: 'center', color: '#078b6d' }} />
                              <View style={{ width: '100%', justifyContent: 'space-evenly' }}>
                                 <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Email Company</Text>
                                 <Text style={{ fontSize: 12 }}>{company_email}</Text>
                              </View>
                           </View>
                           <View style={{ flexDirection: 'row', marginTop: 15, alignItems: 'center' }}>
                              <Icons name='map' style={{ width: '20%', fontSize: 25, textAlign: 'center', justifyContent: 'center', color: '#078b6d' }} />
                              <View style={{ width: '100%', justifyContent: 'space-evenly' }}>
                                 <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Location Company</Text>
                                 <Text style={{ fontSize: 12 }}>{location}</Text>
                              </View>
                           </View>
                        </View>
                     </View>
                  </View>
               </View>
            </ScrollView>
         </SafeAreaView>
      )
   }

   renderLearner = () => {
      const {
         name, email, logo, cover, location, desc, company_name, tagline, phone, facebook_url, twitter_url, linked_url, google_url, as_educator, as_learner, date_of_birth,
         photo, address, handphone, tgl_register, alamat_personal, company_email, nama_npwp, no_npwp, alamat_npwp, nama_bank, nomer_rekening, nama_rekening
      } = this.state.user

      return (
         <SafeAreaView style={{ height: '100%' }}>
            <HeaderApp
               props={this.props.navigation}
               tittle="Account Page"
            />
            <ScrollView style={{ width: '100%' }}
               refreshControl={
                  <RefreshControl
                     refreshing={this.state.refreshing}
                     onRefresh={this._onRefresh.bind(this)}
                  />
               }
            >
               <View style={{ backgroundColor: '#16a596', height: 265, width: '100%' }}>
                  <ImageBackground
                     style={{ width: '100%', height: '100%', position: 'absolute' }} />
                  {
                     photo ?
                        <Image style={{ width: 130, height: 130, borderRadius: 100, borderWidth: 1, borderColor: "white", marginBottom: 0, alignSelf: 'center', marginTop: 30, marginBottom: 10 }}
                           source={{ uri: 'https://skills.id/admin/source/images/learner/' + photo }}
                        />
                        :
                        <Image style={{ width: 130, height: 130, borderRadius: 100, borderWidth: 1, borderColor: "white", marginBottom: 0, alignSelf: 'center', marginTop: 30, marginBottom: 10 }}
                           source={{ uri: 'https://skills.id/admin/source/images/nophoto.jpg' }}
                        />
                  }
                  <Text style={{ alignSelf: 'center', fontSize: 30, fontWeight: '500', letterSpacing: 1, textAlign: 'center', marginBottom: 5, color: 'white' }}>{name}</Text>
                  <Text style={{ alignSelf: 'center', fontSize: 15, fontWeight: '300', letterSpacing: 1, color: 'white' }}>{email}</Text>

                  <View style={{ flexDirection: 'row', marginHorizontal: 20, justifyContent: 'space-around', marginTop: 10, position: 'relative' }}>
                     <TouchableOpacity onPress={() => this.props.navigation.navigate('Password', { detail_profile: this.state.user })}
                        style={{ backgroundColor: '#ea2c62', paddingHorizontal: 20, borderRadius: 50, width: '45%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ color: 'white', fontWeight: '500', textAlign: 'center' }}>Change Password</Text>
                     </TouchableOpacity>
                     <TouchableOpacity onPress={() => this._toEditProfile(this.state.user)}
                        style={{ backgroundColor: '#ea2c62', paddingHorizontal: 20, borderRadius: 50, width: '45%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ color: 'white', fontWeight: '500', textAlign: 'center' }}>Edit Profile</Text>
                     </TouchableOpacity>
                  </View>
               </View>

               <View style={{ marginTop: 50, marginHorizontal: 30, marginBottom: 150 }}>
                  <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 20, marginBottom: 5 }}>Account Info</Text>
                  <View style={{ flexDirection: 'row', marginTop: 15, alignItems: 'center' }}>
                     <Icons name='user-tie' style={{ width: '20%', fontSize: 25, textAlign: 'center', alignItems: 'center', color: '#078b6d' }} />
                     <View style={{ width: '100%', justifyContent: 'space-evenly' }}>
                        <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Name</Text>
                        <Text style={{ fontSize: 12 }}>{name}</Text>
                     </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 15, alignItems: 'center' }}>
                     <Icons name='mail-bulk' style={{ width: '20%', fontSize: 25, textAlign: 'center', justifyContent: 'center', color: '#078b6d' }} />
                     <View style={{ width: '100%', justifyContent: 'space-evenly' }}>
                        <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Email</Text>
                        <Text style={{ fontSize: 12 }}>{email}</Text>
                     </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 15, alignItems: 'center' }}>
                     <Icons name='phone' style={{ width: '20%', fontSize: 25, textAlign: 'center', justifyContent: 'center', color: '#078b6d' }} />
                     <View style={{ width: '100%', justifyContent: 'space-evenly' }}>
                        <Text style={{ fontSize: 15, fontWeight: 'bold' }}>No Handphone</Text>
                        <Text style={{ fontSize: 12 }}>{handphone}</Text>
                     </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 15, alignItems: 'center' }}>
                     <Icons name='map' style={{ width: '20%', fontSize: 25, textAlign: 'center', justifyContent: 'center', color: '#078b6d' }} />
                     <View style={{ width: '100%', justifyContent: 'space-evenly' }}>
                        <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Address</Text>
                        <Text style={{ fontSize: 12 }}>{address}</Text>
                     </View>
                  </View>
               </View>
            </ScrollView>
         </SafeAreaView>
      )
   }

   render() {
      const { as_educator, as_learner } = this.state.user
      if (this.state.isLoading == false) {
         if (as_educator == 1 && as_learner == 0) {
            return (
               this.renderEducator()
            )
         } else if (as_educator == 0 && as_learner == 1) {
            return (
               this.renderLearner()
            )
         } else if (as_educator == 1 && as_learner == 1) {
            return (
               this.renderEducator()
            )
         } else {
            return (
               <LoginPage {...this.props} />
            )
         }
      } else {
         return (
            <View style={{
               flex: 1,
               justifyContent: "center"
            }}>
               <ActivityIndicator size="large" color="#ea2c62" />
            </View>
         )
      }
   }
}

const mapStateToProps = state => {
   return {
      user: state.auth
   }
}

export default connect(mapStateToProps)(Account)

const styles = StyleSheet.create({
   container: {
      width: '100%',
   },
   p: {
      fontSize: 15,
      textAlign: 'center'
   },
   header: {
      position: 'absolute', backgroundColor: '#16a596', height: 265, width: '100%'
   },
   avatar: {
      width: 130,
      height: 130,
      borderRadius: 100,
      borderWidth: 1,
      borderColor: "white",
      marginBottom: 0,
      alignSelf: 'center',
      marginTop: 30,
      marginBottom: 10
   },
   name: {
      fontSize: 22,
      color: '#078b6d',
      fontWeight: 'bold',
   },
   body: {
      marginTop: 0,
   },
   bodyContent: {
      alignItems: 'center',
      marginHorizontal: 15,
   },
   info: {
      fontSize: 16,
      color: '#078b6d',
      marginTop: 10
   },
   descriptiontitle: {
      fontSize: 18,
      padding: 10
   },
   description: {
      fontSize: 16,
      opacity: 0.7,
      color: 'black',
      marginTop: 5
   },
   buttonContainer: {
      marginTop: 10,
      height: 45,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: 5,
      width: 250,
      borderRadius: 30,
      backgroundColor: '#078b6d',
   },
   btntext: {
      color: 'white'
   }
})
