import React, { Component } from 'react'
import { Text, StyleSheet, View, TextInput, TouchableOpacity, Alert, ScrollView, SafeAreaView, ActivityIndicator } from 'react-native'
import { changePass } from '../../../api/profile_api'

export default class Password extends Component {
   constructor(props) {
      // tarik data dari page profile
      // console.log('editnew', props.navigation.state.params.detail_profile.id_user);

      super(props);
      this.state = {
         new_pass: '',
         confirm_pass: '',
         isLoading: false
      };
   }

   confirmPass = () => {
      const baru = this.state.new_pass
      const confirm = this.state.confirm_pass

      if (baru == confirm && confirm !== '') {
         return (
            <View style={styles.center}>
               <Text style={{ color: '#078b6d' }}>* Password sudah sama</Text>
            </View>
         )
      } else {
         return (
            <View style={styles.center}>
               <Text style={{ color: 'red' }}>* Mohon ulang password anda*</Text>
            </View>
         )
      }
   }

   onbuttonClick = async () => {
      this.setState({
         isLoading: true
      })

      if (this.state.new_pass == '' || this.state.confirm_pass == '') {
         Alert.alert('Alert', 'Mohon lengkapi kolom yang masih kosong')
      } else if (this.state.new_pass !== this.state.confirm_pass) {
         Alert.alert('Alert', 'Password berbeda')
      } else {
         const id_user = this.props.navigation.state.params.detail_profile.id_user
         const data = {
            id_user: id_user,
            password: this.state.new_pass
         }
         console.log(data);
         const api = await changePass(data)
         if (api) {
            this.setState({
               isLoading: false
            })
            Alert.alert('Notifikasil', 'Selamat update berhasil')
         } else {
            Alert.alert('Notifikasil', 'Selamat update gagal')
         }
         console.log('hasil update', api.data);
         this.props.navigation.navigate('MyProfile')
      }
   }

   render() {
      if (this.state.isLoading == false) {
         return (
            <SafeAreaView style={{ flex: 1, }}>
               <ScrollView style={{ flex: 1, backgroundColor: 'white' }}>
                  <View style={styles.container}>
                     <View style={{ justifyContent: 'center', alignItems: 'center', height: 80, marginVertical: 50 }}>
                        <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#16a596' }}>Change your password</Text>
                        <Text style={{ color: '#ea2c62', fontStyle: 'italic', }}>* Pastikan data yang diisi sudah benar *</Text>
                     </View>
                     <View>
                        <Text style={styles.title}>New Password</Text>
                        <TextInput secureTextEntry={true} style={styles.inputBox} value={this.state.new_pass}
                           placeholder='Type your password here'
                           onChangeText={(text) => this.setState({ new_pass: text })}
                        />
                     </View>
                     <View>
                        <Text style={styles.title}>Confirm Password</Text>
                        <TextInput secureTextEntry={true} style={styles.inputBox} value={this.state.confirm_pass}
                           placeholder='Type it again'
                           onChangeText={(text) => this.setState({ confirm_pass: text })}
                        />
                     </View>
                     <View>
                        {
                           this.confirmPass()
                        }
                     </View>
                     <TouchableOpacity
                        onPress={() => this.onbuttonClick()}
                        style={styles.button}>
                        <Text style={styles.buttonText}>Save</Text>
                     </TouchableOpacity>
                  </View>
               </ScrollView>
            </SafeAreaView>
         )
      } else {
         return (
            <View style={{ justifyContent: 'center', alignItems: 'center', height: '100%', paddingTop: 200 }}>
               <ActivityIndicator size="large" color="#0000ff" />
            </View>
         )
      }
   }
}

const styles = StyleSheet.create({
   container: {
      marginHorizontal: 15, marginBottom: 150
   },
   title: {
      fontSize: 18,
      fontWeight: '600',
      color: '#16a596'
   },
   center: {
      alignSelf: 'center',
      marginVertical: 50
   },
   text: {
      marginTop: 10,
      paddingHorizontal: 20,
      fontSize: 16
   },
   inputBox: {
      width: '100%',
      marginBottom: 25,
      borderWidth: 1.5,
      // borderStyle: 'dashed',
      borderBottomColor: '#ea2c62',
      borderTopColor: 'white',
      borderRightColor: 'white',
      borderLeftColor: 'white',
      height: 50,
      backgroundColor: 'white',
      borderRadius: 0,
      fontSize: 15,
      marginVertical: 10,
   },
   button: {
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      backgroundColor: '#16a596',
      borderRadius: 25,
      paddingVertical: 15
   },
   buttonText: {
      fontSize: 18,
      fontWeight: '500',
      color: 'white',
      textAlign: 'center',
      alignItems: 'center',
   },
})
