import React,{ Component } from 'react'
import { Text,StyleSheet,View,ScrollView,Dimensions,ImageBackground,TouchableOpacity } from 'react-native'
import { Container,Content,Icon } from 'native-base'

import FooterProduct from './FooterProduct'

import Icons from '../../../../node_modules/react-native-vector-icons/AntDesign'
const DEVICE_WIDTH = Dimensions.get("window").width;
const image = [
   {
      id: '1',
      url: "https://images.unsplash.com/photo-1563380796506-db3af952fa60?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
   },
   {
      id: '2',
      url: "https://images.unsplash.com/photo-1563380796418-2b797de14a96?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
   },
   {
      id: '3',
      url: "https://images.unsplash.com/photo-1444824775686-4185f172c44b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
   },
   {
      id: '4',
      url: "https://images.unsplash.com/photo-1568041606897-15ad588b2682?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
   }
]

export default class Product extends Component {
   render() {
      return (
         <Container>
            <Content>
               <View style={{ maxHeight: 400 }}>
                  <ScrollView horizontal={true} pagingEnabled showsHorizontalScrollIndicator={false}>
                     {
                        image.map((image) => {
                           return (
                              <View style={{ width: DEVICE_WIDTH,height: '90%' }}>
                                 <ImageBackground
                                    style={{ width: DEVICE_WIDTH,height: '100%' }}
                                    source={{ uri: image.url }}>
                                    <View style={{ flex: 1,flexDirection: 'row' }}>
                                       <TouchableOpacity style={{ flex: 1,flexDirection: 'row',alignItems: 'center',marginLeft: 20 }}>
                                          <Icons
                                             style={{ fontSize: 20 }}
                                             name="leftcircle" />
                                       </TouchableOpacity>
                                       <TouchableOpacity style={{ flex: 1,flexDirection: 'row',alignItems: 'center',justifyContent: 'flex-end',marginRight: 20 }}>
                                          <Icons
                                             style={{ fontSize: 20 }}
                                             name="rightcircle" />
                                       </TouchableOpacity>
                                    </View>
                                 </ImageBackground>
                              </View>
                           )
                        })
                     }
                  </ScrollView>
                  <View style={{ flexDirection: 'row',alignSelf: "center",marginTop: -20,marginBottom: 10 }}>
                     {
                        image.map((dot,index) => {
                           return (
                              <View style={styles.dot} />
                           )
                        })
                     }
                  </View>
               </View>
               <View style={{ paddingHorizontal: 10 }}>
                  <View style={{}}>
                     <Text style={styles.title}>CD Sales Vol. 1</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                     <View style={{ flex: 2,flexDirection: 'row',justifyContent: 'flex-start' }}>
                        <View style={{ flexDirection: "row",justifyContent: 'center' }}>
                           <Icon name="star" style={{ color: 'orange',fontSize: 25 }} />
                           <Icon name="star" style={{ color: 'orange',fontSize: 25 }} />
                           <Icon name="star" style={{ color: 'orange',fontSize: 25 }} />
                           <Icon name="star" style={{ color: 'orange',fontSize: 25 }} />
                           <Icon name="star" style={{ color: 'orange',fontSize: 25 }} />
                        </View>
                     </View>
                     <View style={{ flex: 2 }}>
                        <Text style={{ textAlign: 'center',fontSize: 17,marginTop: 2 }}>100% ORI</Text>
                     </View>
                     <View style={{ flex: 2 }}>
                        <Text style={{ textAlign: 'center',fontSize: 17,marginTop: 2 }}>Gratis Ongkir</Text>
                     </View>
                  </View>


                  <View style={{ marginVertical: 10 }}>
                     <View style={{ height: 30,justifyContent: "center" }}>
                        <Text style={{ fontSize: 17 }}>Rincian Product</Text>
                     </View>
                     <View style={{ flexDirection: 'row' }}>
                        <Text style={{ flex: 1,fontSize: 15 }}>Harga</Text>
                        <Text style={{ flex: 3,fontSize: 15 }}>: Rp. 120.000.000.000</Text>
                     </View>
                     <View style={{ flexDirection: 'row' }}>
                        <Text style={{ flex: 1,fontSize: 15 }}>Stok</Text>
                        <Text style={{ flex: 3,fontSize: 15 }}>: 17 Pcs</Text>
                     </View>
                     <View style={{ flexDirection: 'row' }}>
                        <Text style={{ flex: 1,fontSize: 15 }}>Brand</Text>
                        <Text style={{ flex: 3,fontSize: 15 }}>: Motivator Series</Text>
                     </View>
                     <View style={{ flexDirection: 'row' }}>
                        <Text style={{ flex: 1,fontSize: 15 }}>Bahan</Text>
                        <Text style={{ flex: 3,fontSize: 15 }}>: Plastik</Text>
                     </View>
                     <View style={{ flexDirection: 'row' }}>
                        <Text style={{ flex: 1,fontSize: 15 }}>Dikirim Dari</Text>
                        <Text style={{ flex: 3,fontSize: 15 }}>: Meruya, Jakarta Barat</Text>
                     </View>
                     <View style={{ marginVertical: 15 }}>
                        <Text style={{ flex: 1,fontSize: 15,marginBottom: 5 }}>Deskripsi : </Text>
                        <Text style={{ flex: 1,textAlign: 'justify' }}>{'\t'}{'\t'}{'\t'}{'\t'}Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam varius urna ut metus suscipit tempus. Donec ullamcorper egestas vestibulum. Aenean auctor sit amet nisi sed pellentesque. Sed feugiat condimentum feugiat. Morbi ut nibh ac felis pellentesque commodo et non velit. Vivamus aliquet sapien lacus, quis luctus felis blandit eu. Sed et eleifend dui. Mauris ornare pharetra hendrerit. Nam sed commodo ante. Curabitur sed fringilla neque. Sed eleifend, lacus eu viverra lobortis, dui arcu porta nulla, ac ultricies felis velit in sem. Mauris molestie mauris nec leo interdum, sit amet molestie massa rutrum. Nullam feugiat nunc vel elit laoreet, sit amet lobortis tellus luctus.</Text>
                     </View>
                  </View>
               </View>
            </Content>
            <FooterProduct {...this.props} />
         </Container>
      )
   }
}

const styles = StyleSheet.create({
   title: {
      fontSize: 25,
      textAlign: 'justify',
      marginVertical: 5
   },
   dot: {
      width: 10,
      height: 10,
      backgroundColor: 'grey',
      borderRadius: 100,
      marginRight: 10,
   }
})
