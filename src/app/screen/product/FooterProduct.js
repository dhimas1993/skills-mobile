import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, Alert } from 'react-native'
import { Icon } from 'native-base'

export default class FooterProduct extends Component {
   render() {
      return (
         <View style={{ backgroundColor: 'pink', flexDirection: 'row', height: 60, backgroundColor: '#078b6d' }}>
            <TouchableOpacity
               onPress={() => Alert.alert('Notifikasi', 'Product Sudah berhasil ditambahkan kedalam keranjang')}
               style={styles.cart}>
               <Icon name="cart" style={{ color: 'white', fontSize: 25, marginHorizontal: 10 }} />
               <Text style={styles.text}>Add To Cart</Text>
            </TouchableOpacity>
            <TouchableOpacity
               onPress={() => pushScreen(this.props.componentId, 'FacilitatorPage')}
               style={styles.cart}>
               <Icon name="person" style={{ color: 'white', fontSize: 25, marginHorizontal: 10 }} />
               <Text style={styles.text}>Profil Penjual</Text>
            </TouchableOpacity>
         </View>
      )
   }
}

const styles = StyleSheet.create({
   cart: {
      width: '100%',
      backgroundColor: '#078b6d',
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
   },
   text: {
      color: 'white'
   }
})
