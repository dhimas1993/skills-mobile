import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, TouchableOpacity } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { SafeAreaView } from 'react-navigation'

import HeaderApp from '../../components/HeaderApp'

export default class verified extends Component {
   render() {
      return (
         <SafeAreaView style={{ flex: 1 }}>
            <HeaderApp
               props={this.props.navigation}
               tittle="Verified Account"
            />
            <View style={styles.container1}>
               <Image
                  source={{ uri: 'https://img.favpng.com/19/11/23/airplane-paper-plane-png-favpng-1PsqmzZJfKRDtQd0MbF77dnXF.jpg' }}
                  style={{ height: 200, backgroundColor: 'pink', width: '100%' }}
               />
               <Text style={styles.tittle}>Check Your Email !!</Text>
               <Text style={styles.head}>Verify your email to finish signing up for Skills.id</Text>
               <Text style={styles.desc}>Please Confirm your email address by clicking on the link in your inbox to login in our apps or website</Text>
               <Text style={[styles.head, { fontSize: 30, color: '#078b6d', fontWeight: '500' }]}>Login Here !!</Text>
               <TouchableOpacity onPress={() => { this.props.navigation.navigate('LoginPage') }} style={{ backgroundColor: 'red', padding: 10, paddingHorizontal: 30, borderRadius: 50 }}>
                  <Text style={{ color: 'white' }}>Login</Text>
               </TouchableOpacity>
            </View>
         </SafeAreaView>
      )
   }
}

const styles = StyleSheet.create({
   container: {
      backgroundColor: "pink",
      flex: 1
   },
   container1: {
      padding: 15,
      alignItems: 'center', justifyContent: 'center',
      textAlign: 'center',
      height: '80%'
   },
   tittle: {
      fontWeight: 'bold',
      fontSize: 30,
      color: '#078b6d'
   },
   head: {
      marginVertical: 15,
      fontWeight: '200',
      fontSize: 25,
      textAlign: 'center'
   },
   desc: {
      fontWeight: '300',
      fontSize: 15,
      textAlign: 'center'
   }
})
