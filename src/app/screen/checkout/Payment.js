import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableHighlight, TouchableOpacity, Image, Alert } from 'react-native'
import { Icon, Accordion, ListItem, List, Card, Container, Content } from "native-base";
import { SafeAreaView } from 'react-navigation';
import { ScrollView } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';

import { uploadPayment } from '../../../api/payment_api'

const dataArray = [
   {
      id: 1,
      title: 'Petunjuk Transfer ATM',
      content: [
         "Pilih menu lain > transfer",
         "Pilih jenis rekening asal dan pilih Virtual ACcount Billing",
         "Masukan No Virtual Account dan pilih benar",
         "Tagihan harus dibayar akan muncul dilayar konfirmasi",
         "Periksa detail dari pembayaran dan jika sudah yakin benar, Pilih YA"
      ]
   },
   {
      id: 2,
      title: 'Petuntuk Transfer iBanking',
      content: [
         "Pilih Transfer > Virtual Account Billing",
         "Masukan nomor Virtual Account",
         "Pilih rekening Debet dan lanjut",
         "Tagihan yang harus dibayar akan muncul pada layar konfirmasi",
         "Periksa informasi yang tertera di layar, dan pastikan merchant adalah shopee dan nama customer adalah nama kamu",
         'masukan kode otentifikasi token anda lalu ketik proses'
      ]
   },
   {
      id: 3,
      title: 'Petunjuk transfer mBanking',
      content: [
         "Pilih transfer > Virtual Account Billing",
         "Pilih rekening debet > masukan nomer VA pada menu input baru",
         "tagihan akan keluar pada layar konfirmasi",
         "Periksa Informasi yang tertera di layar. pastikan Merchant adalah Shopee dan username kamu sesuai, jika benar masukan password transaksi dan pilih lanjut"
      ]
   },
   {
      id: 4,
      title: 'Petunjuk Transfer SMS Banking',
      content: [
         "Kirim SMS 'TRANSFER no virtual account' ke 3346",
         "Balas SMS yang masuk dengan benar"
      ]
   }
]

export default class Shipping extends Component {
   constructor(props) {
      // console.log('payment', props.navigation.state.params.item);
      super(props);
      this.state = {
         // id_user: this.props.user.auth.data.id_user,
         data: this.props.navigation.state.params.item,
         gambar: '',
         isLoading: false
      }
   }

   _renderHeader(item, expanded) {
      return (
         <View style={{
            padding: 10,
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: "center",
            backgroundColor: "white"
         }}>
            <Text style={{ flexWrap: 'wrap', fontSize: 15, flex: 1 }}>
               {item.title}
            </Text>
            {
               expanded
                  ? <Icon style={{ fontSize: 15 }} name="remove-circle" />
                  : <Icon style={{ fontSize: 15 }} name="add-circle" />
            }
         </View>
      );
   }

   _renderContent(item) {
      return (
         <View
            style={{
               backgroundColor: "white",
               fontStyle: "italic",
               justifyContent: 'flex-start'
            }}>
            {
               item.content.map((items) => {
                  return (
                     <List>
                        <ListItem>
                           <Text style={{ fontSize: 8, flexWrap: 'wrap', marginLeft: -10, marginRight: 5 }}> {'\u2B24'} </Text>
                           <Text style={{ paddingHorizontal: 3, textAlign: 'justify', fontSize: 12 }}>{items}</Text>
                        </ListItem>
                     </List>
                  )
               })
            }
         </View>
      );
   }

   openImage = () => {
      // More info on all the options is below in the API Reference... just some common use cases shown here
      const options = {
         title: 'Select Avatar',
         customButtons: [
            {
               name: 'fb', title: 'Choose Photo from Facebook',
               name: 'google drive', title: 'Choose Photo from Google Drive'
            }
         ],
         storageOptions: {
            skipBackup: true,
            path: 'images',
            quality: 0.2
         },
      };
      /**
       * The first arg is the options object for customization (it can also be null or omitted for default options),
       * The second arg is the callback which sends object: response (more info in the API Reference)
       */
      ImagePicker.showImagePicker(options, (response) => {
         console.log('Response = ', response);

         if (response.didCancel) {
            console.log('User cancelled image picker');
         } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
         } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
         } else {
            // const source = { uri: response.uri };

            // You can also display the image using data:
            const source = response;
            this.setState({
               gambar: source,
            });
         }
      });
   }

   uploadPayment = async () => {
      this.setState({
         isLoading: true
      })

      if (this.state.gambar == '') {
         Alert.alert('Notifikasi', 'Mohon Pilih foto bukti pembayaran terlebih dahulu')
         this.setState({
            isLoading: false
         })
      } else {
         const data = {
            gambar: this.state.gambar,
            id_user: this.state.data.id_user,
            code: this.state.data.code
         }
         console.log('data', data);
         const result = await uploadPayment(data)
         if (result) {
            this.setState({
               isLoading: false
            })
         }
         console.log('hasil update', result);
         Alert.alert('Berhasil');
         this.props.navigation.navigate('More')

      }
   }

   render() {
      const { total_payment } = this.state.data

      return (
         <SafeAreaView style={{ height: '100%' }}>
            <ScrollView style={{ height: '100%' }}>
               <View >
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 10, paddingVertical: 20, backgroundColor: 'white', marginBottom: 5 }}>
                     <Text style={{ fontSize: 15 }}>Total Pembayaran</Text>
                     <Text style={{ color: 'red', fontSize: 15 }}>Rp, {Number(total_payment).toLocaleString("IN")}</Text>
                  </View>

                  <View style={{}}>
                     <View style={{ backgroundColor: 'white', padding: 10 }}>
                        <Text style={{ fontSize: 15 }}>Bank BCA (Verifikasi Manual)</Text>
                     </View>
                     <View style={{ backgroundColor: 'white', padding: 10 }}>
                        <Text style={{ fontSize: 15, marginVertical: 5 }}>No. Rekening</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', left: 0 }}>
                           <Text style={{ fontSize: 25, color: 'orange' }}>1786020205</Text>
                        </View>
                     </View>
                     <View style={{ backgroundColor: 'white', padding: 10 }}>
                        <Text style={{ fontSize: 12, marginVertical: 5 }}>Setelah selesai upload bukti pembayaran/struck</Text>
                     </View>
                  </View>

                  {
                     this.state.data.metode_bayar == 'banktransfer' ?
                        <View style={{ backgroundColor: 'white', marginVertical: 10 }}>
                           <View style={{ padding: 10, flexDirection: 'row' }}>
                              {
                                 this.state.gambar ?
                                    <Image
                                       style={{ flex: 2, backgroundColor: 'lightgrey', height: 150, borderRadius: 10 }}
                                       source={this.state.gambar}
                                    />
                                    :
                                    <View style={{ flex: 2, backgroundColor: 'lightgrey', height: 150, borderRadius: 10 }}></View>
                              }
                              <View style={{ flex: 1, height: 150, justifyContent: 'center' }}>
                                 <TouchableOpacity onPress={() => this.openImage()} style={{ alignItems: 'center', backgroundColor: '#078b6d', margin: 10, padding: 20, borderRadius: 10 }}>
                                    <Text style={[styles.title, { color: 'white' }]}>Upload</Text>
                                 </TouchableOpacity>
                              </View>
                           </View>
                        </View> :
                        null
                  }

                  <View style={{}}>
                     <Card style={{ borderRadius: 10 }}>
                        <Accordion
                           style={{}}
                           dataArray={dataArray}
                           animation={true}
                           expanded={true}
                           renderHeader={this._renderHeader}
                           renderContent={this._renderContent}
                        />
                     </Card>
                  </View>
               </View>
            </ScrollView>
            <View style={{}}>
               <TouchableOpacity
                  onPress={() => this.uploadPayment()}
                  style={{ height: 50, backgroundColor: '#078b6d', margin: 10, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={[styles.title, { color: 'white' }]}>Korfirmasi Pembayaran</Text>
               </TouchableOpacity>
            </View>
         </SafeAreaView>
      )
   }
}

const styles = StyleSheet.create({
   title: {
      fontSize: 15,
      fontWeight: 'bold',
   }
})
