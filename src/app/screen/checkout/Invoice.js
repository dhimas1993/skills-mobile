import React, { Component } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { SafeAreaView } from 'react-navigation'

export default class Invoice extends Component {
   constructor(props) {
      // console.log('wkpage', props.navigation.state.params);
      super(props);
      this.state = {
         data: this.props.navigation.state.params.item
      }
   }

   render() {
      console.log(this.state.data);
      const { code, expired, metode_bayar, potongan, potongan_prakerja, potongan_promo, potongan_referal, send_by, status_payment, tgl_transaksi, tgl_transfer, total_payment, warning } = this.state.data
      return (
         <SafeAreaView>
            <ScrollView>
               <View style={styles.box}>
                  <Image
                     style={{ backgroundColor: 'white', height: 60, width: 225, marginVertical: 10, alignSelf: 'center' }}
                     source={{ uri: 'https://skills.id/source/images/logo_new.png' }}
                  />

                  <View style={{ alignItems: 'center', marginTop: 10 }}>
                     <Text>{code}</Text>
                     <Text>Created : {tgl_transaksi}</Text>
                     <Text>Status : {status_payment}</Text>
                  </View>

                  <View style={{ marginTop: 20 }}>
                     <View style={styles.th}>
                        <Text style={styles.td}>Detail</Text>
                        <Text style={styles.td}>Price</Text>
                     </View>
                     <View style={{ borderWidth: 1, borderColor: 'grey', marginTop: 5 }} />
                  </View>
                  <View style={{ marginTop: 20 }}>
                     <View style={styles.th}>
                        <Text style={styles.td1}>Potongan Promo</Text>
                        <Text style={styles.td1}>Rp, {parseInt(potongan_promo).toLocaleString("IN")}</Text>
                     </View>
                  </View>
                  <View style={{ marginTop: 20 }}>
                     <View style={styles.th}>
                        <Text style={styles.td1}>Saldo Referral</Text>
                        <Text style={styles.td1}>Rp, {parseInt(potongan_referal).toLocaleString("IN")}</Text>
                     </View>
                  </View>
                  <View style={{ marginTop: 20 }}>
                     <View style={styles.th}>
                        <Text style={styles.td1}>Potongan Voucher</Text>
                        <Text style={styles.td1}>Rp, {parseInt(potongan).toLocaleString("IN")}</Text>
                     </View>
                  </View>
                  <View style={{ marginTop: 20 }}>
                     <View style={styles.th}>
                        <Text style={styles.td1}>Potongan Prakerja</Text>
                        <Text style={styles.td1}>Rp, {parseInt(potongan_prakerja).toLocaleString("IN")}</Text>
                     </View>
                  </View>
                  <View style={{ marginVertical: 20, }}>
                     <View style={styles.th}>
                        <Text style={styles.td1}>Grand Total</Text>
                        <Text style={styles.td1}>Rp, {parseInt(total_payment).toLocaleString("IN")}</Text>
                     </View>
                  </View>
               </View>
            </ScrollView>
         </SafeAreaView>
      )
   }
}

const styles = StyleSheet.create({
   box: {
      backgroundColor: 'white',
      margin: 10,
      padding: 5,
      paddingHorizontal: 15,

      borderRadius: 10,
      borderColor: 'lightgrey',
      borderWidth: 2
   },
   th: {
      flexDirection: 'row',
      justifyContent: 'space-between',
   },
   td: {
      fontSize: 16,
      fontWeight: 'bold',
   }
})
