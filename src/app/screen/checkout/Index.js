import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView, SafeAreaView, TouchableOpacity, Alert, ActivityIndicator } from 'react-native'
import { Picker } from '@react-native-community/picker';
import { connect } from 'react-redux'
import { getPayment_waiting, getPayment_verified, cancelPayment } from '../../../api/payment_api'

import HeaderApp from '../../components/HeaderApp'

class Index extends Component {
   constructor(props) {
      // console.log('wkpage', props.navigation.state.params);
      super(props);
      this.state = {
         id_user: this.props.user.auth.data.id_user,
         status: 'All',
         waiting: '',
         verified: ''
      }
   }

   componentDidMount() {
      this.getPayment_waiting()
      this.getPayment_verified()
   }

   onValueChange(value) {
      this.setState({
         status: value,
      });
   }

   getPayment_waiting = async () => {
      const id_user = this.state.id_user

      const data = {
         'id_user': id_user
      }

      const api = await getPayment_waiting(data)
      // console.log(api.data.data)
      if (api) {
         this.setState({
            waiting: api.data.data
         })
      }
   }

   getPayment_verified = async () => {
      const id_user = this.state.id_user

      const data = {
         'id_user': id_user
      }

      const api = await getPayment_verified(data)
      // console.log(api.data.data)
      if (api) {
         this.setState({
            verified: api.data.data
         })
      }
   }

   cancelPayment = async (item) => {
      // console.log(item);
      const code = item.code
      const status = item.status_payment
      const data = {
         'code': code,
         'status_payment': status
      }

      const api = await cancelPayment(data)
      console.log(api.data);

      this.getPayment_waiting()
   }

   render_waiting = () => {
      if (this.state.waiting !== '') {
         return (
            this.state.waiting.map((item) => {
               return (
                  <View>
                     <View style={{ backgroundColor: 'white', borderRadius: 10, marginHorizontal: 10, marginVertical: 5 }}>
                        <View style={{ flexDirection: 'row', marginHorizontal: 15, marginTop: 10 }}>
                           <View style={{ flex: 2, justifyContent: 'center' }}>
                              <Text style={styles.title}>No Invoice</Text>
                           </View>
                           <View style={{ flex: 3, alignItems: 'flex-end', justifyContent: 'center' }}>
                              <Text style={styles.title}>{item.code}</Text>
                           </View>
                        </View>
                        <View style={{ flexDirection: 'row', marginHorizontal: 15, marginTop: 10 }}>
                           <View style={{ flex: 2, justifyContent: 'center' }}>
                              <Text style={styles.title}>Total Harga</Text>
                           </View>
                           <View style={{ flex: 3, alignItems: 'flex-end', justifyContent: 'center' }}>
                              <Text style={styles.title}>Rp. {Number(item.total_payment).toLocaleString("IN")}</Text>
                           </View>
                        </View>
                        <View style={{ flexDirection: 'row', marginHorizontal: 15, marginTop: 10 }}>
                           <View style={{ flex: 2, justifyContent: 'center' }}>
                              <Text style={styles.title}>Tanggal Transaksi</Text>
                           </View>
                           <View style={{ flex: 3, alignItems: 'flex-end', justifyContent: 'center' }}>
                              <Text style={styles.title}>07 October 2020</Text>
                           </View>
                        </View>
                        <View style={{ flexDirection: 'row', marginHorizontal: 15, marginTop: 10 }}>
                           <View style={{ flex: 2, justifyContent: 'center' }}>
                              <Text style={styles.title}>Status</Text>
                           </View>
                           <View style={{ flex: 3, alignItems: 'flex-end', justifyContent: 'center' }}>
                              <Text style={[styles.title, { color: 'red' }]}>{item.status_payment}</Text>
                           </View>
                        </View>
                        <View style={{ flexDirection: 'row', marginHorizontal: 15, marginTop: 10 }}>
                           <View style={{ flex: 2, justifyContent: 'center' }}>
                              <Text style={styles.title}>Metode Bayar</Text>
                           </View>
                           <View style={{ flex: 3, alignItems: 'flex-end', justifyContent: 'center' }}>
                              <Text style={[styles.title]}>Bank Transfer</Text>
                           </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                           <TouchableOpacity
                              onPress={() => this.cancelPayment(item)}
                              style={{ flex: 1, alignItems: 'center', marginVertical: 10, marginLeft: 15, marginRight: 5, backgroundColor: 'red', padding: 10, borderRadius: 10 }}>
                              <Text style={[styles.title, { color: 'white', fontWeight: '700' }]}>Delete</Text>
                           </TouchableOpacity>
                           <TouchableOpacity
                              onPress={() => this.props.navigation.navigate('Payment', { item })}
                              style={{ flex: 1, alignItems: 'center', marginVertical: 10, marginLeft: 5, marginRight: 15, backgroundColor: '#078b6d', padding: 10, borderRadius: 10 }}>
                              <Text style={[styles.title, { color: 'white', fontWeight: '700' }]}>Detail</Text>
                           </TouchableOpacity>
                        </View>
                     </View>
                  </View>
               )
            })
         )
      } else {
         <View>
            <Text>Kosong</Text>
         </View>
      }
   }

   render_verified = () => {
      if (this.state.verified !== undefined) {
         return (
            this.state.verified.map((item) => {
               return (
                  <View>
                     <View style={{ backgroundColor: 'white', borderRadius: 10, marginHorizontal: 10, marginVertical: 5 }}>
                        <View style={{ flexDirection: 'row', marginHorizontal: 15, marginTop: 10 }}>
                           <View style={{ flex: 2, justifyContent: 'center' }}>
                              <Text style={styles.title}>No Invoice</Text>
                           </View>
                           <View style={{ flex: 3, alignItems: 'flex-end', justifyContent: 'center' }}>
                              <Text style={styles.title}>{item.code}</Text>
                           </View>
                        </View>
                        <View style={{ flexDirection: 'row', marginHorizontal: 15, marginTop: 10 }}>
                           <View style={{ flex: 2, justifyContent: 'center' }}>
                              <Text style={styles.title}>Total Harga</Text>
                           </View>
                           <View style={{ flex: 3, alignItems: 'flex-end', justifyContent: 'center' }}>
                              <Text style={styles.title}>Rp. {Number(item.total_payment).toLocaleString("IN")}</Text>
                           </View>
                        </View>
                        <View style={{ flexDirection: 'row', marginHorizontal: 15, marginTop: 10 }}>
                           <View style={{ flex: 2, justifyContent: 'center' }}>
                              <Text style={styles.title}>Tanggal Transaksi</Text>
                           </View>
                           <View style={{ flex: 3, alignItems: 'flex-end', justifyContent: 'center' }}>
                              <Text style={styles.title}>07 October 2020</Text>
                           </View>
                        </View>
                        <View style={{ flexDirection: 'row', marginHorizontal: 15, marginTop: 10 }}>
                           <View style={{ flex: 2, justifyContent: 'center' }}>
                              <Text style={styles.title}>Status</Text>
                           </View>
                           <View style={{ flex: 3, alignItems: 'flex-end', justifyContent: 'center' }}>
                              <Text style={[styles.title, { color: 'red' }]}>{item.status_payment}</Text>
                           </View>
                        </View>
                        <View style={{ flexDirection: 'row', marginHorizontal: 15, marginTop: 10 }}>
                           <View style={{ flex: 2, justifyContent: 'center' }}>
                              <Text style={styles.title}>Metode Bayar</Text>
                           </View>
                           <View style={{ flex: 3, alignItems: 'flex-end', justifyContent: 'center' }}>
                              <Text style={[styles.title]}>Bank Transfer</Text>
                           </View>
                        </View>
                        <TouchableOpacity
                           onPress={() => this.props.navigation.navigate('Invoice', { item })}
                           style={{ alignItems: 'center', margin: 15, backgroundColor: '#078b6d', padding: 10, borderRadius: 10 }}>
                           <Text style={[styles.title, { color: 'white' }]}>Detail</Text>
                        </TouchableOpacity>
                     </View>
                  </View>
               )
            })
         )
      } else {
         <View>
            <Text>Kosong</Text>
         </View>
      }
   }

   render_kosong = () => {
      return (
         <View style={{ backgroundColor: 'white', height: 150, width: '100%', alignItems: 'center', justifyContent: 'center' }}>
            <Text>Kosong</Text>
         </View>
      )
   }

   render() {
      if (this.state.isLoading) {
         return (
            <View style={{ backgroundColor: 'white', height: 150, width: '100%', alignItems: 'center', justifyContent: 'center' }}>
               <ActivityIndicator size="large" color="#0000ff" />
            </View>
         )
      } else {
         return (
            <SafeAreaView style={{ flex: 1 }}>
               <HeaderApp
                  props={this.props.navigation}
                  tittle="Payment Confirmation"
               />
               <ScrollView style={{ height: '100%' }}>
                  <View style={{ marginBottom: 10 }}>
                     <View style={{ backgroundColor: 'lightgreen', padding: 10, alignItems: 'center', marginVertical: 5 }}>
                        <Text style={{ fontWeight: 'bold' }}>Menunggu Pembayaran</Text>
                     </View>
                     {
                        this.state.waiting ?
                           this.render_waiting() : this.render_kosong()
                     }
                  </View>
                  <View>
                     <View style={{ backgroundColor: 'orange', padding: 10, alignItems: 'center', marginBottom: 5 }}>
                        <Text style={{ fontWeight: 'bold' }}>Pembayaran sedang verifikasi</Text>
                     </View>
                     {
                        this.state.verified ?
                           this.render_verified() : this.render_kosong()
                     }
                  </View>
               </ScrollView>
            </SafeAreaView>
         )
      }
   }
}

const mapStateToProps = state => {
   return {
      user: state.auth
   }
}

const styles = StyleSheet.create({
   title: {
      fontSize: 15,
      fontWeight: '300',
   }
})

export default connect(mapStateToProps, null)(Index)
