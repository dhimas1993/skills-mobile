import React, { Component } from 'react'
import { Text, StyleSheet, View, SafeAreaView, ScrollView, Image, TouchableOpacity, TextInput } from 'react-native'
import { Container, Content, Icon, Picker, Form } from 'native-base';

export default class Checkout extends Component {
   constructor(props) {
      super(props);
      this.state = {
         count: 1,
         check: true,
         kurir: '',
         bank: '',
         product: [
            {
               "title": "CD Sales Vol. 1",
               "tag": "online",
               "author": "Dhimas Hertianto",
               "Price": 200000,
               "Disc": 100000,
               "weight": 1,
               "image": "https://images.unsplash.com/photo-1470309864661-68328b2cd0a5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
            },
         ]
      };
   }

   _toPayment = () => {
      this.props.navigation.navigate('Payment')
   }

   kurirValueChange(value) {
      this.setState({
         kurir: value
      });
   }
   bankValueChange(value) {
      this.setState({
         bank: value
      });
   }

   render() {
      return (
         <Container>
            <Content>
               <SafeAreaView style={{ paddingBottom: 25 }}>
                  <ScrollView style={{ backgroundColor: 'lightgrey' }} showsVerticalScrollIndicator={false}>
                     <View style={{ justifyContent: 'space-between', flexDirection: 'row', backgroundColor: 'white', paddingHorizontal: 10, paddingVertical: 20 }}>
                        <Text style={{ fontSize: 15 }}>Alamat Pengiriman</Text>
                        <TouchableOpacity>
                           <Text style={{ fontSize: 15 }}>Pilih Alamat Lain</Text>
                        </TouchableOpacity>
                     </View>

                     <View style={{ backgroundColor: 'white', paddingHorizontal: 10, paddingBottom: 20 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                           <Text style={{ fontSize: 15 }}>Alamat Pengiriman</Text>
                           <View style={{ padding: 5, backgroundColor: 'pink', borderRadius: 5, paddingHorizontal: 15 }}>
                              <Text style={{ fontSize: 15 }}>utama</Text>
                           </View>
                        </View>
                        <View>
                           <Text style={{ fontSize: 15 }}>Dhimas (081218302334)</Text>
                           <Text style={{ fontSize: 15 }}>Jl Rawamngun blok 1 no 18 Kel. Rawamangun Kec. Pulogadung</Text>
                        </View>
                     </View>

                     <View style={{ marginVertical: 5 }}>
                        {
                           this.state.product.map((item) => {
                              return (
                                 <View style={{ flex: 1, flexDirection: 'row', backgroundColor: 'white' }}>
                                    <View style={{ flex: 3 }}>
                                       <Image
                                          style={{ flex: 1 }}
                                          source={{ uri: item.image }} />
                                    </View>
                                    <View style={{ flex: 5, margin: 10, justifyContent: 'space-between' }}>
                                       <View>
                                          <Text style={{ fontSize: 15, textAlign: 'justify', fontWeight: 'bold' }}> {item.title} </Text>
                                          <Text style={{ textDecorationLine: 'line-through', fontSize: 12 }}> Rp. {item.Disc} </Text>
                                          <Text style={{ fontSize: 12 }}> Rp. {item.Price} </Text>
                                       </View>
                                       <View>
                                          <Text style={{ fontSize: 12, color: '#078b6d', fontStyle: 'italic' }}>Author : {item.author} </Text>
                                       </View>
                                    </View>
                                    <View style={{ margin: 5, justifyContent: 'space-around', padding: 5, alignItems: 'center', borderColor: 'lightgrey', marginRight: 5, width: '15%' }}>
                                       <Text style={{ fontSize: 20 }}>1x</Text>
                                    </View>
                                 </View>
                              )
                           })
                        }
                     </View>

                     <View style={{ justifyContent: 'space-between', flexDirection: 'row', backgroundColor: 'white', paddingHorizontal: 10, alignItems: 'center', paddingVertical: 5 }}>
                        <Text style={{ flex: 2, marginRight: 10, fontSize: 15 }}>Pilih Pengiriman</Text>
                        <Picker
                           mode="dropdown"
                           iosIcon={<Icon name="arrow-down" />}
                           placeholder="Pilih"
                           placeholderStyle={{ color: "black" }}
                           placeholderIconColor="black"
                           style={{ flex: 1 }}
                           selectedValue={this.state.kurir}
                           onValueChange={this.kurirValueChange.bind(this)}
                        >
                           <Picker.Item label="JNE" value="9000" />
                           <Picker.Item label="JNT" value="10000" />
                           <Picker.Item label="TIKI" value="9500" />
                        </Picker>
                     </View>

                     <View style={{ justifyContent: 'space-between', flexDirection: 'row', backgroundColor: 'white', paddingHorizontal: 10, alignItems: 'center', paddingVertical: 5 }}>
                        <TextInput
                           style={{ flex: 1, marginRight: 10, backgroundColor: 'white', borderRadius: 5, elevation: 5, paddingHorizontal: 10, height: 40, shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, }}
                           placeholder={'Masukan Kode Promo'}
                        />
                        <TouchableOpacity style={{ backgroundColor: 'green', padding: 10, borderRadius: 5, paddingHorizontal: 22 }}>
                           <Text style={{ color: 'white' }}>Submit</Text>
                        </TouchableOpacity>
                     </View>

                     <View style={{ paddingHorizontal: 10, paddingVertical: 20, backgroundColor: 'white' }}>
                        <View style={{ justifyContent: 'space-between', flexDirection: 'row', backgroundColor: 'white', alignItems: 'center' }}>
                           <Text>Promo</Text>
                           <Text style={{ color: 'darkred' }}>Rp. 0</Text>
                        </View>
                        <View style={{ justifyContent: 'space-between', flexDirection: 'row', backgroundColor: 'white', alignItems: 'center', marginVertical: 10 }}>
                           <Text>Pengiriman</Text>
                           <Text style={{ color: 'darkred' }}>Rp. 10.000</Text>
                        </View>
                        <View style={{ justifyContent: 'space-between', flexDirection: 'row', backgroundColor: 'white', alignItems: 'center' }}>
                           <Text>Subtotal</Text>
                           <Text style={{ color: 'darkred' }}>Rp. 600.000</Text>
                        </View>
                     </View>

                     <View style={{ backgroundColor: 'white', padding: 10, paddingTop: 20, marginVertical: 5 }}>
                        <Text>Metode Pembayaran</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                           <Text style={{ flex: 2 }}>Pilih Bank</Text>
                           <Picker
                              mode="dropdown"
                              iosIcon={<Icon name="arrow-down" />}
                              placeholder="Pilih"
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              style={{ flex: 1 }}
                              selectedValue={this.state.bank}
                              onValueChange={this.bankValueChange.bind(this)}
                           >
                              <Picker.Item label="BNI" value="1" />
                              <Picker.Item label="Mandiri" value="2" />
                              <Picker.Item label="BCA" value="3" />
                              <Picker.Item label="BRI" value="4" />
                              <Picker.Item label="VA BNI" value="5" />
                           </Picker>
                        </View>
                     </View>

                  </ScrollView>

               </SafeAreaView>
            </Content>
            <View style={{ backgroundColor: 'lightgrey', height: 95, justifyContent: 'flex-end', position: 'absolute', bottom: 0, width: '100%', paddingBottom: 25 }}>
               <View style={{ justifyContent: 'space-between', flexDirection: 'row', backgroundColor: '#078b6d', padding: 15, alignItems: 'center' }}>
                  <View style={{}}>
                     <Text style={{ color: 'white' }}>Total Tagihan</Text>
                     <Text style={{ color: 'white' }}>Rp. 610.000</Text>
                  </View>
                  <TouchableOpacity
                     onPress={this._toPayment}
                     style={{ backgroundColor: 'darkred', padding: 10, borderRadius: 5, paddingHorizontal: 15 }}>
                     <Text style={{ color: 'white' }}>BAYAR</Text>
                  </TouchableOpacity>
               </View>
            </View>
         </Container>
      )
   }
}

const styles = StyleSheet.create({

})
