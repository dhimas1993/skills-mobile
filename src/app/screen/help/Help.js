import React, { Component } from "react";
import { View, Text } from 'react-native'
import { Container, Content, Icon, Accordion, ListItem, List, Card } from "native-base";
import { SafeAreaView } from "react-navigation";
import { ScrollView } from "react-native-gesture-handler";

const dataArray = [
   {
      id: 1,
      title: 'Siapa saja yang bisa ikut skills.id',
      content: [
         "Semua orang yang ingin upgrade diri",
         "Seseorang yang karna kepentingan jabatan butuh training tertentu",
         "Orang yang sedang bersiap untuk bekerja"
      ]
   },
   {
      id: 2,
      title: 'Apa itu aplikasi mobile Skills.id',
      content: [
         "Aplikasi yang digunakan sebagai berlangganan training atau workshop yang diselenggarakan Facilitator handal",
         "Tempat yang pas untuk menambah iklmu sebelum bekkerja atau mengupgrade diri supaya tidak kalah saing dengan yang lain",
      ]
   },
   {
      id: 3,
      title: 'Bagaimana cara mendaftar di Skills.id',
      content: [
         "Cukup Registrasi di form Login",
         "Pilih Video atau workshop yang ingin kamu ikuti",
         "Lalu checkout dan payment",
         "Selamat kamu akan menjadi pribadi yang lebih baik dan handal"
      ]
   },
   {
      id: 4,
      title: 'Apa saja fitur yang tersedia di Skills.id',
      content: [
         "Yang pertama ada fitur Join Workshop",
         "Kedua ada fitur Subscribe Video Training dengan mentor terbaik di bidangnya",
         "ketiga juga ada kumpulan artikel mengenai Softskill dan Hardskill"
      ]
   },
   {
      id: 5,
      title: 'Bagaimana cara menghubungi layanan Skills.id',
      content: [
         "Bisa di Whatsapp dan di email info@skils.id",
         "Atau di HO kita Di PT Permata Indonesia"
      ]
   },
   {
      id: 6,
      title: 'Bagaimana cara mengetahui promo terbaru dari Skills.id',
      content: [
         "Halaman page kami akan selalu menginfokan promo minggu ini atau flashsale",
         "Mungkin jika tertarik kalian bisa langganan newslatter pada email sehingga kamu bisa menginfokan semua prom yang ada setiap minggunya"
      ]
   },
   {
      id: 7,
      title: 'Apa itu Facilitator ?',
      content: [
         "Itu adalah mentor yang akan memeberikan materi dalam class workshop atau video",
         "Siapapun bisa menjadi Facilitator hanya diperlukan regist dan kamu akan memproses semua"
      ]
   },
   {
      id: 8,
      title: 'Siapa saja yang bisa mendaftar menjadi facilitator',
      content: [
         "Semua Orang yang berniat membagikan ilmu yang bergina apalagi demnanf nya banyak",
         "terbuka untuk semua u,ur kalangan dan semua bidang"
      ]
   },
   {
      id: 9,
      title: 'Bagaimana cara mendaftar sebagai Facilitator ?',
      content: [
         "Hanya perlu tinggal regist dan pilih login sebagai facilitator",
         "Isi data yang diperlukan dan tunggu verivikasi",
         "Setelah verivikasi kalian sudha bisa mulai upload atau add schedule untuk workshop"
      ]
   },
   {
      id: 9,
      title: 'Bagaimana cara mendaftar sebagai Facilitator ?',
      content: [
         "Hanya perlu tinggal regist dan pilih login sebagai facilitator",
         "Isi data yang diperlukan dan tunggu verivikasi",
         "Setelah verivikasi kalian sudha bisa mulai upload atau add schedule untuk workshop"
      ]
   },
   {
      id: 10,
      title: 'Bagaimana cara mendaftar sebagai Facilitator ?',
      content: [
         "Hanya perlu tinggal regist dan pilih login sebagai facilitator",
         "Isi data yang diperlukan dan tunggu verivikasi",
         "Setelah verivikasi kalian sudha bisa mulai upload atau add schedule untuk workshop"
      ]
   },
   {
      id: 11,
      title: 'Bagaimana cara mendaftar sebagai Facilitator ?',
      content: [
         "Hanya perlu tinggal regist dan pilih login sebagai facilitator",
         "Isi data yang diperlukan dan tunggu verivikasi",
         "Setelah verivikasi kalian sudha bisa mulai upload atau add schedule untuk workshop"
      ]
   },
   {
      id: 12,
      title: 'Bagaimana cara mendaftar sebagai Facilitator ?',
      content: [
         "Hanya perlu tinggal regist dan pilih login sebagai facilitator",
         "Isi data yang diperlukan dan tunggu verivikasi",
         "Setelah verivikasi kalian sudha bisa mulai upload atau add schedule untuk workshop"
      ]
   },
]

export default class AccordionCustomHeaderContent extends Component {
   _renderHeader(item, expanded) {
      return (
         <View style={{
            padding: 10,
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: "center",
            backgroundColor: "white"
         }}>
            <Text style={{ flexWrap: 'wrap', fontSize: 15, flex: 1 }}>
               {item.title}
            </Text>
            {
               expanded
                  ? <Icon style={{ fontSize: 15 }} name="remove-circle" />
                  : <Icon style={{ fontSize: 15 }} name="add-circle" />
            }
         </View>
      );
   }

   _renderContent(item) {
      return (
         <View
            style={{
               backgroundColor: "white",
               fontStyle: "italic",
               justifyContent: 'flex-start'
            }}
         >
            {
               item.content.map((items) => {
                  return (
                     <List>
                        <ListItem>
                           <Text style={{ fontSize: 8, flexWrap: 'wrap', marginLeft: -10, marginRight: 5 }}> {'\u2B24'} </Text>
                           <Text style={{ paddingHorizontal: 3, textAlign: 'justify', fontSize: 15 }}>{items}</Text>
                        </ListItem>
                     </List>
                  )
               })
            }
         </View>
      );
   }

   render() {
      return (
         <SafeAreaView>
            <ScrollView>
               <View style={{ padding: 10 }}>
                  <Text style={{ padding: 10, fontSize: 20 }}>Help</Text>
                  <Card style={{ borderRadius: 15 }}>
                     <Accordion
                        style={{ padding: 5 }}
                        dataArray={dataArray}
                        animation={true}
                        expanded={true}
                        renderHeader={this._renderHeader}
                        renderContent={this._renderContent}
                     />
                  </Card>
               </View>
            </ScrollView>
         </SafeAreaView>
      );
   }
}
<br />
