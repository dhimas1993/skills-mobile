import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView } from 'react-native'
import { Card, Content, Container } from 'native-base'
import { SafeAreaView } from 'react-navigation'

export default class Privacy extends Component {

   state = {
      privacy: [
         "Informasi Pribadi yang Dapat Kami Kumpulkan",
         "Penggunaan Informasi Pribadi",
         "Pengungkapan Informasi Pribadi",
         "Penyimpanan Informasi Pribadi",
         "Hak Anda",
         "Kebijakan Cookies",
         "Pengakuan dan Persetujuan",
         "Perubahan dalam Kebijakan Privasi Kami",
         "Cara Menghubungi Kami"
      ],
      a: [
         "Informasi yang Anda berikan. Anda dapat memberikan informasi melalui formulir elektronik pada Platform Kami maupun dengan berkorespondensi melalui telepon, surat elektronik, dan sebagainya. Informasi ini meliputi informasi yang Anda berikan ketika mendaftar pada Platform Kami, berlangganan layanan Kami, mencari produk, berpartisipasi dalam diskusi online atau fungsi media sosial lain pada Platform Kami, mengikuti kompetisi, promosi, atau survei, serta ketika Anda melaporkan masalah dengan Platform Kami. Informasi yang Anda berikan dapat meliputi nama, alamat, alamat surat elektronik, nomor telepon,informasi finansial dan kartu kredit, deskripsi pribadi, foto, dan data lainnya. Kami dapat meminta Anda untuk melakukan verifikasi terhadap informasi yang Anda berikan, untuk memastikan akurasi dari informasi tersebut.",
         "Informasi teknis, meliputi alamat Protokol Internet (IP address) yang digunakan untuk menghubungkan komputer Anda dengan internet, informasi log in Anda, jenis dan versi perambah (browser) yang digunakan, pengaturan zona waktu, jenis dan versi ekstensi perambah (browser plug-in), system operasi dan platform;",
         "Informasi yang Kami terima dari sumber lain. Kami dapat menerima informasi jika Anda menggunakan situs lain yang Kami operasikan atau layanan lain yang Kami sediakan. Kami juga bekerja sama dengan pihak ketiga (termasuk, namun tidak terbatas pada misalnya, mitra bisnis, sub-kontraktor dalam pelayanan teknis, jasa pembayaran dan pengantaran, jaringan periklanan, penyedia analisis, penyedia pencarian informasi, serta agen acuan kredit) (“Mitra Kami”) dan dapat menerima informasi dari mereka. Kami akan mengambil langkah-langkah dalam batas kewajaran untuk melakukan verifikasi terhadap informasi yang Kami dapatkan dari sumber lain sesuai dengan Peraturan yang Berlaku."

      ]
   }

   render() {
      return (
         <SafeAreaView>
            <ScrollView>
               <View style={{ padding: 10, flex: 1 }}>
                  <Text style={{ fontSize: 25, padding: 10 }}>Privacy</Text>
                  <Card style={{ padding: 10, borderRadius: 10 }}>
                     <View style={{}}>
                        <Text style={{ textAlign: 'justify', fontSize: 15 }}>
                           Kerahasian Informasi Pribadi adalah hal yang terpenting bagi PT. Ruang Raya Indonesia(“Kami”). Kami berkomitmen untuk melindungi dan menghormati privasi pengguna (“Anda”) saat mengakses dan menggunakan aplikasi, situs web (www.ruangguru.com dan situs web lain yang Kami kelola), fitur, teknologi, konten dan produk yang Kami sediakan (selanjutnya, secara Bersama-sama disebut sebagai “Platform”),
                  {"\n"}
                        </Text>
                        <Text style={{ textAlign: 'justify', fontSize: 15 }}>
                           Kebijakan Privasi ini mengatur landasan dasar mengenai bagaimana Kami menggunakan informasi pribadi yang Kami kumpulkan dan/atau atau Anda berikan (“Informasi Pribadi”). Kebijakan Privasi ini berlaku bagi seluruh pengguna Platform, kecuali diatur pada Kebijakan Privasi yang terpisah. Mohon membaca Kebijakan Privasi Kami dengan seksama sehingga Anda dapat memahami pendekatan dan cara Kami dalam menggunakan informasi tersebut.
                  {"\n"}
                        </Text>
                        <View style={{ textAlign: 'justify', fontSize: 15 }}>
                           <Text style={{}}>Kebijakan Privasi ini mencakup hal-hal berikut :</Text>
                           <View style={{ marginTop: 10, marginLeft: 10 }}>
                              {
                                 this.state.privacy.map((privacy) => {
                                    return (
                                       <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10 }}>
                                          <Text style={{ fontSize: 8, flexWrap: 'wrap', marginLeft: -10, marginRight: 10 }}> {'\u2B24'} </Text>
                                          <Text>{privacy}</Text>
                                       </View>

                                    )
                                 })
                              }
                           </View>
                        </View>
                        <Text style={{ textAlign: 'justify', fontSize: 15 }}>
                           {"\n"}
                        Dengan mengunjungi dan/atau mendaftar Akun pada Platform Kami, Anda dan/atau orang tua, wali atau pengampu Anda (jika Anda berusia dibawah 18 (delapan belas) tahun) menerima dan menyetujui pendekatan dan cara-cara yang digambarkan dalam Kebijakan Privasi ini
                        {"\n"}
                        </Text>
                        <View>
                           <Text style={{}}>
                              INFORMASI PRIBADI YANG DAPAT KAMI KUMPULKAN
                           {"\n"}
                           </Text>
                           <Text>
                              Kami dapat mengumpulkan Informasi berupa:
                           {"\n"}
                           </Text>
                           <View style={{ textAlign: 'justify', fontSize: 15 }}>
                              {
                                 this.state.a.map((a) => {
                                    return (
                                       <View style={{ flexDirection: 'row', paddingHorizontal: 10 }}>
                                          <Text style={{ fontSize: 8, flexWrap: 'wrap', marginLeft: -5, marginRight: 10, marginTop: 5 }}> {'\u2B24'} </Text>
                                          <Text style={{ textAlign: 'justify', paddingRight: 10 }}>{a}</Text>
                                       </View>
                                    )
                                 })
                              }
                           </View>
                        </View>
                        <View>

                        </View>
                     </View>
                  </Card>
               </View>
            </ScrollView>
         </SafeAreaView>
      )
   }
}

const styles = StyleSheet.create({})
