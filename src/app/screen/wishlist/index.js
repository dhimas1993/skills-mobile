import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView, SafeAreaView, Alert, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'

import HeaderApp from '../../components/HeaderApp'
import { getWishlist, deleteWishlist } from '../../../api/wishlist_api'

const source = 'https://skills.id/admin/source/images/thumbnail/'

class index extends Component {
   constructor(props) {
      // tarik data dari page redux
      // console.log('data redux', props.user.auth.data.id_user);
      super(props);
      this.state = {
         id_user: this.props.user.auth.data.id_user,
         wishlist: '',
      };
   }

   componentDidMount() {
      this.getWishlist()
   }



   getWishlist = async () => {
      const data = {
         id_user: this.state.id_user,
      }

      const api = await getWishlist(data)
      this.setState({
         wishlist: api.data
      })

      console.log("api", api.data.data);
   }

   alertButton(item) {
      // console.log("ini", item)
      Alert.alert(
         'Notifikasi',
         'Apakah anda yakin untuk menghapus warkshop pada wislist anda ?',
         [
            { text: 'NO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
            { text: 'YES', onPress: () => this.onButtonDelete(item) },
         ]
      );
   }

   onButtonDelete = async (item) => {
      const data = {
         id_wishlist: item
      }
      console.log("delete", item);
      const api = await deleteWishlist(data)

      { this.getWishlist() }
   }



   render() {
      console.log('as', this.state.wishlist);
      return (
         <SafeAreaView style={{ flex: 1 }}>
            <HeaderApp
               props={this.props.navigation}
               tittle="Wishlist Page"
            />

            {
               this.state.wishlist !== '' && this.state.wishlist.message !== 'Data Kosong' ?
                  <ScrollView style={{ height: '100%', ...styles.container }}>
                     {this.state.wishlist.data.map((data) => {

                        return (
                           <TouchableOpacity
                              onPress={() => { this.props.navigation.navigate('WorkshopPage', data) }}
                              style={styles.card}>
                              <Image
                                 style={{ width: 90, borderTopLeftRadius: 10, borderBottomLeftRadius: 10 }}
                                 source={{ uri: source + data.image }}
                              />
                              <View style={[styles.content, { justifyContent: 'center', marginHorizontal: 10, paddingVertical: 10 }]}>
                                 <Text style={styles.title} numberOfLines={2}>{data.title}</Text>
                                 <Text>Price : Rp. {Number(data.price).toLocaleString()}</Text>
                                 <Text>Status : {data.status}</Text>
                              </View>
                              <View>
                                 <TouchableOpacity
                                    onPress={() => this.alertButton(data.id_wishlist)}
                                    style={[styles.content, { alignSelf: 'flex-end' }]}>
                                    <Text style={styles.button}>Delete</Text>
                                 </TouchableOpacity>
                              </View>
                           </TouchableOpacity>
                        )
                     })}
                  </ScrollView>
                  :
                  <ScrollView>
                     <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                        <Text>Data Wishlist anda kosong</Text>
                     </View>
                  </ScrollView>
            }


         </SafeAreaView>
      )
   }
}

const mapStateToProps = state => {
   return {
      user: state.auth
   }
}

export default connect(mapStateToProps)(index)

const styles = StyleSheet.create({
   container: {
      marginVertical: 5,
      marginHorizontal: 15,
      marginBottom: 100
   },
   card: {
      flexDirection: 'row',
      backgroundColor: 'white',
      borderRadius: 10,
      marginBottom: 10
   },
   content: {
      flex: 1,
   },
   title: {
      fontSize: 15,
      fontWeight: 'bold',
      marginBottom: 5,
      fontStyle: 'italic'
   },
   button: {
      flex: 1,
      padding: 10,
      marginRight: 15, color: '#078b6d'
   }
})
