import React, { Component } from 'react'
import { Text, StyleSheet, View, SafeAreaView, Alert, ActivityIndicator } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { connect } from 'react-redux'

import { getReferralById } from '../../../api/referral_api'
import HeaderApp from '../../components/HeaderApp'

class Index extends Component {
   state = {
      referral: '',
      isLoading: false
   }

   componentDidMount() {
      this.getReferralById()
   }

   getReferralById = async () => {

      this.setState({
         isLoading: true
      })
      const data = {
         id_user: this.props.user.auth.data.id_user
      }
      const api = await getReferralById(data)
      if (api.data.message == 'berhasil') {
         this.setState({
            referral: api.data.data[0],
            isLoading: false
         })
      } else {
         Alert.alert("notif", 'gagal')
         this.setState({
            isLoading: false
         })
      }
      // console.log("referral", api.data.data);
   }

   render() {
      if (this.state.isLoading) {
         return (
            <SafeAreaView style={{}}>
               <HeaderApp
                  props={this.props.navigation}
                  tittle="Referral Page"
               />
               <View style={{ height: '85%', justifyContent: 'center' }}>
                  <ActivityIndicator size="large" color="#0000ff" />
               </View>
            </SafeAreaView>
         )
      } else {
         if (this.state.referral) {
            const { kode_referral, saldo_referral } = this.state.referral
            return (
               <SafeAreaView style={{ flex: 1 }}>
                  <HeaderApp
                     props={this.props.navigation}
                     tittle="Referral Page"
                  />
                  <ScrollView style={{ flex: 1, marginHorizontal: 15 }}>
                     <View style={{ justifyContent: 'center', borderBottomWidth: 1 }}>
                        <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'black', marginVertical: 10 }}>My Referral</Text>
                     </View>
                     <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'pink', marginVertical: 15, paddingVertical: 15 }}>
                        <Text style={{ fontWeight: 'bold' }}>SEBARKAN REFERRAL CODE, UNTUNG LEBIH BANYAK</Text>
                     </View>
                     <View style={{ justifyContent: 'center', marginBottom: 15 }}>
                        <Text style={{ textAlign: 'justify' }}>Halo Sobat Skills! Mau dapat untung lebih banyak di Skills.ID? Sebarkan saja Referral Code kamu, ajak kawan-kawanmu untuk register dan ikuti salah satu workshop di Skills.ID menggunakan referral code dari kamu</Text>
                     </View>
                     <View style={{ justifyContent: 'center', paddingBottom: 15, borderBottomWidth: 1 }}>
                        <Text style={{ textAlign: 'justify' }}>Setelah kawanmu melakukan pembayaran dengan referral code kamu, maka otomatis kawanmu akan mendapatkan saldo sebesar Rp. 50.000.- dan saldo referral code kamu pun akan bertambah sebesar Rp. 50.000.-. Ayo dapatkan saldo referral code sebanyak-banyaknya dan ikuti workshop suka-sukamu!</Text>
                     </View>
                     <View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 15, marginHorizontal: 20 }}>
                           <Text style={{ fontWeight: 'bold', color: 'black' }}>KODE REFERRAL ANDA</Text>
                           <Text>{kode_referral}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 15, marginHorizontal: 20 }}>
                           <Text style={{ fontWeight: 'bold', color: 'black' }}>JUMLAH SALDO ANDA</Text>
                           <Text>Rp. {saldo_referral}</Text>
                        </View>
                     </View>
                  </ScrollView>
               </SafeAreaView>
            )
         } else {
            return (
               <SafeAreaView style={{}}>
                  <HeaderApp
                     props={this.props.navigation}
                     tittle="Referral Page"
                  />
                  <View style={{ height: '85%', justifyContent: 'center' }}>
                     <Text style={{ textAlign: 'center' }}>belum ada kode referral</Text>
                  </View>
               </SafeAreaView>
            )
         }
      }
   }
}

const mapStateToProps = state => {
   return {
      user: state.auth
   }
}

export default connect(mapStateToProps, null)(Index)

const styles = StyleSheet.create({})
