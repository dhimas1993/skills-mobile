import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, Image, TextInput, SafeAreaView, ScrollView, SafeAreaViewBase } from 'react-native'
import { Container, Content, Icon, Textarea } from 'native-base'
import HTMLView from 'react-native-htmlview';

import HeaderApp from '../../components/HeaderApp'

export default class WorkshopPage extends Component {
   constructor(props) {
      super(props);
      this.state = {
         // get data dari article home
         data: this.props.navigation.state.params.detail_article,
      }
      // console.log(this.state.data);

   }

   render() {
      const { id, banner, category, title, label, posted_by, posted_date, heading, content, status, alt_image, meta } = this.state.data

      console.log();

      return (
         <SafeAreaView style={{ height: '100%' }}>
            <HeaderApp
               props={this.props.navigation}
               tittle="Article" />
            <ScrollView style={{ height: '100%' }}>
               <View style={{}}>
                  <Image
                     style={{ flex: 1, width: '100%', height: 280, resizeMode: 'cover' }}
                     source={{ uri: 'https://skills.id/admin/source/images/blog/' + banner }}
                  />
                  <View style={{ flex: 1, borderTopLeftRadius: 50, borderTopRightRadius: 50, backgroundColor: 'white', position: 'relative', opacity: 1, marginTop: -50 }}>
                     <View style={{ flexDirection: 'row', margin: 20 }}>

                        <View style={{ flex: 1, alignItems: 'center', marginHorizontal: 10, flexDirection: 'row' }}>
                           <View style={{ flex: 1 }}>
                              <TouchableOpacity>
                                 <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{posted_by}</Text>
                              </TouchableOpacity>
                              <Text style={{ fontSize: 13, flexWrap: "wrap", marginTop: 5 }}>{category}</Text>
                              <Text style={{ fontSize: 13, fontStyle: 'italic' }}>{posted_date}</Text>
                           </View>
                           <View style={{}}>
                              <TouchableOpacity style={{ alignItems: 'flex-end', alignSelf: 'center', flex: 1 }}>
                                 <Icon style={{ fontSize: 24 }} name="bookmark" />
                              </TouchableOpacity>
                              <TouchableOpacity style={{ alignItems: 'flex-end', alignSelf: 'center', flex: 1 }}>
                                 <Icon style={{ fontSize: 24 }} name="share" />
                              </TouchableOpacity>
                           </View>
                        </View>
                     </View>
                     <View style={{ flex: 1, flexWrap: 'wrap', alignItems: 'flex-start', marginHorizontal: 20, marginTop: -5 }}>
                        <View>
                           <Text style={{ textAlign: 'justify', fontWeight: 'bold', fontSize: 20, marginBottom: 20 }}>{title}</Text>
                           <HTMLView value={content} stylesheet={styles} />
                        </View>
                     </View>

                     <View style={{ flex: 1, flexWrap: 'wrap', alignItems: 'flex-start', marginHorizontal: 20 }}>
                        <Text style={{ textAlign: 'justify', fontWeight: 'bold', fontSize: 20, alignSelf: 'center', marginTop: 20 }}>Leave A Comment</Text>
                        <View style={{ width: '100%' }}>
                           <Text style={{ fontWeight: 'bold', fontSize: 18, marginVertical: 10 }}>Name</Text>
                           <TextInput
                              style={{ height: 50, borderColor: 'grey', borderWidth: 1, borderRadius: 50, padding: 15 }}
                              placeholder={'Comment Here'}
                           />
                        </View>
                        <View style={{ width: '100%' }}>
                           <Text style={{ fontWeight: 'bold', fontSize: 18, marginVertical: 10 }}>Email</Text>
                           <TextInput
                              style={{ height: 50, borderColor: 'grey', borderWidth: 1, borderRadius: 50, padding: 15 }}
                              placeholder={'Comment Here'}
                           />
                        </View>
                        <View style={{ width: '100%' }}>
                           <Text style={{ fontWeight: 'bold', fontSize: 18, marginVertical: 10 }}>Comment</Text>
                           <Textarea
                              style={{ height: 100, borderColor: 'grey', borderWidth: 1, borderRadius: 20, padding: 15 }}
                              placeholder={'Comment Here'}
                           />
                        </View>
                        <TouchableOpacity style={{ height: 50, backgroundColor: '#078b6d', width: '100%', borderRadius: 50, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', marginVertical: 10 }}>
                           <Text style={{ color: 'white' }}>Submit</Text>
                        </TouchableOpacity>
                     </View>
                  </View>
               </View>
            </ScrollView>
         </SafeAreaView>

      )
   }
}

const styles = StyleSheet.create({})
