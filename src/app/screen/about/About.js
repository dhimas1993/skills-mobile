import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, ImageBackground } from 'react-native'

const asu = 'https://images.unsplash.com/photo-1544014840-e9e5c4078b17?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2250&q=80'
export default class About extends Component {
   render() {
      return (
         <View style={{ flex: 1, justifyContent: 'space-around', alignItems: 'center' }}>
            <ImageBackground
               style={{ width: '100%', height: 180, marginTop: -25 }}
               source={{ uri: 'https://dxlm84u5gf2hs.cloudfront.net/wp-content/uploads/2019/12/Depositphotos_167269670_l-2015.jpg' }}
            >
               <Text style={styles.title}>About Us</Text>
            </ImageBackground>
            <View style={styles.container}>
               <Text style={styles.text}>
                  PT Permata Indo Sejahtera merupakan perusahaan yang berfokus pada bidang sales, distribution, dan outsourcing yang telah memiliki 10 cabang di seluruh Indonesia. Saat ini PT. Permata Indo Sejahtera sedang mengembangkan sayapnya kedalam industri digital dengan nama Kerja 365, dimana Skills.ID ada didalamnya
               </Text>
               <Text style={styles.text}>
                  Skills.ID adalah workshop placement yang mempertemukan edukator yang memiliki kemampuan untuk berbagi pengalaman, dengan masyarakat yang ingin belajar. Kami menyediakan edukator tempat untuk mempromosikan diri, ilmu, dan kemampuannya
               </Text>
            </View>
            <View>
               <Text style={styles.footer}>
                  @copyright skills.id, All Right Reserved
            </Text>
            </View>
         </View>
      )
   }
}

const styles = StyleSheet.create({
   container: {
      borderTopRightRadius: 50,
   },
   title: {
      fontSize: 25,
      textAlign: 'center',
      color: 'white',
      marginTop: 65
   },
   text: {
      marginHorizontal: 20,
      marginBottom: 15,
      textAlign: 'center'
   },
   footer: {
      justifyContent: 'flex-end',
      alignItems: 'flex-end',
      fontSize: 12,
   },
   picture: {
      width: '50%',
      height: 50,
      backgroundColor: 'pink'
   }
})
