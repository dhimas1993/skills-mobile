import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, Image, WebView, SafeAreaView, Alert, ScrollView, ActivityIndicator, Modal, FlatList } from 'react-native'
import { Icon, Card, Picker, CheckBox } from 'native-base'
import HTMLView from 'react-native-htmlview';
import { connect } from 'react-redux'
import { createCart } from '../../../api/cart_api'
import { createWishlist } from '../../../api/wishlist_api'

import Ionicons from 'react-native-vector-icons/Ionicons'

import HeaderApp from '../../components/HeaderApp'

class WorkshopPage extends Component {
   constructor(props) {
      console.log('wkpage', props.navigation.state.params);
      super(props);
      this.state = {
         // get data dari workshop home
         data: props.navigation.state.params,
         count: 1,
         selected: '',
         wishlist: '',
         workshop: '',
         isLoading: false,
         modalVisible: false,
         multi_tanggal: [],
         start_date: []
      }
      // console.log(this.state.data);
   }

   componentDidMount() {
      this.renderTanggal()
   }

   _plusCount = () => {
      this.setState({
         count: this.state.count + 1
      })
   }

   _minCount = () => {
      if (this.state.count <= 1) {
         Alert.alert('Notifikasi', 'Minimal qty adalah 1')
      } else {
         this.setState({
            count: this.state.count - 1
         })
      }
   }

   createWishlist = async (id, id_workshop) => {
      const id_user = this.props.user.auth.data.id_user
      const learner = this.props.user.auth.data.as_learner
      if (id_user && learner == 1) {
         const data = {
            id_workshop: id,
            id_workshop: id_workshop,
            id_user: id_user
         }

         if (id) {
            const data = {
               id_workshop: id,
               id_user: id_user
            }
            console.log("id", data)
            const api = await createWishlist(data)
            if (api) {
               Alert.alert("Notifikasi", "Create to favorite workshop Success")
            }
         } else {
            const data = {
               id_workshop: id_workshop,
               id_user: id_user
            }
            console.log("wk", data)
            const api = await createWishlist(data)
            if (api) {
               Alert.alert("Notifikasi", "Create to favorite workshop Success")
            }
         }
      } else {
         Alert.alert("Notifikasi", "Silahkan Login Terlebih Dahulu Sebagai Learner")
      }
   }

   onValueChange(value) {
      this.setState({
         selected: value
      });
   }

   addToCart = async (id, id_workshop) => {
      this.setState({
         isLoading: true
      })

      const id_user = this.props.user.auth.data.id_user
      const learner = this.props.user.auth.data.as_learner
      const multi_tanggal = this.state.multi_tanggal.toString()

      const data = {
         id_workshop: id,
         id_user: id_user,
         quant: this.state.count,
         multi_tanggal: this.state.multi_tanggal.toString()
      }

      const data1 = {
         id_workshop: id_workshop,
         id_user: id_user,
         quant: this.state.count,
         multi_tanggal: this.state.multi_tanggal.toString()
      }

      // console.log("ini", data)

      if (id_user && learner == 1) {
         if (multi_tanggal == [""]) {
            Alert.alert("notifikasi", 'Mohon pilih tanggal terlebih dahulu')
         } else {
            if (this.props.user.auth.data.id_user) {
               if (id) {
                  // console.log('ini');
                  const api = await createCart(data)
                  if (api) {
                     this.setState({
                        isLoading: false
                     })
                     console.log("result cart", api.data);
                     Alert.alert("Notifikasi", "Add to Cart Success")
                  } else {
                     this.setState({
                        isLoading: false
                     })
                     Alert.alert("Notifikasi", "Cart ditambahkan")
                  }
               } else {
                  // console.log('itu')
                  const api = await createCart(data1)
                  if (api) {
                     this.setState({
                        isLoading: false
                     })
                     console.log("result cart", api.data);
                     Alert.alert("Notifikasi", "Add to Cart Success")
                  } else {
                     this.setState({
                        isLoading: false
                     })
                     Alert.alert("Notifikasi", "Cart ditambahkan")
                  }
               }
            } else {
               Alert.alert("Notifikasi", "Silahakan Login Terlebih Dahulu")
            }
         }
      } else {
         Alert.alert("Notifikasi", "Silahkan Login Terlebih Dahulu Sebagai Learner")
      }

      this.setState({
         isLoading: false
      })
   }

   renderTanggal = () => {
      const start1 = []
      const start = this.state.data.start_date.split()
      const multi = this.state.data.multi_tanggal.split(", ")
      const ada = this.state.data.multi_tanggal

      const year = new Date().getFullYear()
      const month = (new Date().getMonth() + 1 < 10 ? '0' : '') + (new Date().getMonth() + 1)
      const day = (new Date().getDate() + 1 < 10 ? '0' : '') + (new Date().getDate())
      const now = Number(year + month + day)

      multi.map((item) => {
         start1.push(Number(item.replace(/-/g, '')))
      })
      // console.log("tanggal", start1.length);
      if (ada) {
         return (
            start1.map((item) => {
               if (item >= now) {
                  const tahun = item.toString().substr(0, 4)
                  const bulan = item.toString().slice(4, 6)
                  const tanggal = item.toString().slice(6, 8)
                  const date = tahun + "-" + bulan + "-" + tanggal
                  // console.log(item);
                  return (
                     <TouchableOpacity
                        style={{ backgroundColor: "#078b6d", padding: 15, borderRadius: 25, paddingHorizontal: 25, marginBottom: 5, marginHorizontal: 5 }}
                        onPress={() => this.pushTanggal(date)}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                           <Text style={{ color: 'white' }}>{date}</Text>
                           {
                              this.state.multi_tanggal.map((new_tgl) => {
                                 if (new_tgl == date) {
                                    return (
                                       <CheckBox checked={true} />
                                    )
                                 }
                              })
                           }
                        </View>
                     </TouchableOpacity>
                  )
               }
            })
         )
      } else {
         return (
            start.map((item) => {
               return (
                  <TouchableOpacity
                     style={{ backgroundColor: "#078b6d", padding: 15, borderRadius: 25, paddingHorizontal: 25, marginBottom: 5, marginHorizontal: 5 }}
                     onPress={() => this.pushTanggal(item)}>
                     <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={{ color: 'white' }}>{item}</Text>
                        {
                           this.state.multi_tanggal.map((new_tgl) => {
                              if (new_tgl == item) {
                                 return (
                                    <CheckBox checked={true} />
                                 )
                              }
                           })
                        }
                     </View>
                  </TouchableOpacity>
               )
            })
         )
      }
   }

   pushTanggal = (item) => {
      const index = this.state.multi_tanggal.indexOf(item)
      if (index === -1) {
         this.state.multi_tanggal.push(item)
      } else {
         var array = this.state.multi_tanggal.indexOf(item)
         if (array !== -1) this.state.multi_tanggal.splice(array, 1)
      }
      this.setState([...this.state.multi_tanggal])
      console.log('multi', this.state.multi_tanggal)
   }

   render() {
      const {
         id, title, desc, hosted_by, start_date, category, last_update, time, time_end, multi_tanggal, id_workshop,
         end_date, class_size, min_age, language, price, level, status, image, location,
         keyword, what_you_learn, what_to_bring, terms, inclusive, name, photo, desc_workshop
      } = this.state.data

      // const start = start_date.split()
      // const multi = multi_tanggal.split(", ")
      // console.log(this.state.data)

      if (this.state.isLoading == false) {
         return (
            <SafeAreaView style={{ flex: 1 }}>
               <HeaderApp
                  props={this.props.navigation}
                  tittle="Workshop Page"
               />
               <ScrollView>
                  <View style={{}}>
                     <Image
                        style={{ flex: 1, width: '100%', height: 280, resizeMode: 'cover' }}
                        source={{ uri: 'https://skills.id/admin/source/images/thumbnail/' + image }}
                     />
                     <View style={{ flex: 1, borderTopLeftRadius: 50, borderTopRightRadius: 50, backgroundColor: 'white', position: 'relative', opacity: 1, marginTop: -50 }}>
                        <View style={{ flexDirection: 'row', margin: 20 }}>
                           <TouchableOpacity>
                              <Image
                                 style={{ borderRadius: 100, height: 50, width: 50, justifyContent: 'flex-start', alignItems: 'center' }}
                                 source={{ uri: 'https://skills.id/admin/source/images/speaker/' + photo }}
                              />
                           </TouchableOpacity>
                           <View style={{ flex: 1, alignItems: 'center', marginHorizontal: 15, flexDirection: 'row' }}>
                              <View style={{ flex: 1 }}>
                                 <TouchableOpacity>
                                    <Text style={{ fontSize: 15, fontWeight: 'bold' }}>{name ? name : title}</Text>
                                 </TouchableOpacity>
                                 <Text style={{ fontSize: 13, flexWrap: "wrap", marginTop: 5 }}>{category}</Text>
                                 <Text style={{ fontSize: 13, fontStyle: 'italic' }}>{start_date}</Text>
                              </View>
                              <View style={{}}>
                                 <TouchableOpacity
                                    onPress={() => this.createWishlist(id, id_workshop)}
                                    style={{ alignItems: 'flex-end', alignSelf: 'center', flex: 1 }}>
                                    <Ionicons style={{ fontSize: 25, color: 'black' }} name="ios-bookmark" />
                                 </TouchableOpacity>
                                 <TouchableOpacity style={{ alignItems: 'flex-end', alignSelf: 'center', flex: 1 }}>
                                    <Icon style={{ fontSize: 25 }} name="share" />
                                 </TouchableOpacity>
                              </View>
                           </View>
                        </View>

                        <View style={{ flex: 1, flexWrap: 'wrap', alignItems: 'flex-start', marginHorizontal: 20 }}>
                           <View>
                              <Text style={{ textAlign: 'justify', fontWeight: 'bold', fontSize: 20, marginBottom: 10 }}>{title}</Text>
                              <View style={{ fontSize: 15, }}>
                                 <HTMLView value={desc ? desc : desc_workshop} stylesheet={styles} />
                              </View>
                              <Text style={{ textAlign: 'justify', fontWeight: 'bold', fontSize: 20, marginTop: 30 }}>What You Learn</Text>
                              <View style={{ textAlign: 'left', fontSize: 13, }}>
                                 <HTMLView value={what_you_learn} stylesheet={styles} />
                              </View>

                              <Text style={{ textAlign: 'justify', fontWeight: 'bold', fontSize: 20, marginTop: 15 }}>What is the price inclusive of?</Text>
                              <View style={{ textAlign: 'left', fontSize: 13, marginTop: 15, }}>
                                 <HTMLView value={inclusive} stylesheet={styles} />
                              </View>

                              <Text style={{ textAlign: 'justify', fontWeight: 'bold', fontSize: 20, marginTop: 15 }}>What you bring</Text>
                              <View style={{ textAlign: 'left', fontSize: 13, marginTop: 15, }}>
                                 <HTMLView value={what_to_bring} stylesheet={styles} />
                              </View>

                              <Text style={{ textAlign: 'justify', fontWeight: 'bold', fontSize: 20, marginTop: 15 }}>Term And Conditions</Text>
                              <View style={{ textAlign: 'left', fontSize: 13, marginTop: 15 }}>
                                 <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ flex: 1, textAlign: 'center', fontSize: 12 }}>{'\u2B24'}</Text>
                                    <HTMLView value={terms} />
                                 </View>
                              </View>

                              <Card style={{ borderRadius: 20, marginTop: 35, marginBottom: 25, padding: 10, flex: 1 }}>
                                 <Text style={styles.titleScedule}>Schedule</Text>
                                 <View
                                    style={{ marginVertical: 5 }}>
                                    {
                                       this.renderTanggal()
                                    }
                                 </View>
                                 <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: 1, padding: 5 }}>
                                       <View style={{ marginVertical: 5 }}>
                                          <Text style={styles.titleScedule}>Price</Text>
                                          <Text style={styles.textScedule}>Rp {Number(price).toLocaleString("IN")} / Ticket</Text>
                                       </View>

                                       <View style={{ marginVertical: 5 }}>
                                          <Text style={styles.titleScedule}>Class Size</Text>
                                          <Text style={styles.textScedule}>{class_size}</Text>
                                       </View>
                                       <View style={{ marginVertical: 5 }}>
                                          <Text style={styles.titleScedule}>Language</Text>
                                          <Text style={styles.textScedule}>{language}</Text>
                                       </View>
                                    </View>
                                    <View style={{ flex: 1, padding: 5, marginLeft: 10 }}>
                                       <View style={{ marginVertical: 5 }}>
                                          <Text style={styles.titleScedule}>Time</Text>
                                          <Text style={styles.textScedule}>{time} - {time_end ? time_end : 'Selesai'}</Text>
                                       </View>
                                       <View style={{ marginVertical: 5 }}>
                                          <Text style={styles.titleScedule}>Minimum Age</Text>
                                          <Text style={styles.textScedule}>{min_age}</Text>
                                       </View>
                                       <View style={{ marginVertical: 5 }}>
                                          <Text style={styles.titleScedule}>Level</Text>
                                          <Text style={styles.textScedule}>{level}</Text>
                                       </View>

                                    </View>
                                 </View>

                                 <View style={{ marginVertical: 5, marginHorizontal: 15, marginTop: 30 }}>
                                    <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 22 }}>Total</Text>
                                    <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 17 }}>Rp {Number(price * this.state.count).toLocaleString("IN")}</Text>
                                 </View>
                                 <View style={{ marginVertical: 5, marginHorizontal: 15 }}>
                                    <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 22 }}>Qty</Text>
                                    <View style={{ height: 45, justifyContent: 'space-around', padding: 5, borderRadius: 50, alignItems: 'center', borderStyle: 'solid', borderWidth: 2, borderColor: 'lightgrey', flexDirection: 'row', marginTop: 10 }}>
                                       <TouchableOpacity onPress={this._minCount}>
                                          <Icon style={{ fontSize: 25 }} name="ios-remove" />
                                       </TouchableOpacity>
                                       <Text style={{ fontSize: 17 }}>{this.state.count}</Text>
                                       <TouchableOpacity onPress={this._plusCount}>
                                          <Icon style={{ fontSize: 25 }} name="add" />
                                       </TouchableOpacity>
                                    </View>
                                 </View>
                                 <TouchableOpacity
                                    onPress={() => this.addToCart(id, id_workshop)}
                                    style={{ height: 45, backgroundColor: '#078b6d', borderRadius: 50, justifyContent: 'center', alignItems: 'center', margin: 10 }}
                                 >
                                    <Text style={{ color: 'white', fontSize: 17 }}>Add To Cart</Text>
                                 </TouchableOpacity>
                              </Card>
                           </View>
                        </View>
                     </View>
                  </View>
                  {/* {this.renderModal()} */}
               </ScrollView>
            </SafeAreaView>
         )
      } else {
         return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
               <Text>Please Wait</Text>
               <ActivityIndicator size="large" color="#0000ff" />
            </View>
         )
      }
   }
}

const mapStateToProps = state => {
   return {
      user: state.auth
   }
}

export default connect(mapStateToProps)(WorkshopPage)

const styles = StyleSheet.create({
   titleScedule: {
      fontSize: 17,
      fontWeight: 'bold',
      margin: 10
   },
   textScedule: {
      fontSize: 15,
      fontStyle: 'italic',
      marginLeft: 10
   },
   centeredView: {
      flex: 1,
      justifyContent: 'flex-end'
   },
   modalView: {
      minHeight: '50%',
      backgroundColor: "pink",
      borderRadius: 25,
      padding: 20,
      // alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
         width: 0,
         height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5
   }
})
