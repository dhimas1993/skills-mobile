import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, ScrollView, TouchableHighlight, TouchableOpacity, Button, Alert } from 'react-native'
import { Row, Card, Icon, Container, Content } from 'native-base';

const asu = 'https://images.unsplash.com/photo-1503428593586-e225b39bddfe?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60'

const data = [
   {
      'title': 'CD Sales Vol.1',
      'disc': 'Rp, 145.000',
      'price': 'Rp, 50.000',
      'picture': asu,
      'desc': 'lorem nih yakan yang banyak pokoknya'
   },
   {
      'title': 'CD Sales Vol.2',
      'disc': 'Rp, 145.000',
      'price': 'Rp, 50.000',
      'picture': asu,
      'desc': 'lorem nih yakan yang banyak pokoknya'
   },
   {
      'title': 'CD Sales Vol.3',
      'disc': 'Rp, 145.000',
      'price': 'Rp, 50.000',
      'picture': asu,
      'desc': 'lorem nih yakan yang banyak pokoknya'
   },
   {
      'title': 'CD Sales Vol.4',
      'disc': 'Rp, 145.000',
      'price': 'Rp, 50.000',
      'picture': asu,
      'desc': 'lorem nih yakan yang banyak pokoknya'
   },
   {
      'title': 'CD Sales Vol.5',
      'disc': 'Rp, 145.000',
      'price': 'Rp, 50.000',
      'picture': asu,
      'desc': 'lorem nih yakan yang banyak pokoknya'
   },
]

export default class Books extends Component {
   render() {
      return (
         <Container>
            <Content>
               <ScrollView>
                  <View style={{ width: '100%', justifyContent: 'space-between', flexWrap: 'wrap', marginVertical: 5, flexDirection: 'row', paddingHorizontal: 10 }}>
                     {
                        data.map((data) => {
                           return (
                              <Card style={{ width: '48%', borderRadius: 10, paddingBottom: 5 }}>
                                 <Image style={{ height: 120, flex: 1, borderTopLeftRadius: 10, borderTopRightRadius: 10 }} source={{ uri: asu }}></Image>
                                 <View style={{ flex: 1, marginTop: 5, paddingLeft: 5 }}>
                                    <Text style={{ paddingTop: 2, fontSize: 15, flexWrap: 'wrap' }}>{data.title}</Text>
                                    <Text style={{ paddingTop: 2, fontSize: 10, flexWrap: "wrap", textDecorationLine: 'line-through' }}>{data.disc}</Text>
                                    <Text style={{ paddingTop: 2, fontSize: 10, flexWrap: "wrap" }}>Rp. 50,000</Text>
                                    <TouchableOpacity
                                       style={{ backgroundColor: '#078b6d', alignContent: 'center', justifyContent: 'center', borderRadius: 10, marginRight: 5, marginTop: 10 }}>
                                       <Text style={styles.textButton}>Detail</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                       onPress={() => Alert.alert('Notifikasi', 'Comming Soon')}
                                       style={{ backgroundColor: '#078b6d', alignContent: 'center', justifyContent: 'center', borderRadius: 10, marginRight: 5, marginVertical: 5 }}>
                                       <Text style={styles.textButton}>Add To Cart</Text>
                                    </TouchableOpacity>
                                 </View>
                              </Card>
                           )
                        })
                     }
                  </View>
               </ScrollView>
            </Content>
         </Container>
      )
   }
}

const styles = StyleSheet.create({
   textButton: {
      color: 'white',
      textAlign: 'center',
      fontSize: 10,
      paddingVertical: 5
   }
})
