import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, SafeAreaView, TextInput, TouchableOpacity } from 'react-native';
import { Content, Tab, Tabs, TabHeading, Icon, Container } from 'native-base';

import HeaderApp from '../../components/HeaderApp'
import Video from './facilitatorScreen/Video'
import Workshop from './facilitatorScreen/Workshop'
import Ecommerce from './facilitatorScreen/Ecommerce'

export default class FacilitatorPage extends Component {
   constructor(props) {
      // console.log("facilitator", props.navigation.state.params);

      super(props);
      this.state = {
         // get data dari workshop home
         data: this.props.navigation.state.params.detail_edu,
         facilitator: ''
      }
   }

   componentDidMount() {
      this.state.data
   }

   componentDidUpdate() {
      // this.getWorkshop()
   }

   render() {
      const { name, email, competency, sum_wk, photo, id_user, id_speaker } = this.state.data
      // console.log('faci', this.state.data)
      return (
         <Container>
            <SafeAreaView >
               <View>
                  <HeaderApp
                     props={this.props.navigation}
                     tittle="Profile Facilitator"
                  />
               </View>
               <View style={{ flexDirection: 'row', paddingHorizontal: 15, paddingVertical: 15, backgroundColor: 'white' }}>
                  <View style={{ flex: 2 }}>
                     <Image style={styles.avatar} source={{ uri: 'https://skills.id/admin/source/images/speaker/' + photo }} />
                  </View>
                  <View style={{ flex: 8, paddingLeft: 20 }}>
                     <Text style={{ fontSize: 15, marginLeft: 10, paddingRight: 60, fontWeight: 'bold', marginBottom: 5 }}>{name}</Text>
                     <Text style={{ fontSize: 15, marginLeft: 10, marginBottom: 5, fontStyle: 'italic' }}>{email}</Text>
                     <View style={{ flexDirection: 'row', justifyContent: 'flex-start', paddingVertical: 2 }}>
                        <Text style={{ fontSize: 15, marginLeft: 10 }}>0 videos</Text>
                        <Text style={{ fontSize: 15, marginLeft: 50 }}>{sum_wk} class</Text>
                     </View>
                     <View style={{ flexDirection: "row", justifyContent: 'flex-start', marginLeft: 10 }}>
                        <View style={{ flexDirection: "row", marginVertical: 5 }}>
                           <Icon name="star" style={{ color: 'orange', fontSize: 18, marginRight: 5 }} />
                           <Icon name="star" style={{ color: 'orange', fontSize: 18, marginRight: 5 }} />
                           <Icon name="star" style={{ color: 'orange', fontSize: 18, marginRight: 5 }} />
                           <Icon name="star" style={{ color: 'orange', fontSize: 18, marginRight: 5 }} />
                           <Icon name="star" style={{ color: 'orange', fontSize: 18, marginRight: 5 }} />
                        </View>
                        <View style={{ marginVertical: 5, marginLeft: 30 }}>
                           <Text style={{ fontSize: 15 }}>5.0</Text>
                        </View>
                     </View>
                  </View>
               </View>
               <Workshop {...this.props} />
            </SafeAreaView>
            {/* <Tabs tabBarUnderlineStyle={{ backgroundColor: '#078b6d' }}>
               <Tab heading={
                  <TabHeading style={{ backgroundColor: 'white' }}>
                     <Text style={{}}>Workshop ({sum_wk})</Text>
                  </TabHeading>}>
                  <Workshop {...this.props} />
               </Tab>
               <Tab heading={
                  <TabHeading style={{ backgroundColor: 'white' }}>
                     <Text style={{}}>Video</Text>
                  </TabHeading>}>
                  <Video {...this.props} />
               </Tab>
               <Tab heading={
                  <TabHeading style={{ backgroundColor: 'white' }}>
                     <Text style={{}}>Ecommerce</Text>
                  </TabHeading>}>
                  <Ecommerce {...this.props} />
               </Tab>
            </Tabs> */}
         </Container>
      )
   }
}

const styles = StyleSheet.create({
   container: {
      width: '100%',
   },
   header: {
      backgroundColor: '#078b6d',
      width: '100%'
   },
   avatar: {
      width: 90,
      height: 90,
      borderRadius: 100,
      borderWidth: 2,
      borderColor: "#078b6d",
   },
   name: {
      fontSize: 22,
      color: '#078b6d',
      fontWeight: '600',
   },
   bodyContent: {
      flex: 1,
      alignItems: 'center',
      padding: 30,
   },
   name: {
      fontSize: 28,
      color: "#696969",
      fontWeight: "600"
   },
   info: {
      fontSize: 16,
      color: '#078b6d',
      marginTop: 10
   },
   description: {
      fontSize: 16,
      color: "#696969",
      marginTop: 10,
      textAlign: 'center',
      marginBottom: 25
   },
   buttonContainer: {
      marginTop: 10,
      height: 45,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: 20,
      width: 250,
      borderRadius: 30,
      backgroundColor: '#078b6d',
      borderColor: 'violet'
   },
   btntext: {
      color: 'white'
   }
})
