import { put, call, takeLatest, all } from 'redux-saga/effects';

import { handleError } from '../helper';
import {
  POST_LOGIN_BEGIN,
  POST_LOGIN_FAILURE,
  POST_LOGIN_SUCCESS,
  POST_LOGOUT_BEGIN,
  POST_LOGIN_REQUEST,
  POST_LOGOUT_REQUEST,
  POST_LOGOUT_SUCCESS,
  POST_LOGOUT_FAILURE,
} from './constant';

import { loginUser } from '../../api/auth_api';

export function* postLogin({ payload }) {
  try {
    yield put({ type: POST_LOGIN_BEGIN });
    console.log('data payload sga', payload);
    const data = yield call(loginUser, payload.form);
    console.log('data user saga', data.data.data[0]);
    // Once user authenticated
    yield put({
      type: POST_LOGIN_SUCCESS,
      payload: data.data.data[0],
    });
  } catch (error) {
    console.log(error);
    yield call(handleError, error, POST_LOGIN_FAILURE);
  }
}

export function* postLogout({ payload }) {
  try {
    yield put({ type: POST_LOGOUT_BEGIN });
    // Once user loggedout from server
    yield put({
      type: POST_LOGOUT_SUCCESS,
      payload: ''
    });
  } catch (error) {
    yield call(handleError, error, POST_LOGOUT_FAILURE);
  }
}

export default function* authFetch() {
  yield all([
    takeLatest(POST_LOGIN_REQUEST, postLogin),
    takeLatest(POST_LOGOUT_REQUEST, postLogout),
  ]);
}
