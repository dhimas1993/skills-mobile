/**
 * @format
 * @flow
 */

import {
  POST_LOGIN_REQUEST,
  POST_LOGOUT_REQUEST,
  POST_LOGOUT_SUCCESS,
  POST_REGIS_REQUEST,
  SET_IS_NEW,
} from './constant';

export const postLoginRequest = form => ({
  type: POST_LOGIN_REQUEST,
  payload: { form },
});

export const postLogoutRequest = token => ({
  type: POST_LOGOUT_REQUEST,
  payload: {
    token,
  },
});

export const forceLogout = () => ({
  type: POST_LOGOUT_REQUEST,
});


export const postRegisRequest = form => ({
  type: POST_REGIS_REQUEST,
  payload: { form },
});
