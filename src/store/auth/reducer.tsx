/**
 * @format
 * @flow
 */

import {
  POST_LOGIN_BEGIN,
  POST_LOGIN_SUCCESS,
  POST_LOGIN_FAILURE,
  POST_LOGOUT_BEGIN,
  POST_LOGOUT_SUCCESS,
  POST_LOGOUT_FAILURE,
  SET_IS_NEW,
} from './constant';

const initialState = {
  token: '',
  isLoggedIn: false,
  loading: false,
  error: null,
  data: [],
  log: ''
};

import { combineReducers } from 'redux'

const Auth = (state = initialState, action) => {
  switch (action.type) {
    case POST_LOGIN_BEGIN:
      console.log('begin', state);
      return {
        ...state,
        loading: true,
        log: '',
        isLoggedIn: false,
        error: null,
      };

    case POST_LOGIN_SUCCESS:
      console.log('payload, saga', state, action);
      console.log('success, saga', state);
      console.log('TEST LOGIN  INI BOS');

      return {
        ...state,
        loading: false,
        log: '',
        isLoggedIn: true,
        data: action.payload,

      };

    case POST_LOGIN_FAILURE:
      console.log('fail', state, action);
      return {
        ...state,
        loading: false,
        isLoggedIn: false,
        log: 'false',
        error: action.payload.error,
      };

    case POST_LOGOUT_BEGIN:
      return {
        ...state,
        loading: true,
        log: '',
        error: null,
      };

    case POST_LOGOUT_SUCCESS:
      return {
        ...state,
        loading: false,
        isLoggedIn: false,
        isNew: false,
        token: '',
        data: [],
        log: '',
        error: null,
      };

    case POST_LOGOUT_FAILURE:
      return {
        ...state,
        loading: false,
        log: '',
        error: action.payload.error,
      };

    default:
      return state;

  }
};

export default combineReducers(
  {
    auth: Auth
  }
)
