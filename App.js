import React, { Component } from 'react';
import { Provider } from 'react-redux'
import Ionicons from 'react-native-vector-icons/Ionicons';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import { PersistGate } from 'redux-persist/integration/react';
import { store as STORE, persistor } from './src/store/index';

import Home from './src/app/page/Home/Home'
import Cart from './src/app/page/Cart/Cart'
import Explore from './src/app/page/Explore/Explore'
import More from './src/app/page/More/More'
import CategoryPage from './src/app/screen/category/CategoryPage'
import DetailCategoryPage from './src/app/screen/category/DetailCategoryPage'
import FacilitatorPage from './src/app/screen/facilitator/FacilitatorPage'
import WorkshopPage from './src/app/screen/workshop/WorkshopPage'
import ArticlePage from './src/app/screen/article/ArticlePage'
import Checkout from './src/app/screen/checkout/Checkout'
import Pay from './src/app/screen/checkout/Index'
import Payment from './src/app/screen/checkout/Payment'
import MyProfile from './src/app/screen/account/Account'
import Invoice from './src/app/screen/checkout/Invoice'
import EditProfileEducator from './src/app/screen/account/EditAccountEducator'
import EditProfileLearner from './src/app/screen/account/EditAccountLearner'
import MyVideo from './src/app/screen/video/myVideo'
import SubmitWorkshop from './src/app/screen/submit/workshop/SubmitWorkshop'
import ManageWorkshop from './src/app/screen/submit/workshop'
import editWorkshop from './src/app/screen/submit/workshop/editWorkshop'
import Privacy from './src/app/screen/privacy/Privacy'
import Help from './src/app/screen/help/Help'
import About from './src/app/screen/about/About'
import Login from './src/app/screen/login/Login'
import LoginPage from './src/app/screen/login/loginScreen/LoginPage'
import RegisFacilitator from './src/app/screen/login/loginScreen/RegisterPageFacilitator'
import RegisParticipant from './src/app/screen/login/loginScreen/RegisterPageParticipant'
import Password from './src/app/screen/account/Password'
import Verified from './src/app/screen/verified/verified'

import Trainer from './src/app/screen/submit/trainer/Index'
import addTrainer from './src/app/screen/submit/trainer/addTrainer'
import editTrainer from './src/app/screen/submit/trainer/editTrainer'

import Gallery from './src/app/screen/submit/galery/index'
import addGallery from './src/app/screen/submit/galery/addGallery'

import Wishlist from './src/app/screen/wishlist/index'
import ReferralPage from './src/app/screen/Referral/Index'

import HotWorkshop from './src/app/components/Hot_workshop'
import Upcomming from './src/app/components/UpComming'





const MainTabFacilitaor = createBottomTabNavigator({
  Home: Home,
  Cart: Cart,
  Explore: Explore,
  More: More,
},
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        console.log('app', navigation)
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === 'Home') {
          iconName = focused
            ? 'ios-home'
            : 'ios-home';
          // Sometimes we want to add badges to some icons.
          // You can check the implementation below.
          // IconComponent = HomeIconWithBadge;
        } else if (routeName === 'Cart') {
          iconName = focused ? 'ios-cart' : 'ios-cart';
        } else if (routeName === 'Explore') {
          iconName = focused ? 'ios-search' : 'ios-search';
        } else if (routeName === 'More') {
          iconName = focused ? 'ios-more' : 'ios-more';
        }

        // You can return any component that you like here!
        return <IconComponent style={{ paddingVertical: 10 }} name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#ea2c62',
      inactiveTintColor: 'gray',
    },
  }
);


const RootStack = createStackNavigator(
  {
    // Auth: AuthScreen,
    Main_facilitator: MainTabFacilitaor,
    Payment: Payment,
    Checkout: Checkout,
    WorkshopPage: WorkshopPage,
    ArticlePage: ArticlePage,
    CategoryPage: CategoryPage,
    DetailCategoryPage: DetailCategoryPage,
    FacilitatorPage: FacilitatorPage,
    MyProfile: MyProfile,
    EditProfileLearner: EditProfileLearner,
    EditProfileEducator: EditProfileEducator,
    MyVideo: MyVideo,
    SubmitWorkshop: SubmitWorkshop,
    ManageWorkshop: ManageWorkshop,
    Privacy: Privacy,
    Help: Help,
    About: About,
    Login: Login,
    LoginPage: LoginPage,
    RegisFacilitator: RegisFacilitator,
    RegisParticipant: RegisParticipant,
    Trainer: Trainer,
    addTrainer: addTrainer,
    editTrainer: editTrainer,
    Password: Password,
    editWorkshop: editWorkshop,
    Gallery: Gallery,
    addGallery: addGallery,
    Wishlist: Wishlist,
    ReferralPage: ReferralPage,
    Pay: Pay,
    Invoice: Invoice,
    Verified: Verified,
    HotWorkshop: HotWorkshop,
    Upcomming: Upcomming,
  },
  {
    headerMode: 'none'
  }
)


const AppContainer = createAppContainer(RootStack)

class App extends Component {

  render() {
    return (
      <Provider store={STORE}>
        <PersistGate persistor={persistor}>
          <AppContainer {...this.props} />
        </PersistGate>
      </Provider>
    )
  }
}


export default App
